-- Parameters
DECLARE @location VARCHAR(3), @TheDate DATE, @locationId INT
SET @location = 'HKG'
SET @TheDate = '20180416'
SELECT @locationId  = loc.LocationID FROM shared.Location loc where loc.City =  @location   -- 1 = LDN, 3 = HKG

-- QUERY 1 : Run procesure EXEC Voice_Data.dbo.uspPreAnalytics an then run the following query to see what has been copied where for the
--			 location set above on the date set above.
-- Voice Data Staging Records and Status
SELECT 'Staging Number of Records' As Process, count(*) NumberOfRecords
FROM [Voice_Data].[dbo].[Staging_Metadata]
where location = @location
and cast(localstarttime as date) = @TheDate

SELECT 'Staging By Status' As Process, CopyStatus, Status, count(*) NumberOfRecords
FROM [Voice_Data].[dbo].[Staging_Metadata]
where location = @location
and cast(localstarttime as date) = @TheDate
group by copystatus, status

-- Pre-Analytics
SELECT  'Pre-Analytics Number of Records', count(*) NumberOfRecords
FROM [Voice_Data].[dbo].PreAnalytics_Metadata
where location = @location and cast(localstarttime as date) = @TheDate

SELECT  'Pre-Analytics by Status', status, s.name, count(*) NumberOfRecords
FROM [Voice_Data].[dbo].PreAnalytics_Metadata m
left join Voice_data.dbo.DataCallStatus s
	ON m.Status = s.statusId
where location = @location and cast(localstarttime as date) = @TheDate
group by status, s.name
order by status

-- confirm the calls have been copied to the Analytics database
select 'Analytics number of records', count(*) NumberOfRecords
from Voice_Analytics.audio.call c
where locationId = @locationId and cast(localstarttime as date) = @TheDate

-- group by status
select 'Analytics by Status', status, count(*) 
from Voice_Analytics.audio.[call] c
left join Voice_data.dbo.DataCallStatus s
	ON c.Status = s.statusId
where locationId = @locationId and cast(localstarttime as date) = @TheDate 
group by status, s.name
ORDER BY status

-- by filter type satisfied
select 'Filters Satisfied', cfs.FilterTypeId, ft.Name, count(*) NumberofRecords
FROM Voice_Analytics.audio.call c
INNER JOIN Voice_Analytics.audio.CallFiltersSatisfied cfs
	ON c.callid=cfs.CallID
INNER JOIN Voice_Analytics.filter.FilterType ft
	ON ft.FilterTypeID = cfs.FilterTypeId
where locationId = @locationId and cast(localstarttime as date) = @TheDate
GROUP BY cfs.FilterTypeId, ft.name
ORDER BY 2,3


select 'Actual Filter Used For Ingest', ActualFilterUsedForIngest, fc.Name, count(*) NumberOfCalls, seq as FilterSequence
from Voice_Analytics.audio.call c
inner join Voice_Analytics.[filter].[FilterCategory] fc
	on c.ActualFilterUsedForIngest = fc.FilterCategoryID
where locationId = @locationId and cast(localstarttime as date) = @TheDate
group by ActualFilterUsedForIngest, fc.Name, seq
order by fc.seq

DECLARE @NumberOfRecords INT
SELECT @NumberOfRecords = count(*) 
from Voice_Analytics.audio.call c
where locationId = @locationId and cast(localstarttime as date) = @TheDate
-- Calls per hour
select  DATEPART(HH,c.LocalStartTime), CAST(count(*)/CAST(@NumberOfRecords as decimal(10,2))*100 as decimal(5,2))
from Voice_Analytics.audio.call c
where locationId = @locationId and cast(localstarttime as date) = @TheDate
group by DATEPART(HH,c.LocalStartTime)
order by 1

select 'Last Followed', cdf.custodianId CustodianId, c.DisplayName CustodianName, bu.Name AssetClass ,d.Name Segment, MAX(cdf.DateFollowed) LastFollowed
from Voice_Analytics.[filter].[CustodianDateFollowed] cdf
inner join Voice_Analytics.shared.Custodian c
	ON cdf.CustodianId = c.CustodianID
inner join Voice_Analytics.shared.Department d
	ON c.DepartmentID = d.DepartmentID
inner join Voice_Analytics.shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
where c.LocationID = @locationId
group by cdf.custodianId, c.DisplayName, bu.Name ,d.Name
order by bu.Name ,d.Name, 6 desc

-- Bnnchmarks applicable
select bu.name, d.name,  b.name, f.UTCStartDateTime, f.UTCStopDateTime
from Voice_Analytics.filter.filter f
inner join Voice_Analytics.shared.department d
	on f.DepartmentId = d.DepartmentID
inner join Voice_Analytics.shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID
inner join Voice_Analytics.filter.Benchmark b
	ON f.OriginalFilterId = b.BenchmarkID
 where f.UTCStartDateTime >= @TheDate and f.UTCStartDateTime < DATEADD (DD,1, @TheDate)
and f.LocationId=@locationId and f.FilterTypeID in (1,2)
order by 1,2,3,4,5

-- Market numbers applicable
-- Bnnchmarks applicable
select bu.name, b.name, f.UTCStartDateTime, f.UTCStopDateTime
from Voice_Analytics.filter.filter f
inner join Voice_Analytics.shared.BusinessUnit bu
	on f.BusinessUnitId = bu.BusinessUnitID
inner join Voice_Analytics.filter.MarketEventData b
	ON f.OriginalFilterId = b.MarketEventDataId
 where f.UTCStartDateTime >= @TheDate and f.UTCStartDateTime <  DATEADD (DD,1, @TheDate)
and f.LocationId=@locationId and f.FilterTypeID=3
order by 1,2,3,4
