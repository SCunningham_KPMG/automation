SELECT CustodianLocation,
		CAST(LocalStartTime as date) thedate,
	count(*)

  FROM [Audio2_Metadata].[report].[TraderInteractions]

  where LocalStartTime >= '20180322' AND LocalStartTime < '20180404' AND CustodianLocation = 'HKG'
  group by CustodianLocation,
		CAST(LocalStartTime as date)
order by 1,2

select LocationId, Cast(LocalStartTime as date), count(*) 
from [Voice_Analytics].audio.call
where LocalStartTime >= '20180322'  AND LocalStartTime < '20180404' and status <> 61 and locationId=3
group by LocationId, Cast(LocalStartTime as date)  
order by 1,2

select c.*, cnew.status
FROM [Audio2_Metadata].[report].[TraderInteractions] C
LEFT JOIN [Voice_Analytics].audio.call cnew
	ON c.InteractionID = cnew.callId
WHERE CAST(c.LocalStartTime as date) >= '20180322'  AND CAST(c.LocalStartTime as date) < '20180404' AND C.custodianLocation = 'HKG'
AND cnew.callid is null
and c.interactionId NOT IN
	(SELECT CallID FROM Voice_Data.dbo.PreAnalytics_Metadata WHERE Location = 'HKG' AND CAST(LocalStartTime as date) >= '20180322' AND CAST(c.LocalStartTime as date) < '20180404' AND status=29)
order by 1

select cnew.*
FROM [Voice_Analytics].audio.call cnew
LEFT JOIN [Audio2_Metadata].[report].[TraderInteractions] C
	ON c.InteractionID = cnew.callId
WHERE CAST(cnew.LocalStartTime as date) >= '20180322'  AND CAST(cnew.LocalStartTime as date) < '20180404' AND cnew.LocationID=3
AND c.InteractionID is null
and cnew.status <> 61
and cnew.callId not in
	(SELECT InteractionID FROM Audio2_Metadata.etl.TraderInteraction_Duplicates)
order by 1

select cnew.*
FROM [Voice_Analytics].audio.call cnew
LEFT JOIN [Audio2_Metadata].[report].[TraderInteractions] C
	ON c.InteractionID = cnew.callId
WHERE CAST(cnew.LocalStartTime as date) = '20180322' AND Cnew.LocationID = 3 and cnew.Status <> 61 -- custodian not found
AND c.InteractionID is null
order by 1




select * from Audio2_Metadata.report.TraderInteractions where interactionid = '6536174247462647848'
SELECT * from [Voice_Analytics].audio.call WHERE callid = '6536174247462647848'
SELECT * from [Voice_Data].dbo.Staging_Metadata WHERE callid = '6536174247462647848'
SELECT * from [Voice_Data].dbo.Staging_TrunkLabel WHERE callid = '6536174247462647848'
SELECT * from [Voice_Data].dbo.PreAnalytics_Metadata WHERE callid = '6536174247462647848'



