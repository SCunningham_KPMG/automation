USE Voice_Analytics
GO

-- 495 rows
INSERT INTO [filter].[MarketEventDataBusinessUnit]
           ([MarketEventDataId]
           ,[BusinessUnitId]
           ,[LocationId]
)
SELECT 
		--old.[Location]
  --    ,old.[AssetClass]
  --    ,old.[NumbersDay]
  --    ,old.[NumbersDescription]
  --    ,old.[LocalStartTime]
  --    ,old.[LocalEndTime]
	  new.MarketEventDataId
	  ,bu.BusinessUnitID
	  ,l.LocationID
FROM [Audio2_Metadata].[dbo].[MarketNumbers_Parameters] old
INNER JOIN [Voice_Analytics].[filter].[MarketEventData] new
	on old.NumbersDay = new.[MarketEventDataDay]
	and old.LocalStartTime = new.[LocalStartTime]
	and old.LocalEndTime = new.[LocalStopTime]
	and old.NumbersDescription = new.Name
INNER JOIN [Voice_Analytics].[shared].[Location] l 
	on old.Location= l.City
INNER JOIN [Voice_Analytics].[shared].[BusinessUnit] bu
	on old.AssetClass = bu.Code


INSERT INTO [filter].[MarketEventDataBusinessUnit]
           ([MarketEventDataId]
           ,[BusinessUnitId]
           ,[LocationId]
)
SELECT 
		--old.[Location]
  --    ,old.[AssetClass]
  --    ,old.[NumbersDay]
  --    ,old.[NumbersDescription]
  --    ,old.[LocalStartTime]
  --    ,old.[LocalEndTime]
	  new.MarketEventDataId
	  ,bu.BusinessUnitID
	  ,l.LocationID
FROM [Audio2_Metadata].[dbo].[MarketEvent_Parameters] old
INNER JOIN [Voice_Analytics].[filter].[MarketEventData] new
	on old.EventDay = new.[MarketEventDataDay]
	and old.LocalStartTime = new.[LocalStartTime]
	and old.LocalEndTime = new.[LocalStopTime]
	and old.EventDescription = new.Name
INNER JOIN [Voice_Analytics].[shared].[Location] l 
	on old.Location= l.City
INNER JOIN [Voice_Analytics].[shared].[BusinessUnit] bu
	on old.AssetClass = bu.Code




