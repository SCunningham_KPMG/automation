USE [Voice_Analytics]
GO

INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20141102 02:00:00','20150308 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20150308 02:00:00','20151101 02:00:00',-4
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20151101 02:00:00','20160313 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20160313 02:00:00','20161106 02:00:00',-4
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20161106 02:00:00','20170312 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20170312 02:00:00','20171105 02:00:00',-4
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20171105 02:00:00','20180311 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20180311 02:00:00','20181104 02:00:00',-4
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20181104 02:00:00','20190310 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20190310 02:00:00','20191103 02:00:00',-4
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20191103 02:00:00','20200308 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'EST','20200308 02:00:00','20201101 02:00:00',-4

INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'HKT','20150101','20990101',8

INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'JST','20150101','20990101',9

INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20141026 02:00:00','20150329 01:00:00',0
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20150329 01:00:00','20151025 02:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20151025 02:00:00','20160327 01:00:00',0
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20160327 01:00:00','20161030 02:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20161030 02:00:00','20170326 01:00:00',0
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20170326 01:00:00','20171029 02:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20171029 02:00:00','20180325 01:00:00',0
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20180325 01:00:00','20181028 02:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20181028 02:00:00','20190331 01:00:00',0
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20190331 01:00:00','20191027 02:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20191027 02:00:00','20200329 01:00:00',0
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'GMT','20200329 01:00:00','20201025 02:00:00',1

INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20141102 02:00:00','20150308 02:00:00',-6
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20150308 02:00:00','20151101 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20151101 02:00:00','20160313 02:00:00',-6
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20160313 02:00:00','20161106 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20161106 02:00:00','20170312 02:00:00',-6
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20170312 02:00:00','20171105 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20171105 02:00:00','20180311 02:00:00',-6
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20180311 02:00:00','20181104 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20181104 02:00:00','20190310 02:00:00',-6
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20190310 02:00:00','20191103 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20191103 02:00:00','20200308 02:00:00',-5
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CST','20200308 02:00:00','20201101 02:00:00',-5


INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20141005 02:00:00','20150405 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20150405 03:00:00','20151004 02:00:00',10
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20141005 02:00:00','20150405 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20150405 03:00:00','20151004 02:00:00',10
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20151004 02:00:00','20160403 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20160403 03:00:00','20161002 02:00:00',10
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20161002 02:00:00','20170402 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20170402 03:00:00','20171001 02:00:00',10
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20171001 02:00:00','20180401 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20180401 03:00:00','20181007 02:00:00',10
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20181007 02:00:00','20190407 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20190407 03:00:00','20191006 02:00:00',10
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20191006 02:00:00','20200405 03:00:00', 11
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'AEST','20200405 03:00:00','20201004 02:00:00',10

INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20141026 02:00:00','20150329 01:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20150329 01:00:00','20151025 02:00:00',2
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20151025 02:00:00','20160327 01:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20160327 01:00:00','20161030 02:00:00',2
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20161030 02:00:00','20170326 01:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20170326 01:00:00','20171029 02:00:00',2
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20171029 02:00:00','20180325 01:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20180325 01:00:00','20181028 02:00:00',2
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20181028 02:00:00','20190331 01:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20190331 01:00:00','20191027 02:00:00',2
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20191027 02:00:00','20200329 01:00:00',1
INSERT INTO [shared].[TimezoneDates] ([TimeZone],[StartDate],[StopDate],[UTCHoursOffset])
	SELECT 'CET','20200329 01:00:00','20201025 02:00:00',2

