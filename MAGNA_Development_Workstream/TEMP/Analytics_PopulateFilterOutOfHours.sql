USE [Voice_Analytics]
GO

INSERT INTO Voice_Analytics.[filter].[OutOfHours]
           (locationId
           ,businessunitid
           ,[LocalStartTime]
           ,[LocalEndTime]
           ,[StartDate]
           ,[EndDate]
		   ,[IsActive]
)
SELECT b.locationId ,bu.businessunitid, StartTime, EndTime, StartDate, EndDate, case when endDate > getdate() then 1 else 0 end
FROM [Audio2_Metadata].[dbo].[OutOfHoursParameters] a
INNER JOIN Voice_Analytics.shared.Location b on a.Location = b.City
INNER JOIN Voice_Analytics.shared.BusinessUnit bu on a.AssetClass = bu.Code