USE [Voice_Analytics]
GO
/****** Object:  StoredProcedure [dbo].[uspCopyToAnalytics]    Script Date: 13/03/2018 15:58:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*------------------------------------------------------------------------------------------------------
Procedure:      dbo.uspCopyToAnalytics
Author:         PBains

Description:   
Copies data from Voice_Data pre-analytics to Voice_Analytics audio tables, in preparation for analysis.


Call:
EXEC dbo.spCopyToAnalytics

Return Value:
	0 = success
	>0 = error. Look in Voics_Analytics.dbo.ErrorLog

Change History
------------------------------
20180313 1.00	PB	Initial version
------------------------------------------------------------------------------------------------------ */


ALTER PROCEDURE [dbo].[uspCopyToAnalytics]
AS
BEGIN
 
	SET NOCOUNT ON 
	DECLARE @retStatus INT
	DECLARE @currentDateTime datetime

	SET @currentDateTime = getdate()
	SET @retStatus = 0

BEGIN TRY
	SET @retStatus = 1
	UPDATE Voice_Data.dbo.PreAnalytics_Metadata SET Status = Status + 2 WHERE Status IN (40,41)

	BEGIN TRAN

	SET @retStatus = 2
	INSERT INTO [audio].[Call]
		(CallId
		,[LocalStartTime]
		,[UTCStartTime]
		,[LocalEndTime]
		,[UTCEndTime]
		,[DurationInSeconds]
		,[TrunkLabel]
		,[ParentID]
		,[DialledNumber]
		,[PhoneNumber]
		,[CLIANI]
		,[LanguageID]
		,[CallDirectionId]
		,[InitiatorID]
		,[LocationID]
		,[DateCreated]
		,[Status])
	SELECT
		[CallID],
		[LocalStartTime],
		[UTCStartTime],
		[LocalStopTime],
		[UTCStopTime],
		[Duration],
		[TrunkLabel],
		[CallParentID],
		[DialedNumber],
		[PhoneNumber],
		CallingLineIdentifier as CLIANI,
		NULL As LanguageId,
		d.CallDirectionId,
		[InitiatorTRID],
		loc.locationId,
		getdate(),
		60 As Status
	FROM [Voice_Data].[dbo].[PreAnalytics_Metadata] m
	INNER JOIN Voice_Analytics.[shared].[Location] loc
		ON m.location = loc.city
	INNER JOIN Voice_Analytics.[audio].[CallDirection] d
		ON m.CallDirection = d.name
	WHERE m.status IN (42,43)

	SET @retStatus = 3
	INSERT INTO [audio].[File]
		([Name]
		,[FileTypeID]
		,[Directory]
		,[CompleteFilePath]
		,[RecordingTypeID]
		,[DateCreated])
	SELECT MediaFileName, NULL, c.CallId, MediaPath, NULL, getdate()
	FROM [audio].[call] c
	INNER JOIN [Voice_Data].[dbo].[PreAnalytics_Metadata] m
		ON c.CallID = m.CallID
	WHERE c.status = 60

	SET @retStatus = 4
	INSERT INTO [audio].[CallFile]
			   ([CallID]
			   ,[FileID])
	SELECT f.Directory, f.FileId FROM audio.[file] f INNER JOIN [audio].[call] c ON f.Directory = c.CallID WHERE c.status = 60
	UPDATE audio.[file] SET directory = NULL

	SET @retStatus = 4
	INSERT INTO [audio].[CallCustodianIdentity]
		([CallID]
		,[CustodianIdentityID]
		,[DeviceTypeID]
		,[IsInitiator]
		,[CallDirectionId]
		,[DateCreated])
	SELECT 
		c.CallID,
		i.CustodianIdentityID,
		m.FinalDeviceTypeID,
		NULL as IsInitator,
		d.CallDirectionId,
		getdate() as DateCreated

	FROM [audio].[call] c
	INNER JOIN [Voice_Data].[dbo].[PreAnalytics_Metadata] m
		ON c.CallID = m.CallID
	INNER JOIN [shared].[CustodianIdentity] i
		on i.CustodianIdentity = m.ParticipantIdentity
		and c.LocalStartTime >= i.StartDate 
		and c.LocalEndTime <= i.EndDate
	INNER JOIN [shared].[Custodian] cu
		ON i.CustodianID = cu.CustodianID
		AND cu.LocationID = c.LocationID
	INNER JOIN Voice_Analytics.[audio].[CallDirection] d
		ON m.CallDirection = d.name
	WHERE c.status = 60

	COMMIT
	UPDATE  Voice_Data.dbo.PreAnalytics_Metadata SET Status = Status + 8 WHERE Status IN (42,43)
	SET @retStatus = 0
END TRY
BEGIN CATCH
	INSERT Voice_Analytics.dbo.ErrorLog (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage) 
		SELECT ERROR_NUMBER(), ERROR_SEVERITY (), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), 'Error location:' + CAST(@retStatus as varchar(3)) + '::' + ERROR_MESSAGE()

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION
END CATCH

RETURN @retStatus

END




