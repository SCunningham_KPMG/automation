USE [Voice_Analytics]
GO


INSERT INTO [filter].[Filter]
           ([FilterTypeID]
		   ,OriginalFilterId
           ,[UTCStartDateTime]
           ,[UTCStopDateTime]
           ,[businessUnitId]
           ,[departmentId]
           ,[locationId]
           )

SELECT 
d.MarketEventDataTypeiD 
,d.[MarketEventDataId]
,DATEADD(hh,tzd1.UTCHoursOffset, d.marketEventDataDay + CAST(d.localSTartTime as datetime)) UTCStartDateTime
,DATEADD(hh,tzd2.UTCHoursOffset, d.marketEventDataDay + CAST(d.localStopTime as datetime)) UTCStopDateTime
,b.businessUnitId
,NULL
,b.locationId 
/*
,d.marketEventDataId, d.marketEventDataDay, d.localSTartTime, d.localStopTime, tzd1.UTCHoursOffset, tzd2.UTCHoursOffset
,d.marketEventDataDay + CAST(d.localSTartTime as datetime) StartDateTime
,d.marketEventDataDay + CAST(d.localStopTime as datetime) StopDateTime
*/
  FROM [Voice_Analytics].[filter].[MarketEventData] d
  inner join [filter].[MarketEventDataBusinessUnit] b
	on d.[MarketEventDataId] = b.[MarketEventDataId]
	and b.isactive=1
	inner join [shared].[Location] loc
		on b.locationId = loc.LocationID
	inner join [shared].[TimezoneDates] tzd1
		on tzd1.timezone = loc.TimeZone
		and d.marketEventDataDay + CAST(d.localSTartTime as datetime) >= tzd1.startdate and d.marketEventDataDay + CAST(d.localSTartTime as datetime) < tzd1.stopdate
	inner join [shared].[TimezoneDates] tzd2
		on tzd2.timezone = loc.TimeZone
		and d.marketEventDataDay + CAST(d.localStopTime as datetime) >= tzd2.startdate and d.marketEventDataDay + CAST(d.localStopTime as datetime) < tzd2.stopdate
  where d.isactive =1 
  order by 3 desc

INSERT INTO [filter].[Filter]
           ([FilterTypeID]
		   ,OriginalFilterId
           ,[UTCStartDateTime]
           ,[UTCStopDateTime]
           ,[businessUnitId]
           ,[departmentId]
           ,[locationId]
           )
SELECT 
d.BenchmarkTypeID
,d.BenchmarkId
,DATEADD(mi,b.minutesbefore,rbutc.UTCDateTime) UTCStartDateTime
,DATEADD(mi,b.minutesafter,rbutc.UTCDateTime) UTCStopDateTime
,NULL
,b.DepartmentID
,b.locationId 

FROM [Voice_Analytics].[filter].[Benchmark] d
  inner join [filter].[BenchmarkDepartment] b
	on d.[BenchmarkId] = b.[BenchmarkId]
	and b.isactive=1
	inner join [shared].[Location] loc
		on b.locationId = loc.LocationID
	inner join [Audio2_Metadata].[report].[Benchmark_UTC] rbutc
		on d.name = rbutc.benchmarkname

  where d.isactive =1 


INSERT INTO [filter].[Filter]
           ([FilterTypeID]
		   ,OriginalFilterId
           ,[UTCStartDateTime]
           ,[UTCStopDateTime]
           ,[businessUnitId]
           ,[departmentId]
           ,[locationId]
           )
SELECT 
d.BenchmarkTypeID
,d.BenchmarkId
,DATEADD(mi,b.minutesbefore,rbutc.UTCDateTime) UTCStartDateTime
,DATEADD(mi,b.minutesafter,rbutc.UTCDateTime) UTCStopDateTime
,NULL
,b.DepartmentID
,b.locationId 

FROM [Voice_Analytics].[filter].[Benchmark] d
  inner join [filter].[BenchmarkDepartment] b
	on d.[BenchmarkId] = b.[BenchmarkId]
	and b.isactive=1
	inner join [shared].[Location] loc
		on b.locationId = loc.LocationID
	inner join [Audio2_Metadata].[report].[BenchmarkList_Expiries_UTC] rbutc
		on d.name = rbutc.benchmarkname

  where d.isactive =1 


  -- DMO auction days
  delete from [filter].[Filter]
   where originalfilterid=6 and FilterTypeID=1
   and cast(UTCStartDateTime as date) NOT IN 
  ( SELECT [AuctionDate] FROM [Audio2_Metadata].[dbo].[DMOAuctionDays])

  -- DMO close
  delete from [filter].[Filter]
   where originalfilterid=7 and FilterTypeID=1
   and cast(UTCStartDateTime as date) NOT IN 
  ( SELECT [AuctionDate] FROM [Audio2_Metadata].[dbo].[DMOAuctionDays])

  -- Treasury auction days
  delete from [filter].[Filter]
   where originalfilterid=50 and FilterTypeID=1
   and cast(UTCStartDateTime as date) NOT IN 
  ( SELECT [AuctionDate] FROM [Audio2_Metadata].[dbo].TreasuryAuctionDays)

  -- Market numbers for a month per asset class per location

  select d.name, bu.name, d.LocalStartTime, d.LocalStopTime, f.UTCStartDateTime, f.UTCStopDateTime
  from filter.filter f 
  inner join [filter].[MarketEventData] d
	on f.OriginalFilterId = d.MarketEventDataId
  inner join [filter].[MarketEventDataBusinessUnit] b
	on d.[MarketEventDataId] = b.[MarketEventDataId]
	and b.isactive=1
	and b.LocationId = f.locationId
	and f.BusinessUnitId = b.BusinessUnitId
	inner join shared.businessunit bu
		on b.BusinessUnitId = bu.BusinessUnitID
  where f.FilterTypeID = 3 and f.locationId = 2 and UTCStartDateTime >= '20180301' and UTCStartDateTime < '20180401'

  -- Benchmark Window for a month per segment per location

	select distinct d.name, bu.Name, dep.name,  f.UTCStartDateTime, f.UTCStopDateTime
	from filter.filter f 
	inner join [filter].[BenchmarkDepartment] bd
		on f.OriginalFilterId = bd.[BenchmarkId]
		and bd.LocationId = f.LocationId
		and bd.isactive=1
	inner join [filter].[Benchmark] d
		on bd.BenchmarkId = d.BenchmarkID
	inner join shared.department dep
		on bd.DepartmentId = dep.DepartmentID
	inner join shared.businessunit bu
		on dep.BusinessUnitId = bu.BusinessUnitID
	inner join [Audio2_Metadata].[report].[Benchmark_UTC] rbutc
		on d.name = rbutc.benchmarkname
	where f.FilterTypeID = 1 and f.locationId = 2 and f.UTCStartDateTime >= '20180315' and f.UTCStartDateTime < '20180316'
	order by 2,3,1

  -- Benchmark Expiries for a month per segment per location
	select distinct d.name, bu.Name, dep.name,  f.UTCStartDateTime, f.UTCStopDateTime
	from filter.filter f 
	inner join [filter].[BenchmarkDepartment] bd
		on f.OriginalFilterId = bd.[BenchmarkId]
		and bd.LocationId = f.LocationId
		and bd.isactive=1
	inner join [filter].[Benchmark] d
		on bd.BenchmarkId = d.BenchmarkID
	inner join shared.department dep
		on bd.DepartmentId = dep.DepartmentID
	inner join shared.businessunit bu
		on dep.BusinessUnitId = bu.BusinessUnitID
	inner join [Audio2_Metadata].[report].[BenchmarkList_Expiries_UTC] rbutc
		on d.name = rbutc.benchmarkname
	where f.FilterTypeID = 2 and f.locationId = 2 and f.UTCStartDateTime >= '20180315' and f.UTCStartDateTime < '20180316'
	order by 2,3,1

  -- Benchmark Expiries for a month per segment per location
	select distinct d.name, bu.Name, dep.name,  f.UTCStartDateTime, f.UTCStopDateTime
	from filter.filter f 
	inner join [filter].[BenchmarkDepartment] bd
		on f.OriginalFilterId = bd.[BenchmarkId]
		and bd.LocationId = f.LocationId
		and bd.isactive=1
	inner join [filter].[Benchmark] d
		on bd.BenchmarkId = d.BenchmarkID
	inner join shared.department dep
		on bd.DepartmentId = dep.DepartmentID
	inner join shared.businessunit bu
		on dep.BusinessUnitId = bu.BusinessUnitID
	inner join [Audio2_Metadata].[report].[BenchmarkList_Expiries_UTC] rbutc
		on d.name = rbutc.benchmarkname
	where f.FilterTypeID = 2 and f.locationId = 2 and f.UTCStartDateTime >= '20180315' and f.UTCStartDateTime < '20180316'
	order by 2,3,1
