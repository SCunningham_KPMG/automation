
UPDATE dbo.PreAnalytics_Metadata SET 
ParticipantIdentity = RIGHT(RTRIM(FirstName), 4),
participantidentityType = 1 -- TraderId from First Name
,Status = 1
WHERE LastName = 'SBUS' and status=0

UPDATE dbo.PreAnalytics_Metadata SET		
ParticipantIdentity = FirstName,
participantidentityType = 2  -- Mobile
,Status = 1
WHERE (LastName = 'MOB' or DeviceID=30) AND Location='LDN'  and status=0
GO

UPDATE dbo.PreAnalytics_Metadata SET		
ParticipantIdentity = FirstName,
participantidentityType = 2  -- Mobile
,Status = 1
WHERE (MiddleName like '%MOB%' or DeviceID=30) AND Location='NYC'  and status=0
GO

UPDATE dbo.PreAnalytics_Metadata SET		
ParticipantIdentity = Station,
participantidentityType = 2  -- Mobile
,Status = 1
WHERE DeviceID=30 AND Location='HKG' and status=0
GO

UPDATE dbo.PreAnalytics_Metadata SET		
ParticipantIdentity = FirstName,
participantidentityType = 3  -- Cisco
,Status = 1
WHERE LastName = 'EXT' AND Location='LDN' and status=0
GO

UPDATE dbo.PreAnalytics_Metadata SET		
ParticipantIdentity = FirstName,
participantidentityType = 3  -- Cisco
,Status = 1
WHERE ISNull(Station,'') <> '' AND Location='NYC' AND DeviceId=0 and status=0
GO

UPDATE dbo.PreAnalytics_Metadata SET		
ParticipantIdentity = TraderId,
participantidentityType = 4  -- Trader ID from Trader Id Field
,status=1
WHERE ParticipantIdentity IS NULL AND status=0 and traderId is not null
GO

select * from dbo.PreAnalytics_Metadata where  ParticipantIdentity = '0'


-- Trunk labels


UPDATE dbo.PreAnalytics_TrunkLabels SET 
ParticipantIdentity = RIGHT(RTRIM(FirstName), 4),
participantidentityType = 1 -- TraderId from First Name
,Status = 1
WHERE LastName = 'SBUS' and Len(FirstName)=5 and status=0

UPDATE dbo.PreAnalytics_TrunkLabels SET		
ParticipantIdentity = FirstName,
participantidentityType = 2  -- Mobile
,Status = 1
WHERE (LastName = 'MOB' or DeviceTypeId=22) AND Location='LDN'  and status=0
GO

-- NOT DONE
UPDATE dbo.PreAnalytics_TrunkLabels SET		
ParticipantIdentity = FirstName,
participantidentityType = 2  -- Mobile
,Status = 1
WHERE (MiddleName like '%MOB%' or DeviceID=30) AND Location='NYC'  and status=0
GO

UPDATE dbo.PreAnalytics_TrunkLabels SET		
ParticipantIdentity = Station,
participantidentityType = 2  -- Mobile
,Status = 1
WHERE DeviceTypeId=22 AND Location='HKG' and status=0
GO

UPDATE dbo.PreAnalytics_TrunkLabels SET		
ParticipantIdentity = FirstName,
participantidentityType = 3  -- Cisco
,Status = 1
WHERE LastName = 'EXT' AND Location='LDN' and status=0
GO

UPDATE dbo.PreAnalytics_TrunkLabels SET		
ParticipantIdentity = FirstName,
participantidentityType = 3  -- Cisco
,Status = 1
WHERE ISNull(Station,'') <> '' AND Location='NYC' and status=0 and firstname is not null and firstname <> 'TRID'
GO

UPDATE dbo.PreAnalytics_TrunkLabels SET		
ParticipantIdentity = TraderId,
participantidentityType = 4  -- Trader ID from Trader Id Field
,status=1
WHERE ParticipantIdentity IS NULL AND status=0 and traderId is not null
GO

-- get custodian from trunk label

UPDATE dbo.PreAnalytics_Metadata SET 
	ParticipantIdentity = tl.ParticipantIdentity,
	ParticipantIdentityType = tl.ParticipantIdentityType
	--select m.callId, tl.participantidentitytype, tl.ParticipantIdentity, tl.pos
from dbo.PreAnalytics_Metadata m
inner join 
(
	select t.callId, t.location, t.participantidentitytype, t.participantidentity, RANK () OVER (partition by callid, location order by ParticipantIdentity) pos
	from dbo.PreAnalytics_TrunkLabels t
) tl
on m.callid = tl.callid and m.Location = tl.Location
where m.ParticipantIdentity = '0'  and tl.pos = 1 and m.status = 1

UPDATE dbo.PreAnalytics_Metadata SET status = 2 where status = 1

UPDATE [dbo].[PreAnalytics_Metadata]
SET TrunkLabel = t.trunkLabel, DialedNumber = t.dialleddigits, status = 3
--select top 1000 m.CallID, t.TrunkLabel, t.DialledDigits 
from [dbo].[PreAnalytics_Metadata] m
inner join [dbo].[PreAnalytics_TrunkLabels] t on m.callid = t.CallId and m.Location = t.location
and m.ParticipantIdentity = t.ParticipantIdentity and m.Status = 2





-- -------------------------------------------------------------------------------------------------------------
-- Rules

UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = 0 WHERE LastName = 'SBUS'
UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = 1 WHERE LastName = 'MOB' or deviceid=30
UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = 1 WHERE( Middlename like '%MOB%' or deviceid=30) AND Location = 'NY'
UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = '3' + CAST(InitialDeviceTypeID as VARCHAR(10)) WHERE FinalDeviceTypeID IS NULL AND InitialDeviceTypeID IS NOT NULL

UPDATE dbo.PreAnalytics_Metadata Set TraderId = RIGHT(RTRIM(FirstName), 4), Station = RIGHT(RTRIM(Station), 4) WHERE Lastname = 'SBUS'
UPDATE dbo.PreAnalytics_Metadata Set MediaFileName = NULL	WHERE MediaFileName LIKE '%---%' AND status in (2,3)
UPDATE dbo.PreAnalytics_Metadata SET 
			DialedNumber = 
							CASE 
								WHEN CHARINDEX('-',DialedNumber) >= 1 
								THEN LEFT (DialedNumber, CHARINDEX('-',DialedNumber)-1)
								ELSE DialedNumber 
							END 

UPDATE dbo.PreAnalytics_Metadata SET DialedNumber = LTRIM(RTRIM(DialedNumber)) 
UPDATE dbo.PreAnalytics_Metadata SET TrunkLabel = LTRIM(RTRIM(TrunkLabel)) 

UPDATE dbo.PreAnalytics_Metadata SET status = 4 WHERE Duration < 3 AND status in (2,3)
UPDATE dbo.PreAnalytics_Metadata SET status = 5 WHERE FinalDeviceTypeID IN (SELECT DeviceTypeId FROM Voice_Analytics.[audio].[DeviceType] WHERE Name = 'Speaker')

UPDATE dbo.PreAnalytics_Metadata SET status = 5 WHERE 
REPLACE(trunkLabel, ' ', '') IN ('BBC1','BBC2','BLMBTV','CH4','CH5','CNBC','CNN','HSBCTV1','HSBCTV2','ITV','NEWS24','SKYNEWS','RAD4') 
AND status IN (2,3)

-- get rid off honk kong duplicates
UPDATE dbo.PreAnalytics_Metadata SET status = 6 WHERE
CallId IN
(SELECT CallId
FROM
(
	select CallId, TraderId, LocalStartTime, LocalStopTime, InitialDeviceTypeID
	,rank () over (partition by TraderId, LocalStartTime, LocalStopTime, InitialDeviceTypeID	order by mediafilename) as pos
	from dbo.PreAnalytics_Metadata where status in (9) and location = 'HKG'
) a
where pos > 1
) AND status IN (2,3)
-- -------------------------------------------------------------------------------------------------------------



SELECT top 10 *  FROM dbo.PreAnalytics_Metadata where  status IN (9)
