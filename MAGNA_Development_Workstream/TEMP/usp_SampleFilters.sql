USE [Voice_Analytics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*------------------------------------------------------------------------------------------------------
Procedure:      filter.uspSampleFilters
Author:         PBains

Description:   
Extract a random sample of records that match one filter


-- Sample location 1 on the 25th March 2018
EXEC filter.uspSampleFilters 1, '20180325'


Change History
------------------------------
20180314 - PBains - 1.0 : New procedure
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [filter].uspSampleFilters
		@LocationId INT,
		@DataDate	DATE

AS
BEGIN
DECLARE @retStatus INT,
		@filterCategoryId INT,
		@SampleMultiplier DECIMAL (8,4)

SET NOCOUNT ON
SET @retStatus = 0
BEGIN TRY	


END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	EXEC dbo.LogError

END CATCH

RETURN @retStatus

END





