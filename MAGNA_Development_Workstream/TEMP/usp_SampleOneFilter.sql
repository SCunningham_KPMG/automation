USE [Voice_Analytics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*------------------------------------------------------------------------------------------------------
Procedure:      filter.uspSampleOneFilter
Author:         PBains

Description:   
Extract a random sample of records that match one filter


-- Sample location 1 on the 25th March 2018
EXEC filter.uspSampleOneFilter 1, '20180325'


Change History
------------------------------
20180314 - PBains - 1.0 : New procedure
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [filter].uspSampleOneFilter
		@LocationId INT,
		@DataDate	DATE,
		@filterCategoryId INT,
		@SampleMultiplier DECIMAL (8,4)
AS
BEGIN
DECLARE @retStatus INT

SET NOCOUNT ON
SET @retStatus = 0
BEGIN TRY	

	-- Find the number of calls per segment and multiply by the samplemultiplier to see how many calls should be marked for the filter.
	WITH cteCallsPerSegemnt AS
	(
		SELECT sd.BusinessUnitID, sd.DepartmentID, count(*) TotalSegmentCalls, CEILING(count(*) * @SampleMultiplier) CallsToSample
		FROM audio.call c
		INNER JOIN audio.CallCustodianIdentity cci
			ON c.CallID = cci.CallID
		INNER JOIN shared.CustodianIdentity sci
			ON cci.CustodianIdentityID = sci.CustodianIdentityID
		INNER JOIN shared.Custodian sc
			ON sci.CustodianID = sc.CustodianID
		INNER JOIN shared.Department sd
			ON sc.DepartmentID = sd.DepartmentID
		WHERE CAST (c.localstarttime as date) = @DataDate AND c.locationId = @LocationId AND ActualFilterUsedForIngest IS NULL
		GROUP BY sd.BusinessUnitID, sd.DepartmentID
	),

	cteCallsPerFilterTypePerSegemnt AS
	(
		SELECT sd.BusinessUnitID, sd.DepartmentID, c.CallID, ROW_NUMBER () OVER (PARTITION BY sd.BusinessUnitID, sd.DepartmentID ORDER BY NewId()) Pos
		FROM audio.call c
		INNER JOIN audio.CallFiltersSatisfied cfs
			ON c.CallID = cfs.CallID
		INNER JOIN filter.FilterType ft 
			ON cfs.FilterTypeId = ft.FilterTypeID
			AND ft.filterCategoryId  = @filterCategoryId
		INNER JOIN audio.CallCustodianIdentity cci
			ON c.CallID = cci.CallID
		INNER JOIN shared.CustodianIdentity sci
			ON cci.CustodianIdentityID = sci.CustodianIdentityID
		INNER JOIN shared.Custodian sc
			ON sci.CustodianID = sc.CustodianID
		INNER JOIN shared.Department sd
			ON sc.DepartmentID = sd.DepartmentID
		WHERE CAST (c.localstarttime as date) = @DataDate AND c.locationId = @LocationId AND ActualFilterUsedForIngest IS NULL
	)

	UPDATE audio.[call]
	SET ActualFilterUsedForIngest = @filterCategoryId
	--SELECT --b.businessunitid, b.departmentid, b.callid, s.CallsToSample, s.TotalSegmentCalls
	FROM cteCallsPerFilterTypePerSegemnt b
	INNER JOIN cteCallsPerSegemnt s 
		ON b.BusinessUnitID = s.businessUnitId
		AND b.DepartmentID = s.departmentId
	INNER JOIN audio.[call] c 
		ON b.callId = c.callId
	WHERE b.Pos <= s.CallsToSample

END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	EXEC dbo.LogError

END CATCH

RETURN @retStatus

END





