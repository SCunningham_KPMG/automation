select c.custodianId, c.isinscopetraderfollow, c.isactive, c.departmentid, bu.BusinessUnitID
from [shared].[Custodian] c
inner join shared.Department d
	on c.DepartmentID = d.DepartmentID
inner join shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID

select c.locationId, bu.BusinessUnitID, c.departmentid, count(*) NumberCustodians, COUNT(*)/40 + 1 NumberToFollow
from [shared].[Custodian] c
inner join shared.Department d
	on c.DepartmentID = d.DepartmentID
inner join shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID
group by c.locationId, bu.BusinessUnitID, c.departmentid


select sc.LocationID, d.BusinessUnitID, d.departmentId, sc.CustodianID, MAX(ISNULL(fcdf.DateFollowed,'19000101')) LastFollowed
from audio.Call c
inner join [audio].[CallCustodianIdentity] ci
	on c.CallID = ci.CallID
inner join shared.CustodianIdentity sci
	on ci.CustodianIdentityID = sci.CustodianIdentityID
inner join shared.Custodian sc
	on sci.CustodianID = sc.CustodianID
	and sc.IsInScopeTraderFollow = 1
	and sc.IsActive = 1
inner join shared.Department d
	on sc.DepartmentID = d.DepartmentID
left join filter.custodiandatefollowed fcdf
	on sc.CustodianID = fcdf.custodianId
where c.LocalStartTime >= '20180214' and c.LocalEndTime < '20180215'
group by sc.LocationID, d.BusinessUnitID, d.departmentId, sc.CustodianID







select top 10  c.callId, sc.CustodianID, sc.LocationID, d.departmentId, d.BusinessUnitID, c.LocalStartTime, c.LocalEndTime
from audio.Call c
inner join [audio].[CallCustodianIdentity] ci
	on c.CallID = ci.CallID
inner join shared.CustodianIdentity sci
	on ci.CustodianIdentityID = sci.CustodianIdentityID
inner join shared.Custodian sc
	on sci.CustodianID = sc.CustodianID
	and sc.IsInScopeTraderFollow = 1
	and sc.IsActive = 1
inner join shared.Department d
	on sc.DepartmentID = d.DepartmentID
left join filter.custodiandatefollowed fcdf
	on sc.CustodianID = fcdf.custodianId
where c.LocalStartTime >= '20180214' and c.LocalEndTime < '20180215'

/*
location, date, metadata, trunklabels, callfollow, benchmark, marketdata, callprofile, rendom, electronic, lexicon, sampled, loadfile
*/