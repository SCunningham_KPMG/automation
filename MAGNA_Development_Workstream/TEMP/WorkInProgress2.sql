
DECLARE @dataDate date
DECLARE @locationId INT
DECLARE @sampleSize INT

select top 1 @dataDate = datadate, @locationId = locationId from [dbo].[LocationDataStatus] where phase2filteringComplete is not null order by datadate, locationId

SELECT @sampleSize= COUNT(*) FROM audio.call WHERE locationId = @locationId AND cast(localstarttime as date) = @dataDate AND status=77
SELECT @dataDate, @locationId, @sampleSize

-- Call Follow 100%
--UPDATE audio.call SET ActualFilterUsedForIngest = 5
select c.*
FROM audio.call c
INNER JOIN audio.CallFiltersSatisfied cfs
	ON c.CallID = cfs.CallID
	AND cfs.FilterTypeId = 5
WHERE CAST (c.localstarttime as date) = @dataDate AND c.locationId = @locationId
AND ActualFilterUsedForIngest IS NULL

-- Calls per segment
SELECT sd.BusinessUnitID, sd.DepartmentID, count(*), CEILING(count(*) * 0.005) HALFPERCENTTOSAMPLE
FROM audio.call c
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity sci
	ON cci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN shared.Custodian sc
	ON sci.CustodianID = sc.CustodianID
INNER JOIN shared.Department sd
	ON sc.DepartmentID = sd.DepartmentID
WHERE CAST (c.localstarttime as date) = '20180315' AND c.locationId = 2 AND ActualFilterUsedForIngest IS NULL
GROUP BY sd.BusinessUnitID, sd.DepartmentID
order by 1,2




-- Benchmark Calls
SELECT sd.BusinessUnitID, sd.DepartmentID, count(*) NCalls
FROM audio.call c
INNER JOIN audio.CallFiltersSatisfied cfs
	ON c.CallID = cfs.CallID
INNER JOIN filter.FilterType ft 
	ON cfs.FilterTypeId = ft.FilterTypeID
	AND ft.filterCategoryId  = 3
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity sci
	ON cci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN shared.Custodian sc
	ON sci.CustodianID = sc.CustodianID
INNER JOIN shared.Department sd
	ON sc.DepartmentID = sd.DepartmentID
WHERE CAST (c.localstarttime as date) = '20180315' AND c.locationId = 2 AND ActualFilterUsedForIngest IS NULL
GROUP BY sd.BusinessUnitID, sd.DepartmentID
order by 1,2


DECLARE @dataDate date
DECLARE @locationId INT
DECLARE @sampleMultiplier DECIMAL (5,3)
SET @sampleMultiplier = 0.5

select top 1 @dataDate = datadate, @locationId = locationId from [dbo].[LocationDataStatus] where phase2filteringComplete is not null order by datadate, locationId
SELECT @dataDate, @locationId

SELECT --TOP(@sampleMultiplier) PERCENT 
c.callId
FROM audio.call c
INNER JOIN audio.CallFiltersSatisfied cfs
	ON c.CallID = cfs.CallID
INNER JOIN filter.filterType ft
	ON cfs.FilterTypeId = ft.FilterTypeID
INNER JOIN filter.FilterCategory fc
	on ft.FilterCategoryId = fc.filterCategoryId
	and fc.filterCategoryId = 3
WHERE CAST (c.localstarttime as date) = @dataDate AND c.locationId = @locationId
AND c.ActualFilterUsedForIngest IS NULL


SELECT c.locationId, cast(c.LocalStartTime as date), cfs.FilterTypeId, count(*)
FROM audio.call C
INNER JOIN [audio].[CallFiltersSatisfied] cfs
	ON c.callId = cfs.CallID
group by  c.locationId, cast(c.LocalStartTime as date), cfs.FilterTypeId
order by 2,1,3






