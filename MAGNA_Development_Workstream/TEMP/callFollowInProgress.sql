	SELECT lf.custodianId, lf.BusinessUnitID, lf.DepartmentID, pre.NumberOfCalls
	FROM
	(
		select 
			lf.LocationId, lf.BusinessUnitId, lf.DepartmentId, lf.custodianId, 
			row_number() over (partition by lf.LocationId, lf.BusinessUnitId, lf.DepartmentId order by lf.lastfollowed) seq
		from  [working].[TF_LastFollowed] lf
	) lf 
	left join [working].[TF_NumberToFollow] ntf
		on lf.locationId = ntf.locationId
		and lf.businessunitid = ntf.businessunitid
		and lf.departmentid = ntf.departmentid

	left join 
	(
		SELECT ci.CustodianID, count(*) as NumberOfCalls
		FROM [audio].[CallCustodianIdentity] cci
		inner join audio.call c
			ON cci.callId = c.CallId
		inner join shared.CustodianIdentity ci
			ON cci.CustodianIdentityID = ci.CustodianIdentityId
		WHERE CAST(c.LocalStartTime as date) = '20180315' AND c.locationId=2
		GROUP BY ci.CustodianID
	) pre
	ON lf.CustodianId = pre.CustodianID
	where lf.seq <= ntf.NumberToFollow
	order by 2,3,1





select BusinessUnitID, DepartmentID from [working].[TF_LastFollowed] where locationId = 2 group by BusinessUnitID, DepartmentID

