USE [Voice_Data]
GO

/****** Object:  Table [dbo].[PreAnalytics_Metadata]    Script Date: 16/03/2018 11:34:56 ******/
DROP TABLE [dbo].[PreAnalytics_Metadata]
GO

/****** Object:  Table [dbo].[PreAnalytics_Metadata]    Script Date: 16/03/2018 11:34:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreAnalytics_Metadata](
	[Location] [varchar](20) NOT NULL,
	[CallID] [bigint] NOT NULL,
	[CallParentID] [bigint] NULL,
	[TraderId] [int] NULL,
	[LocalStartTime] [datetime] NULL,
	[LocalStopTime] [datetime] NULL,
	[UTCStartTime] [datetime] NULL,
	[UTCStopTime] [datetime] NULL,
	[Duration] [int] NULL,
	[InitiatorTRID] [varchar](500) NULL,
	[MediaFileName] [varchar](8000) NULL,
	[MediaPath] [varchar](500) NULL,
	[MediaFileId] [int] NULL,
	[TrunkLabel] [varchar](500) NULL,
	[DeviceId] [int] NULL,
	[InitialDeviceTypeID] [int] NULL,
	[FinalDeviceTypeID] [int] NULL,
	[CallDirection] [varchar](500) NULL,
	[Station] [varchar](500) NULL,
	[PhoneNumber] [varchar](500) NULL,
	[DialedNumber] [varchar](500) NULL,
	[ParticipantIdentityType] [int] NULL,
	[ParticipantIdentity] [varchar](500) NULL,
	[CopiedToAnalyticsDateTime] [datetime] NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[FirstName] [varchar](500) NULL,
	[LastName] [varchar](500) NULL,
	[MiddleName] [varchar](500) NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_PreAnalytics_Metadata_DateCreated]  DEFAULT (getdate()),
	[CallingLineIdentifier] [varchar](50) NULL,
 CONSTRAINT [PK_PreAnalytics_Metadata] PRIMARY KEY CLUSTERED 
(
	[Location] ASC,
	[CallID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


