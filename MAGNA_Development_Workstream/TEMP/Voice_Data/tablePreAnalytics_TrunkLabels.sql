USE [Voice_Data]
GO

/****** Object:  Table [dbo].[PreAnalytics_TrunkLabels]    Script Date: 16/03/2018 11:36:22 ******/
DROP TABLE [dbo].[PreAnalytics_TrunkLabels]
GO

/****** Object:  Table [dbo].[PreAnalytics_TrunkLabels]    Script Date: 16/03/2018 11:36:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreAnalytics_TrunkLabels](
	[CallId] [bigint] NOT NULL,
	[FirstName] [varchar](500) NULL,
	[LastName] [varchar](500) NULL,
	[TraderId] [int] NULL,
	[TrunkLabel] [varchar](500) NULL,
	[DeviceTypeId] [int] NULL,
	[Location] [varchar](3) NOT NULL,
	[DialledDigits] [varchar](50) NULL,
	[Station] [varchar](50) NULL,
	[ParticipantIdentityType] [int] NULL,
	[ParticipantIdentity] [varchar](500) NULL,
	[CopiedToAnalytics] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [PreAnalytics_TrunkLabels_CopyStatus]  DEFAULT ((0)),
	[CreatedDateTime] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_PreAnalytics_TrunkLabels] PRIMARY KEY CLUSTERED 
(
	[Location] ASC,
	[CallID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


