USE [Voice_Data]
GO

/****** Object:  StoredProcedure [dbo].[uspPreAnalytics]    Script Date: 16/03/2018 11:32:19 ******/
DROP PROCEDURE [dbo].[uspPreAnalytics]
GO

/****** Object:  StoredProcedure [dbo].[uspPreAnalytics]    Script Date: 16/03/2018 11:32:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*------------------------------------------------------------------------------------------------------
Procedure:      dbo.uspPreAnalytics
Author:         PBains

Description:   
Moves the data from staging to preAnalytics and then prepares the data for transfer to Analytics
Call:
EXEC dbo.uspPreAnalytics

Return Value:
	0 = success
	>1 =Error. Look at Voics_Analytics.dbo.ErrorLog

Change History
------------------------------
20180313 1.00	PB	Initial version
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[uspPreAnalytics]
AS
BEGIN
 
	SET NOCOUNT ON 
	DECLARE @retStatus INT
	SET @retStatus = 0

BEGIN TRY
	EXEC @retStatus = [dbo].[uspCopyToPreAnalytics]
	EXEC @retStatus = [dbo].[uspPrepareForAnalytics]
	SET @retStatus = 0
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	EXEC dbo.LogError
END CATCH

RETURN @retStatus

END





GO


