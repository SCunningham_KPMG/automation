USE [Voice_Data]
GO

/****** Object:  StoredProcedure [dbo].[uspPrepareForAnalytics]    Script Date: 16/03/2018 11:32:38 ******/
DROP PROCEDURE [dbo].[uspPrepareForAnalytics]
GO

/****** Object:  StoredProcedure [dbo].[uspPrepareForAnalytics]    Script Date: 16/03/2018 11:32:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*------------------------------------------------------------------------------------------------------
Procedure:      dbo.uspPrepareForAnalytics
Author:         PBains

Description:   
Prepares the data for copying to the analytics model in the Voice_Analytics database.


Call:
EXEC dbo.uspPrepareForAnalytics

Return Value:
	0 = success
	>0 = error. Look in Voice_Analytics.dbo.ErrorLog

Change History
------------------------------
20180313 1.00	PB	Initial version
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[uspPrepareForAnalytics]
AS
BEGIN
 
	SET NOCOUNT ON 
	DECLARE @retStatus INT
	DECLARE @currentDateTime datetime

	SET @currentDateTime = getdate()
	SET @retStatus = 0

BEGIN TRY
-- ***********************************************************************************************************************************
--	Get Custodian from metadata
-- ***********************************************************************************************************************************
	SET @retStatus = 1
--	Custodian identified by TraderId from First Name
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = RIGHT(RTRIM(FirstName), 4), participantidentityType = 1 ,Status = 21
		WHERE LastName = 'SBUS' and status= 20

	SET @retStatus = 2
--	Custodian identified by Mobile phone from LDN
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = FirstName, participantidentityType = 2 , Status = 21
		WHERE (LastName = 'MOB' or DeviceID=30) AND Location='LDN'  and status=20

	SET @retStatus = 3
--	Custodian identified by Mobile phone from NYC
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = FirstName, participantidentityType = 2, Status = 21
		WHERE (MiddleName like '%MOB%' or DeviceID=30) AND Location='NYC'  and status=20

	SET @retStatus = 4
--	Custodian identified by Mobile phone from HKG
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = Station, participantidentityType = 2, Status = 21
		WHERE DeviceID=30 AND Location='HKG' and status=20

	SET @retStatus = 5
--	Custodian identified by Cisco from LDN
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = FirstName, participantidentityType = 3, Status = 21
		WHERE LastName = 'EXT' AND Location='LDN' and status=20

	SET @retStatus = 6
--	Custodian identified by Cisco from NYC
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = FirstName, participantidentityType = 3, Status = 21
		WHERE ISNull(Station,'') <> '' AND Location='NYC' AND DeviceId=0 and status=20

	SET @retStatus = 7
-- Custodian identified by Trader ID from Trader Id Field
	UPDATE dbo.PreAnalytics_Metadata SET ParticipantIdentity = TraderId, participantidentityType = 4, status=21
		WHERE ParticipantIdentity IS NULL AND status=20 and traderId is not null

-- ***********************************************************************************************************************************
-- Get Custodian for Trunk label data
-- ***********************************************************************************************************************************

	SET @retStatus = 8
--	Custodian identified by TraderId from First Name
	UPDATE dbo.PreAnalytics_TrunkLabels SET ParticipantIdentity = RIGHT(RTRIM(FirstName), 4), participantidentityType = 1 ,Status = 21
		WHERE LastName = 'SBUS' and Len(FirstName)=5 and status= 20

	SET @retStatus = 9
--	Custodian identified by Mobile phone from LDN
	UPDATE dbo.PreAnalytics_TrunkLabels SET ParticipantIdentity = FirstName, participantidentityType = 2 , Status = 21
		WHERE (LastName = 'MOB' or DeviceTypeId=22) AND Location='LDN'  and status=20

	SET @retStatus = 10
--	Custodian identified by Mobile phone from HKG
	UPDATE dbo.PreAnalytics_TrunkLabels SET ParticipantIdentity = Station, participantidentityType = 2, Status = 21
		WHERE DeviceTypeId=22 AND Location='HKG' and status=20

	SET @retStatus = 11
--	Custodian identified by Cisco from LDN
	UPDATE dbo.PreAnalytics_TrunkLabels SET ParticipantIdentity = FirstName, participantidentityType = 3, Status = 21
		WHERE LastName = 'EXT' AND Location='LDN' and status=20

	SET @retStatus = 12
--	Custodian identified by Cisco or Mobile from NYC. We cannot differentiate between mobile/cisco yet, but we will find out later
	UPDATE dbo.PreAnalytics_TrunkLabels SET ParticipantIdentity = FirstName, participantidentityType = 3, Status = 21
		WHERE ISNull(Station,'') <> '' AND Location='NYC' and status=20 and firstname is not null and firstname <> 'TRID'

	SET @retStatus = 13
-- Custodian identified by Trader ID from Trader Id Field
	UPDATE dbo.PreAnalytics_TrunkLabels SET ParticipantIdentity = TraderId, participantidentityType = 4, status=21
		WHERE ParticipantIdentity IS NULL AND status=20 and traderId is not null

-- ***********************************************************************************************************************************
-- Populate metadata custodian with custodian from trunk label as thecustodian did not have a valid value, i.e. it was zero.
-- ***********************************************************************************************************************************

	SET @retStatus = 14
	UPDATE dbo.PreAnalytics_Metadata SET 
		ParticipantIdentity = tl.ParticipantIdentity, ParticipantIdentityType = tl.ParticipantIdentityType
	from dbo.PreAnalytics_Metadata m
	inner join 
	(
		select t.callId, t.location, t.participantidentitytype, t.participantidentity, RANK () OVER (partition by callid, location order by ParticipantIdentity) pos
		from dbo.PreAnalytics_TrunkLabels t
	) tl
	on m.callid = tl.callid and m.Location = tl.Location
	where m.ParticipantIdentity = '0'  and tl.pos = 1 and m.status = 21

	SET @retStatus = 15
	UPDATE dbo.PreAnalytics_Metadata SET status = 22 WHERE status = 21

-- ***********************************************************************************************************************************
-- Add trunk label and dialled digits from trunk table to metadata table. After this, records processed here no longer need the trunk record.
-- ***********************************************************************************************************************************

	SET @retStatus = 16
	UPDATE dbo.PreAnalytics_Metadata
		SET TrunkLabel = t.trunkLabel, DialedNumber = t.dialleddigits, status = 23
	from dbo.PreAnalytics_Metadata m
	inner join dbo.PreAnalytics_TrunkLabels t on m.callid = t.CallId and m.Location = t.location
		and m.ParticipantIdentity = t.ParticipantIdentity and m.Status = 22

-- ***********************************************************************************************************************************
-- Apply rules e.g. modifying device typeid, removing duplicates, calls < n seconds, speaker calls etc.
-- ***********************************************************************************************************************************
	SET @retStatus = 17
	UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = 0 WHERE LastName = 'SBUS' and status in (22,23)
	UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = 1 WHERE (LastName = 'MOB' or deviceid=30) and status in (22,23)
	UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = 1 WHERE( Middlename like '%MOB%' or deviceid=30) AND Location = 'NY' and status in (22,23)
	UPDATE dbo.PreAnalytics_Metadata Set FinalDeviceTypeID = '3' + CAST(InitialDeviceTypeID as VARCHAR(10)) 
		WHERE FinalDeviceTypeID IS NULL AND InitialDeviceTypeID IS NOT NULL and status in (22,23)

	SET @retStatus = 18
	UPDATE dbo.PreAnalytics_Metadata Set TraderId = RIGHT(RTRIM(FirstName), 4), Station = RIGHT(RTRIM(Station), 4) WHERE Lastname = 'SBUS' and status IN (22,23)

	SET @retStatus = 19
	UPDATE dbo.PreAnalytics_Metadata Set MediaFileName = NULL	WHERE MediaFileName LIKE '%---%' AND status IN (22,23)

	SET @retStatus = 20
	UPDATE dbo.PreAnalytics_Metadata SET 
		DialedNumber = 
			CASE 
				WHEN CHARINDEX('-',DialedNumber) >= 1 
				THEN LEFT (DialedNumber, CHARINDEX('-',DialedNumber)-1)
				ELSE DialedNumber 
			END 
		WHERE status IN (22,23)

	UPDATE dbo.PreAnalytics_Metadata SET DialedNumber = LTRIM(RTRIM(DialedNumber)) WHERE status IN (22,23)

	SET @retStatus = 21
	UPDATE dbo.PreAnalytics_Metadata SET TrunkLabel = LTRIM(RTRIM(TrunkLabel)) WHERE status IN (22,23)

	SET @retStatus = 22
--	Calls less than 3 seconds
	UPDATE dbo.PreAnalytics_Metadata SET status = 26 WHERE Duration < 3 AND status IN (22,23)

	SET @retStatus = 23
	UPDATE dbo.PreAnalytics_Metadata SET status = 27 
		WHERE FinalDeviceTypeID IN (SELECT DeviceTypeId FROM Voice_Analytics.[audio].[DeviceType] WHERE Name = 'Speaker') AND status IN (22,23)

	SET @retStatus = 24
	UPDATE dbo.PreAnalytics_Metadata SET status = 28 WHERE 
			REPLACE(trunkLabel, ' ', '') IN ('BBC1','BBC2','BLMBTV','CH4','CH5','CNBC','CNN','HSBCTV1','HSBCTV2','ITV','NEWS24','SKYNEWS','RAD4') 
			AND status IN (22,23)

	-- get rid off honk kong duplicates
	SET @retStatus = 25
	UPDATE dbo.PreAnalytics_Metadata SET status = 29 WHERE
	CallId IN
	(SELECT CallId
	FROM
	(
		select CallId, TraderId, LocalStartTime, LocalStopTime, InitialDeviceTypeID
		,rank () over (partition by TraderId, LocalStartTime, LocalStopTime, InitialDeviceTypeID	order by mediafilename) as pos
		from dbo.PreAnalytics_Metadata where status IN (22,23) and location = 'HKG'
	) a
	where pos > 1
	) AND status IN (22,23)

	SET @retStatus = 26
	UPDATE dbo.PreAnalytics_Metadata SET status = 41 where status  = 22
	UPDATE dbo.PreAnalytics_Metadata SET status = 40 where status  = 23

	SET @retStatus = 0
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	EXEC dbo.LogError
END CATCH


RETURN @retStatus

END





GO


