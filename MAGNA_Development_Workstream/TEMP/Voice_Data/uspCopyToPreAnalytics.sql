USE [Voice_Data]
GO

/****** Object:  StoredProcedure [dbo].[uspCopyToPreAnalytics]    Script Date: 16/03/2018 11:31:20 ******/
DROP PROCEDURE [dbo].[uspCopyToPreAnalytics]
GO

/****** Object:  StoredProcedure [dbo].[uspCopyToPreAnalytics]    Script Date: 16/03/2018 11:31:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*------------------------------------------------------------------------------------------------------
Procedure:      dbo.spCopyToPreAnalytics
Author:         PBains

Description:   
Copies data from staging to pre-analytics as and when it arrives.
If the status is zero it hasn't been copied before. Sets it to 1, does the copy and then sets it to 2.
Copies metadata first and then trunk label data.

Call:
EXEC dbo.spCopyToPreAnalytics

Return Value:
	0 = success
	1 - 4: metadata copy failed. Look at Voics_Analytics.dbo.ErrorLog
	5 - 8: trunk label copy failed. Look at Voics_Analytics.dbo.ErrorLog

Change History
------------------------------
20180313 1.00	PB	Initial version
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[uspCopyToPreAnalytics]
AS
BEGIN
 
	SET NOCOUNT ON 
	DECLARE @retStatus INT
	DECLARE @currentDateTime datetime
	DECLARE @currentDateTime2 datetime

	SET @currentDateTime = getdate()
	SET @retStatus = 0
BEGIN TRY
	BEGIN TRAN

	SET @retStatus = 1
	UPDATE dbo.Staging_Metadata SET CopyStatus = 1 
	FROM dbo.Staging_Metadata m
	INNER JOIN [dbo].[DataflowStatus] dfs
		ON m.FileId = dfs.FileId
		AND dfs.MetadataCopiedToStaging IS NOT NULL
		AND dfs.TrunkdataCopiedToStaging IS NOT NULL
		AND dfs.MetadataCopiedToPreAnalytics IS NULL
	WHERE CopyStatus = 0 

	SET @retStatus = 2
	INSERT INTO dbo.PreAnalytics_Metadata (
		Location
		,CallID
		,CallParentID
		,TraderId
		,LocalStartTime
		,LocalStopTime
		,UTCStartTime
		,UTCStopTime
		,Duration
		,InitiatorTRID
		,MediaFileName
		,MediaPath
		,MediaFileId
		,TrunkLabel
		,DeviceId
		,InitialDeviceTypeID
		,FinalDeviceTypeID
		,CallDirection
		,Station
		,PhoneNumber
		,DialedNumber
		,ParticipantIdentityType
		,ParticipantIdentity
		,CopiedToAnalyticsDateTime
		,Status
		,Firstname
		,MiddleName
		,LastName
		,CallingLineIdentifier)
    
	SELECT 
		Location
		,CAST(CallId as BIGINT)
		,CAST(CrossCompleteID as BIGINT)
		,TraderId
		,LocalStartTime
		,LocalStopTime
		,UTCStartTime
		,UTCStopTime		
		,DATEPART(HOUR, cast (duration as time)) *3600 + DATEPART(MINUTE, cast (duration as time)) *60 + DATEPART(SECOND, cast (duration as time))
		,CallInitiatorTRID
		,MediaFileName
		,MediaFilePath
		,MediaFileId
		,Null as TrunkLabel
		,ISNULL(ParticipantDeviceID,DeviceID) AS DeviceID
		,ISNULL(ParticipantDeviceTypeID,DeviceTypeID) AS InitialDeviceTypeID
		,Null as FinalDeviceTypeId
		,CallDirection
		,ISNULL(Station,ParticipantStation) AS Station
		,ISNULL(PhoneNumber,ParticipantPhoneNumber) AS PhoneNumber
		,DialedNumber
		,null
		,null
		,getdate()
		,20
		,Firstname
		,MiddleName
		,LastName
		,CallerId
	FROM  dbo.Staging_Metadata
	WHERE CopyStatus = 1


	SET @retStatus = 4

	UPDATE dbo.Staging_TrunkLabel 
	SET CopyStatus = 1 
	FROM dbo.Staging_TrunkLabel tl
	INNER JOIN dbo.Staging_Metadata m
		ON tl.CallID = m.CallID
	INNER JOIN [dbo].[DataflowStatus] dfs
		ON m.FileId = dfs.FileId
		AND dfs.MetadataCopiedToStaging IS NOT NULL
		AND dfs.TrunkdataCopiedToStaging IS NOT NULL
		AND dfs.TrunkDataCopiedToPreAnalytics IS NULL
	WHERE tl.CopyStatus = 0

	SET @retStatus = 6
	INSERT INTO dbo.PreAnalytics_TrunkLabels
           (CallId
           ,FirstName
           ,LastName
           ,TraderId
           ,TrunkLabel
           ,DeviceTypeId
           ,Location
           ,DialledDigits
           ,Station
		   ,[Status]
	)
	SELECT DISTINCT CallID, FirstName, LastName,TraderId,TrunkLabel, deviceTypeID, Location, DialedDigits, Station, 20 
	FROM dbo.Staging_TrunkLabel WHERE CopyStatus = 1

	SET @retStatus = 7
	SET @currentDateTime2 = getdate()


	UPDATE dbo.DataflowStatus SET MetadataCopiedToPreAnalytics = @currentDateTime , TrunkdataCopiedToPreAnalytics = @currentDateTime2
	WHERE FileId IN  
		(SELECT DISTINCT FileId FROM dbo.Staging_Metadata WHERE CopyStatus = 1)

	SET @retStatus = 3
	UPDATE dbo.Staging_Metadata SET CopyStatus = 2, CopiedToPreAnalytics = @currentDateTime WHERE CopyStatus = 1 
	UPDATE dbo.Staging_TrunkLabel SET CopyStatus = 2, CopiedToPreAnalytics = @currentDateTime2 WHERE CopyStatus = 1 


	SET @retStatus = 8
	COMMIT
	SET @retStatus = 0
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	EXEC dbo.LogError
END CATCH

RETURN @retStatus

END




GO


