
USE [Voice_Analytics]
GO

INSERT INTO [filter].[MarketEventData]
           ([Name]
           ,[MarketEventDataTypeD]
           ,[MarketEventDataDay]
           ,[LocalStartTime]
           ,[LocalStopTime]
           ,[IsActive]
)
SELECT DISTINCT [NumbersDescription], 1, [NumbersDay], [LocalStartTime], [LocalEndTime], 1 
FROM [Audio2_Metadata].[dbo].[MarketNumbers_Parameters] ORDER BY 3,1,4

INSERT INTO [filter].[MarketEventData]
           ([Name]
           ,[MarketEventDataTypeD]
           ,[MarketEventDataDay]
           ,[LocalStartTime]
           ,[LocalStopTime]
           ,[IsActive]
)
SELECT DISTINCT [EventDescription], 2, [EventDay], [LocalStartTime], [LocalEndTime], 1 
FROM [Audio2_Metadata].[dbo].[MarketEvent_Parameters] ORDER BY 3,1,4

UPDATE [filter].[MarketEventData] SET [MarketEventDataTypeD] = [MarketEventDataTypeD] + 2