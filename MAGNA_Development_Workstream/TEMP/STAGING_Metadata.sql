-- New table  STAGING_Metadata

SELECT TOP 1000 
       [SegmentID] as CallId
      ,[SegmentCallDirectionTypeID] as CallDirection
      ,[Station]
      ,[ParticipantStation]
      ,[Phone-Number] as PhoneNumber
      ,[ParticipantPhone-Number] as ParticipantPhoneNumber
	  ,ParticipantAgentID as TraderId
      ,[DeviceID]
      ,[ParticipantDeviceID]
      ,[DeviceTypeID]
      ,[ParticipantDeviceTypeID]
	  ,[ParticipantCTIAgentName] ParticipantCTITraderName
      ,[TrunkGroup]
      ,[ParticipantTrunkGroup]
      ,[TrunkNumber]
      ,[ParticipantTrunkNumber]
	  ,[FirstName]
	  ,[MiddleName]
	  ,[LastName]
	  ,[FullName]
	  ,[SegmentInitiatorUserID] AS CallInitiatorTRID
	  ,[InternalSegmentClientStartTime] AS LocalStartTime
	  ,[InternalSegmentClientStopTime] AS LocalStopTime
	  ,[SegmentStartTime] AS UTCStartTime
	  ,[SegmentStopTime] AS UTCStopTime
	  ,[SegmentDuration] AS Duration
	  ,[CrossCompleteID]
	  ,[SegmentDialedNumber] AS DialedNumber
      ,[FileId] as MediaFileId
      ,[Path] as MediaFilePath
      ,[FileName] as MediaFileName
	  ,[Status]	
	  ,RecordingLogger	
	  ,SegmentSwitchCallID	
	  ,CallerId
	  ,'HKG' as Location -- 'HKG' , 'LDN', 'NYC'
	  ,1 as ParticipantIdentityType -- Trader Id, Phone 1 , Phone 2, Email - Work, Mobile etc
	  ,'' as ParticipantIdentity -- the email address, phone number, trader id etc.
	  ,getdate() as CreatedDateTime -- The datetime this record was created in staging
  FROM [Voice_Data].[dbo].[HKG_MetaData]


