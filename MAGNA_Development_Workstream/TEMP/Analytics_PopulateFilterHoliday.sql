
USE [Voice_Analytics]
GO

INSERT INTO [filter].[Holiday]
           ([HolidayDateFull],[HolidayDate],[Active],[LocationID])
SELECT [HolidayDateFull], [HolidayDate], 1, 1 FROM [Audio2_Metadata].[dbo].[Holidays] WHERE IsLdn = 1
INSERT INTO [filter].[Holiday]
           ([HolidayDateFull],[HolidayDate],[Active],[LocationID])
SELECT [HolidayDateFull], [HolidayDate], 1, 2 FROM [Audio2_Metadata].[dbo].[Holidays] WHERE IsNyc = 1
INSERT INTO [filter].[Holiday]
           ([HolidayDateFull],[HolidayDate],[Active],[LocationID])
SELECT [HolidayDateFull], [HolidayDate], 1, 3 FROM [Audio2_Metadata].[dbo].[Holidays] WHERE IsHKG = 1

GO

