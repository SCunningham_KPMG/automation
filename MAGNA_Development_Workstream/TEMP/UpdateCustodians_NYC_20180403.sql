-- Roumeliotis,George
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3476622098',2238,'20180403',NULL,'NYC')
-- Yang,Hongya
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3476958892',2060,'20180403',NULL,'NYC')
--Savidis,George
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3476622486',1380,'20180403',NULL,'NYC')
--Patel,Amit
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463552179',1385,'20180403',NULL,'NYC')
--Healy,Joe
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3476622111',1397,'20180403',NULL,'NYC')
--Pichichero,Brian
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6468873962',1379,'20180403',NULL,'NYC')
--Smith,Thomas
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3476622109',1063,'20180403',NULL,'NYC')
--Shah,Amish
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463741480',1083,'20180403',NULL,'NYC')
--Zakir,Mehedi
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463740545',2225,'20180403',NULL,'NYC')
--Kiraly,Matthew
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463741464',2062,'20180403',NULL,'NYC')
--Paulus,Michael
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6467994680',1384,'20180403',NULL,'NYC')
--Blunt,Richard
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3476622101',1050,'20180403',NULL,'NYC')
--Rutkovsky,Benjamin
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6466099686',2236,'20180403',NULL,'NYC')
--Gardella,Christopher
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6466814958',2065,'20180403',NULL,'NYC')
--Kolahdooz,Mousa
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463741528',2066,'20180403',NULL,'NYC')
--Habib,Joseph
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3475719457',2061,'20180403',NULL,'NYC')
--Ascenso,Fernando
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463740610',1133,'20180403',NULL,'NYC')
--Gilpin,Steven
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6467620158',1131,'20180403',NULL,'NYC')
--Soustra,Frederic
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6463552641',1346,'20180403',NULL,'NYC')
UPDATE [dbo].[CustodianMobileIdentity] SET EndDate = '20180403' WHERE MobileIdentityID=749
--Verdon,Christopher
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('3478995058',1121,'20180403',NULL,'NYC')
UPDATE [dbo].[CustodianMobileIdentity] SET EndDate = '20180403' WHERE MobileIdentityID=888

--Gwyn-Williams,Kyle
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('kyle.gwyn-williams@us.hsbc.com',1250)
--Teissier,Alexandre
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('alexandre.teissier@us.hsbc.com',2223)
--Harris,Jason
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('jason.a.harris@us.hsbc.com',2259)
--Kattan,SaulS
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('saul.s.kattan@us.hsbc.com',2263)

--Mugabe,Tsungai
INSERT [dbo].[CustodianMobileIdentity] (MobileNo, CustodianId, StartDate, EndDate, Location) values ('6464612929',2241,'20180403',NULL,'NYC')
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('tmugabe1',2258)
--Howell,Matthew
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('matthew.c.howell@us.hsbc.com',2258)
insert [dbo].CustodianIdentity ([Address], CustodianId) VALUES ('mhowell32',2258)


--Milgram,Natan
UPDATE [dbo].[Custodians_Staging] SET EndDate = '20180329', IsInScope=0, DateRemoved='20180403' WHERE CustodianId = 627

--Collins, Timothy
INSERT INTO [dbo].[Custodians_Staging]
           ([EmployeeNo] ,[KPMG Combined Segment],[CustodianDisplayName] ,[IsInScope] ,[StartDate] ,[EndDate] ,[DateAdded]
           ,[DateRemoved],[ProjectPhase],[Location] ,[AssetClass])
     VALUES ('44108358','EQ-Finance','Collins, Timothy',1,'20180403',Null,'20180403',Null,'BAU','NYC','EQ')

INSERT [dbo].CustodianIdentity ([Address], CustodianId) SELECT 'tim.j.collins@us.hsbc.com',CustodianId FROM [dbo].[Custodians_Staging] WHERE [EmployeeNo] = '44108358'
INSERT [dbo].[CustodianTraderIdentity] (TraderId, CustodianId, StartDate, EndDate, Location) 
	SELECT 7765,CustodianId,'20180403',Null,'NYC' FROM [dbo].[Custodians_Staging] WHERE [EmployeeNo] = '44108358'
INSERT [dbo].CustodianCiscoIdentity (CiscoNo, CustodianId, StartDate, EndDate, Location) 
	SELECT '2125257927',CustodianId,'20180403',Null,'NYC' FROM [dbo].[Custodians_Staging] WHERE [EmployeeNo] = '44108358'

--Cerdan, Olivia
INSERT INTO [dbo].[Custodians_Staging]
           ([EmployeeNo] ,[KPMG Combined Segment],[CustodianDisplayName] ,[IsInScope] ,[StartDate] ,[EndDate] ,[DateAdded]
           ,[DateRemoved],[ProjectPhase],[Location] ,[AssetClass])
     VALUES ('44005379','FX-Sales','Cerdan, Olivia',1,'20180403',Null,'20180403',Null,'BAU','NYC','FX')

INSERT [dbo].CustodianIdentity ([Address], CustodianId) SELECT 'tim.j.collins@us.hsbc.com',CustodianId FROM [dbo].[Custodians_Staging] WHERE [EmployeeNo] = '44005379'
INSERT [dbo].[CustodianTraderIdentity] (TraderId, CustodianId, StartDate, EndDate, Location) 
	SELECT 5212,CustodianId,'20180403',Null,'NYC' FROM [dbo].[Custodians_Staging] WHERE [EmployeeNo] = '44005379'
INSERT [dbo].CustodianCiscoIdentity (CiscoNo, CustodianId, StartDate, EndDate, Location) 
	SELECT '2125252182',CustodianId,'20180403',Null,'NYC' FROM [dbo].[Custodians_Staging] WHERE [EmployeeNo] = '44005379'




