USE [Voice_Analytics]
GO

INSERT INTO [filter].[BenchmarkDepartment]
           ([BenchmarkId]
           ,[DepartmentId]
           ,[MinutesBefore]
           ,[MinutesAfter]
           ,[StartDate]
           ,[StopDate]
           ,[LocationId]
           ,[IsActive]
           )

SELECT 
	new.benchmarkId
	  ,d.DepartmentID
	  ,old.MinutesBefore
	  ,old.MinutesAfter
	  ,old.StartDate
	  ,old.EndDate
	  ,l.LocationID
	  ,1
FROM [Audio2_Metadata].[dbo].[Benchmark_Segment_New] old
INNER JOIN [Voice_Analytics].[filter].[Benchmark] new
	on old.BenchmarkName = new.Name
	and new.BenchmarkTypeID = 1
	and new.isactive = 1
INNER JOIN [Voice_Analytics].[shared].[Location] l 
	on old.Location= l.City
INNER JOIN [Voice_Analytics].[shared].[BusinessUnit] bu
	on CASE WHEN old.Segment = 'BSM' THEN old.Segment ELSE LEFT(old.Segment,2) END= bu.Code
INNER JOIN [Voice_Analytics].[shared].department d
	on d.BusinessUnitID = bu.BusinessUnitID
	and CASE WHEN old.Segment = 'BSM' THEN old.Segment ELSE RIGHT(old.Segment,LEN(old.Segment)-3) END= d.name
where old.Segment <> 'NA'
order by 1


INSERT INTO [filter].[BenchmarkDepartment]
           ([BenchmarkId]
           ,[DepartmentId]
           ,[MinutesBefore]
           ,[MinutesAfter]
           ,[StartDate]
           ,[StopDate]
           ,[LocationId]
           ,[IsActive]
           )

SELECT 
	new.benchmarkId
	  ,d.DepartmentID
	  ,old.MinutesBefore
	  ,old.MinutesAfter
	  ,old.StartDate
	  ,old.EndDate
	  ,l.LocationID
	  ,1
FROM [Audio2_Metadata].[dbo].[Benchmark_Segment_Expiries_New] old
INNER JOIN [Voice_Analytics].[filter].[Benchmark] new
	on old.BenchmarkName = new.Name
	and new.BenchmarkTypeID = 2
	and new.isactive = 1
INNER JOIN [Voice_Analytics].[shared].[Location] l 
	on old.Location= l.City
INNER JOIN [Voice_Analytics].[shared].[BusinessUnit] bu
	on CASE WHEN old.Segment = 'BSM' THEN old.Segment ELSE LEFT(old.Segment,2) END= bu.Code
INNER JOIN [Voice_Analytics].[shared].department d
	on d.BusinessUnitID = bu.BusinessUnitID
	and CASE WHEN old.Segment = 'BSM' THEN old.Segment ELSE RIGHT(old.Segment,LEN(old.Segment)-3) END= d.name
where old.Segment <> 'NA'
order by 1