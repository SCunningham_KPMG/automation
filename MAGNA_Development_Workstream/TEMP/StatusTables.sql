-- Start from 22nd March

SELECT  FileId
		,replace(filename,'Metadata_Metadata','Metadata') Filename
		,Location
		,NumerOfRows NumberOfRows
		,MetadataCopiedToStaging
		,TrunkdataCopiedToStaging
		,MetadataCopiedToPreAnalytics
		,TrunkDataCopiedToPreAnalytics
FROM [Voice_Data].[dbo].[DataflowStatus] 
where replace(filename, 'Metadata_Metadata','Metadata') >= 'Metadata_2018-03-22.csv'
order by replace(filename, 'Metadata_Metadata','Metadata') desc, 3

select * from Voice_Analytics.dbo.LocationDataStatus order by 2 desc,1

select location, audioFileExists, count(*) from voice_data.dbo.PreAnalytics_Metadata 
where cast(LocalStartTime as date) = '20180406' group by location, AudioFileExists
/*
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180322',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180322',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180322',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180323',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180323',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180323',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180324',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180324',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180324',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180325',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180325',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180325',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180326',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180326',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180326',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180327',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180327',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180327',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180328',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180328',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180328',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180329',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180329',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180329',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180330',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180330',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180330',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180331',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180331',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180331',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180401',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180401',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180401',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180402',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180402',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180402',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180403',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180403',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180403',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180404',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180404',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180404',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180405',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180405',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180405',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180406',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180406',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180406',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180407',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180407',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180407',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180408',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180408',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180408',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180409',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180409',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180409',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180410',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180410',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180411',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180411',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180410',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180411',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180412',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180412',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180412',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180413',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180413',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180413',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180414',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180414',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180414',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180415',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180415',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180415',getdate())

  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (1,'20180416',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (2,'20180416',getdate())
  insert Voice_Analytics.dbo.LocationDataStatus (locationId, DataDate, ReadyForPhase2Filtering) values (3,'20180416',getdate())

EXECUTE  [filter].[uspLexicon] 1 ,'20180322'
EXECUTE  [filter].[uspLexicon] 2 ,'20180322'
EXECUTE  [filter].[uspLexicon] 3 ,'20180322'
EXECUTE  [filter].[uspLexicon] 1 ,'20180323'
EXECUTE  [filter].[uspLexicon] 2 ,'20180323'
EXECUTE  [filter].[uspLexicon] 3 ,'20180323'
EXECUTE  [filter].[uspLexicon] 1 ,'20180324'
EXECUTE  [filter].[uspLexicon] 2 ,'20180324'
EXECUTE  [filter].[uspLexicon] 3 ,'20180324'
EXECUTE  [filter].[uspLexicon] 1 ,'20180325'
EXECUTE  [filter].[uspLexicon] 2 ,'20180325'
EXECUTE  [filter].[uspLexicon] 3 ,'20180325'
EXECUTE  [filter].[uspLexicon] 1 ,'20180326'
EXECUTE  [filter].[uspLexicon] 2 ,'20180326'
EXECUTE  [filter].[uspLexicon] 3 ,'20180326'
EXECUTE  [filter].[uspLexicon] 1 ,'20180327'
EXECUTE  [filter].[uspLexicon] 2 ,'20180327'
EXECUTE  [filter].[uspLexicon] 3 ,'20180327'
EXECUTE  [filter].[uspLexicon] 1 ,'20180328'
EXECUTE  [filter].[uspLexicon] 2 ,'20180328'
EXECUTE  [filter].[uspLexicon] 3 ,'20180328'
EXECUTE  [filter].[uspLexicon] 1 ,'20180329'
EXECUTE  [filter].[uspLexicon] 2 ,'20180329'
EXECUTE  [filter].[uspLexicon] 3 ,'20180329'
EXECUTE  [filter].[uspLexicon] 1 ,'20180330'
EXECUTE  [filter].[uspLexicon] 2 ,'20180330'
EXECUTE  [filter].[uspLexicon] 3 ,'20180330'
EXECUTE  [filter].[uspLexicon] 1 ,'20180331'
EXECUTE  [filter].[uspLexicon] 2 ,'20180331'
EXECUTE  [filter].[uspLexicon] 3 ,'20180331'
EXECUTE  [filter].[uspLexicon] 1 ,'20180401'
EXECUTE  [filter].[uspLexicon] 2 ,'20180401'
EXECUTE  [filter].[uspLexicon] 3 ,'20180401'
EXECUTE  [filter].[uspLexicon] 1 ,'20180402'
EXECUTE  [filter].[uspLexicon] 2 ,'20180402'
EXECUTE  [filter].[uspLexicon] 3 ,'20180402'
EXECUTE  [filter].[uspLexicon] 1 ,'20180403'
EXECUTE  [filter].[uspLexicon] 2 ,'20180403'
EXECUTE  [filter].[uspLexicon] 3 ,'20180403'
EXECUTE  [filter].[uspLexicon] 1 ,'20180404'
EXECUTE  [filter].[uspLexicon] 2 ,'20180404'
EXECUTE  [filter].[uspLexicon] 3 ,'20180404'
EXECUTE  [filter].[uspLexicon] 1 ,'20180405'
EXECUTE  [filter].[uspLexicon] 2 ,'20180405'
EXECUTE  [filter].[uspLexicon] 3 ,'20180405'
EXECUTE  [filter].[uspLexicon] 1 ,'20180406'
EXECUTE  [filter].[uspLexicon] 2 ,'20180406'
EXECUTE  [filter].[uspLexicon] 3 ,'20180406'
EXECUTE  [filter].[uspLexicon] 1 ,'20180407'
EXECUTE  [filter].[uspLexicon] 2 ,'20180407'
EXECUTE  [filter].[uspLexicon] 3 ,'20180407'
*/

-- Run Phase 1
/*
DECLARE @RC int
SELECT getdate()
EXECUTE @RC = [Voice_Data].[dbo].[uspPreAnalytics] 
SELECT @RC
SELECT getdate()

DECLARE @RC int
SELECT getdate()
EXECUTE @RC = [dbo].[uspPhase1Processing] 
SELECT @RC
SELECT getdate()

DECLARE @RC int
SELECT getdate()
EXECUTE @RC = [dbo].[uspPhase2Processing] 
SELECT @RC
SELECT getdate()
*/
select * from dbo.errorlog
-- Run phase 3 (Copy to output file)
/*
DECLARE @RC INT
SELECT getdate()
EXECUTE @RC = [dbo].[uspPhase3Processing] 
SELECT @RC
SELECT getdate()


EXECUTE  [dbo].[PopulateETLTraderInteractions] 
EXECUTE  [dbo].PopulateReportSamplingTracker

select count(*) from filter.outputfile where status=3

select count(*) from filter.outputnofilter where status=3

select * from audio2_metadata.report.samplingtracker where loaddate >= '20180412'

update filter.outputfile set status =  4 where cast(localstarttime as date) <= '20180405'
update filter.outputnofilter set status =  4 where cast(localstarttime as date) <= '20180405'

select location, cast([LocalStartTime] as date), count(*) from [filter].[OutputFile] group by location, cast([LocalStartTime] as date) order by 1,2
select status, count(*) from audio.call group by status
select ActualFilterUsedForIngest, count(*) from audio.call group by ActualFilterUsedForIngest

select cast (localstarttime as date), location, FilterTypeId, 
count(*) from filter.OutputFile   group by cast (localstarttime as date), location, FilterTypeId order by 1,2,3

select status, count(*) from filter.OutputFile  group by status
select status, count(*) from filter.outputnofilter  group by status
9 / 28

*/
select cast(localstarttime as date), cfs.FilterTypeId,count(*) from audio.call c inner join audio.CallFiltersSatisfied cfs on c.callid = cfs.CallID 
group by cast(localstarttime as date), cfs.FilterTypeId order by 1,2

--UPDATE filter.outputnofilter set status=2, ReleaseFileID = 'N3022'  where status=1 and ReleaseFileID is null

