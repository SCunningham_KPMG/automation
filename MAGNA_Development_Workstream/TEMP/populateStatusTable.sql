USE [Voice_Data]
GO

INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 0,'Staging - not processed')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 1,'Staging - being copied to pre-analytics')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 2,'Staging - copied to pre-analytics')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 20,'Pre-analytics - not processed')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 21,'Pre-analytics - custodian identified in metadata and trunk label')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 22,'Pre-analytics - ready to add trunk label and dialled digits to metadata')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 23,'Pre-analytics - trunk label and dialled digits added to metadata')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 25,'Audio file does not exist')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 26,'Call less than 3 seconds')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 27,'Speaker call')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 28,'News channel')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (29 ,'Duplicate (HKG) rule')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (40 ,'Data has passed validation and can be copied to analytics')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (41 ,'Data has passed validation and can be copied to analytics. A trunk label could not be found')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 42,'Intermediate stage of copy to analytics for status 40 records')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (43 ,'Intermediate stage of copy to analytics for status 41 records')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (50 ,'Data has been copied to analytics. Previous state was 42')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (51 ,'Data has been copied to analytics. Previous state was 43')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (60 ,'Data in analytics has not been processed')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 61,'Valid custodian not found for data')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (62 ,'Data is ready for first set of filters')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (70 ,'Benchmark expiry filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (71 ,'Benchmark window filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 72,'Market numbers filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (73 ,'Out of hours filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES ( 74,'Weekends/Holidays filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (75 ,'Intra-desk filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (76 ,'Trader follow filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (77 ,'Intensity filter applied')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (80 ,'Random filter applied and data sampled')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (81 ,'Copied to ingest table')
INSERT INTO [dbo].[DataCallStatus] ([statusId],[name]) VALUES (82 ,'Copied to no filter table')

GO


