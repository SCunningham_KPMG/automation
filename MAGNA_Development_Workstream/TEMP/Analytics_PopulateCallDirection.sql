USE [Voice_Analytics]
GO

INSERT INTO [audio].[CallDirection]
           ([CallDirectionID]
           ,[Name]
           ,[DateCreated])
SELECT 1,'Incoming',getdate()

INSERT INTO [audio].[CallDirection]
           ([CallDirectionID]
           ,[Name]
           ,[DateCreated])
SELECT 2,'Outgoing',getdate()
GO

INSERT INTO [audio].[CallDirection]
           ([CallDirectionID]
           ,[Name]
           ,[DateCreated])
SELECT 0,'Unknown',getdate()
GO

INSERT INTO [audio].[CallDirection]
           ([CallDirectionID]
           ,[Name]
           ,[DateCreated])
SELECT 3,'Internal',getdate()
GO
