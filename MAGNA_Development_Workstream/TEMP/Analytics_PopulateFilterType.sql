USE [Voice_Analytics]
GO
SET IDENTITY_INSERT [filter].[FilterType] ON 
GO
INSERT INTO [filter].[FilterType]
           ([FilterTypeId]
		   ,[Name]
           ,[Description])
SELECT 1, 'Benchmark Expiry', 'Benchmark Expiry'
GO
INSERT INTO [filter].[FilterType]
           ([FilterTypeId]
		   ,[Name]
           ,[Description])
SELECT 2, 'Benchmark Window', 'Benchmark Window'
GO
INSERT INTO [filter].[FilterType]
           ([FilterTypeId]
		   ,[Name]
           ,[Description])
SELECT 3, 'Market Numbers', 'Market Numbers'
GO
INSERT INTO [filter].[FilterType]
           ([FilterTypeId]
		   ,[Name]
           ,[Description])
SELECT 4, 'Market Events', 'Market Events'
GO
SET IDENTITY_INSERT [filter].[FilterType] OFF
GO