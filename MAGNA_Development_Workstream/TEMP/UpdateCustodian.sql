select sc.CustodianID, sc.EndDate, sc.IsActive, cs.EndDate, cs.IsInScope, bu.name, d.name
from [shared].[Custodian] sc
inner join shared.department d
	on sc.DepartmentID = d.DepartmentID
inner join shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID
inner join Audio2_Metadata.dbo.Custodians_Staging cs
	on sc.CustodianID = cs.CustodianId

where sc.EndDate <> cs.EndDate
or sc.IsActive <> cs.IsInScope
order by 4,6,7


select * from Audio2_Metadata.dbo.Custodians_Staging cs
left join [shared].[Custodian] sc on sc.CustodianID = cs.CustodianId where sc.CustodianID is null


update [shared].[Custodian]
set EndDate = cs.EndDate, IsActive = cs.IsInScope
from [shared].[Custodian] sc
inner join shared.department d
	on sc.DepartmentID = d.DepartmentID
inner join shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID
inner join Audio2_Metadata.dbo.Custodians_Staging cs
	on sc.CustodianID = cs.CustodianId
where sc.EndDate <> cs.EndDate
or sc.IsActive <> cs.IsInScope