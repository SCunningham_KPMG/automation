USE [Voice_Analytics]
GO
SET IDENTITY_INSERT [filter].[FilterExpiryType] ON 
GO
INSERT INTO [filter].[FilterExpiryType]
           ([FilterExpiryTypeId]
		   ,[Name]
           ,[Description])
SELECT 0, 'N/A', 'Not Applicable'
GO

INSERT INTO [filter].[FilterExpiryType]
           ([FilterExpiryTypeId]
		   ,[Name]
           ,[Description])
SELECT 1, '3rd Thu', '3rd Thu'
GO

INSERT INTO [filter].[FilterExpiryType]
           ([FilterExpiryTypeId]
		   ,[Name]
           ,[Description])
SELECT 2, '3rd Fri', '3rd Fri'
GO

INSERT INTO [filter].[FilterExpiryType]
           ([FilterExpiryTypeId]
		   ,[Name]
           ,[Description])
SELECT 3, '2nd Fri', '2nd Fri'
GO

INSERT INTO [filter].[FilterExpiryType]
           ([FilterExpiryTypeId]
		   ,[Name]
           ,[Description])
SELECT 4, '2nd Last Trading Day', '2nd Last Trading Day'
GO

INSERT INTO [filter].[FilterExpiryType]
           ([FilterExpiryTypeId]
		   ,[Name]
           ,[Description])
SELECT 5, 'Fri after 3rd Thu', 'Fri after 3rd Thu'
GO

SET IDENTITY_INSERT [filter].[FilterExpiryType] OFF
GO