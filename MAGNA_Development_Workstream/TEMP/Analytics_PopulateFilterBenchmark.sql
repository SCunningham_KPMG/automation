USE [Voice_Analytics]
GO

INSERT INTO [filter].[Benchmark]
           ([Name]
           ,[BenchmarkTypeID]
           ,[TimeZone]
           ,[Timestamp]
		   ,[ExpiryTag]
           ,[StartDate]
           ,[StopDate]
           ,[IsActive]
           )

SELECT [BenchmarkName]
	  ,1
      ,[Benchmark_TimeZone]
      ,[Benchmark_Timestamp]
	  ,0
      ,[Startdate]
      ,[EndDate]
      ,[IsEnabled]
  FROM [Audio2_Metadata].[dbo].[BenchmarkList] order by 2

INSERT INTO [filter].[Benchmark]
           ([Name]
           ,[BenchmarkTypeID]
		   ,[ExpiryTag]
           ,[TimeZone]
           ,[Timestamp]
           ,[StartDate]
           ,[StopDate]
           ,[IsActive]
           )

SELECT [BenchmarkName]
	  ,2
	  ,ExpiryTag
      ,[Benchmark_TimeZone]
      ,[Benchmark_Timestamp]
      ,[Startdate]
      ,[EndDate]
      ,[IsEnabled]
  FROM [Audio2_Metadata].[dbo].[BenchmarkList_Expiries] order by 2

  UPDATE  [Voice_Analytics].[filter].[Benchmark] SET IsActive=0 WHERE StopDate < getdate()



