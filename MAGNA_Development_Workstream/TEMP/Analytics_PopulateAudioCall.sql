USE [Voice_Analytics]
GO
UPDATE [Voice_Data].[dbo].[PreAnalytics_Metadata] SET status = 9 WHERE status IN (2,3)

/*
TRUNCATE TABLE [audio].[Call]
TRUNCATE TABLE [audio].[File]
TRUNCATE TABLE [audio].[CallFile]
TRUNCATE TABLE [audio].[CallCustodianIdentity]
*/
 select top 10 from [Voice_Data].[dbo].[PreAnalytics_Metadata]

INSERT INTO [audio].[Call]
           (CallId
		   ,[LocalStartTime]
           ,[UTCStartTime]
           ,[LocalEndTime]
           ,[UTCEndTime]
           ,[DurationInSeconds]
           ,[TrunkLabel]
           ,[ParentID]
           ,[Channel]
           ,[ClientReference]
           ,[DialledNumber]
           ,[PhoneNumber]
           ,[CLIANI]
           ,[LanguageID]
           ,[BatchID]
           ,[DirectionId]
           ,[InitiatorID]
           ,[LocationID]
           ,[DateCreated]
           ,[Include]
		   ,[Status])
SELECT
		[CallID],
		[LocalStartTime],
		[UTCStartTime],
		[LocalStopTime],
		[UTCStopTime],
		[Duration],
		[TrunkLabel],
		[CallParentID],
		NULL as Channel,
		NULL As ClientReference,
		[DialedNumber],
		[PhoneNumber],
		CallingLineIdentifier as CLIANI,
		NULL As LanguageId,
		NULL As BatchId,
		d.CallDirectionId,
		[InitiatorTRID],
		loc.locationId,
		getdate(),
		1 as Include,
		0 As Status
FROM [Voice_Data].[dbo].[PreAnalytics_Metadata] m
INNER JOIN Voice_Analytics.[shared].[Location] loc
	ON m.location = loc.city
INNER JOIN Voice_Analytics.[audio].[CallDirection] d
	ON m.CallDirection = d.name
WHERE m.status = 9


INSERT INTO [audio].[File]
           ([Name]
           ,[FileTypeID]
           ,[Directory]
           ,[CompleteFilePath]
           ,[RecordingTypeID]
           ,[Include]
           ,[DateCreated])
SELECT MediaFileName, NULL, c.CallId, MediaPath, NULL, 1, getdate()
FROM [audio].[call] c
INNER JOIN [Voice_Data].[dbo].[PreAnalytics_Metadata] m
	ON c.CallID = m.CallID
WHERE c.status = 0

INSERT INTO [audio].[CallFile]
           ([CallID]
           ,[FileID])

SELECT f.Directory, f.FileId FROM audio.[file] f INNER JOIN [audio].[call] c ON f.Directory = c.CallID WHERE c.status = 0
GO
UPDATE audio.[file] SET directory = NULL
GO


INSERT INTO [audio].[CallCustodianIdentity]
           ([CallID]
           ,[CustodianIdentityID]
           ,[DeviceTypeID]
           ,[IsInitiator]
           ,[DirectionId]
           ,[DateCreated])
SELECT 
c.CallID,
i.CustodianIdentityID,
m.InitialDeviceTypeID,-- m.FinalDeviceTypeID,
NULL as IsInitator,
d.CallDirectionId,
getdate() as DateCreated

FROM [audio].[call] c
INNER JOIN [Voice_Data].[dbo].[PreAnalytics_Metadata] m
	ON c.CallID = m.CallID
INNER JOIN [shared].[CustodianIdentity] i
	on i.CustodianIdentity = m.ParticipantIdentity
	and c.LocalStartTime >= i.StartDate 
	and c.LocalEndTime <= i.EndDate
INNER JOIN [shared].[Custodian] cu
	ON i.CustodianID = cu.CustodianID
	AND cu.LocationID = c.LocationID
INNER JOIN Voice_Analytics.[audio].[CallDirection] d
	ON m.CallDirection = d.name
WHERE c.status = 0

UPDATE [Voice_Analytics].[audio].[Call]
	SET status = 1 -- no custodian
  FROM [Voice_Analytics].[audio].[Call] c 
  left join [Voice_Analytics].[audio].[CallCustodianIdentity] ci
	on c.callid = ci.CallID
where ci.CallCustodianIdentityID is null and c.Status = 0

-- **************************************************************************

