USE [Voice_Analytics]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/*------------------------------------------------------------------------------------------------------
Procedure:      dbo.PopulateETLTraderInteractions
Author:         PBains

Description:   
For reporting pirposes.

EXEC dbo.PopulateETLTraderInteractions 



Change History
------------------------------
20180406 - PBains - 1.0 : New peocedure
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [dbo].PopulateETLTraderInteractions
	@LocationId INT,
	@DataDate	DATE
AS
BEGIN
DECLARE @retStatus INT
SET NOCOUNT ON
SET @retStatus = 0

BEGIN TRY	
	BEGIN TRAN
	INSERT INTO [Audio2_Metadata].ETL.[TraderInteractions]
			   ([InteractionID]
			   ,[InteractionOriginalID]
			   ,[LocalStartTime]
			   ,[GMTStartTime]
			   ,[LocalEndTime]
			   ,[GMTEndTime]
			   ,[DurationInSeconds]
			   ,[DeviceTypeID]
			   ,[DeviceType]
			   ,[CallDirectionTypeID]
			   ,[CallDirection]
			   ,[TraderID]
			   ,[TraderName]
			   ,[Segment]
			   ,[InitiatorTRID]
			   ,[IsInitiator]
			   ,[TrunkLabel]
			   ,[TrunkLabelDescr]
			   ,[DialedNumber]
			   ,[PhoneNumber]
			   ,[FileName]
			   ,[HasAudioFile]
			   ,[CustodianAssetClass]
			   ,[ProjectPhase]
			   ,[CustodianLocation]
			   ,[CLIANI]
			   ,[UniqueRowId]
			   ,[YYYYMMDDHH])
	SELECT o.callid, o.callId, c.LocalStartTime, c.UTCStartTime , c.LocalEndTime, c.UTCEndTime, c.DurationInSeconds, dt.DeviceTypeID, dt.Name,
		cd.CallDirectionID, cd.Name, o.TraderID, o.TraderName, 
		bu.code + case when d.code <> '' then '-' + d.code else '' end as segment,
		'' InitiatorTRID,'' IsInitiator, COALESCE(c.TrunkLabel,'') as TrunkLabel,
		COALESCE(tld.[Fulltrunklabel],'') TrunkLabelDescription ,
		c.DialledNumber, c.PhoneNumber, o.MediaFileName,1,bu.Code,'BAU',loc.City,
		o.CallLineIdentifier,Null,''
		FROM [Voice_Analytics].[filter].[outputFile] o
		INNER JOIN [Voice_Analytics].audio.Call c 
			ON o.callId = c.CallID
		INNER JOIN [Voice_Analytics].audio.CallDirection cd
			ON c.CallDirectionId = cd.CallDirectionID
		LEFT JOIN [Voice_Analytics].audio.CallFile cf
			ON c.callId = cf.CallID
		LEFT JOIN [Voice_Analytics].audio.[File] f
			ON cf.FileID = f.fileId
		INNER JOIN [Voice_Analytics].filter.FilterType ft
				ON o.filterTypeId = ft.filterTypeId
		INNER JOIN [Voice_Analytics].audio.CallCustodianIdentity cci
			ON c.CallID = cci.CallID
		INNER JOIN [Voice_Analytics].shared.CustodianIdentity scci
			ON cci.CustodianIdentityID = scci.CustodianIdentityID
		LEFT JOIN[Voice_Analytics]. audio.deviceType dt
			on cci.DeviceTypeID = dt.DeviceTypeID
		INNER JOIN [Voice_Analytics].shared.Custodian sc
			ON scci.CustodianID = sc.CustodianID
		INNER JOIN [Voice_Analytics].shared.Department d
			on sc.DepartmentID = d.DepartmentID
		INNER JOIN [Voice_Analytics].shared.BusinessUnit bu
			ON d.BusinessUnitID = bu.BusinessUnitID
		LEFT JOIN [Voice_Analytics].shared.CustodianIdentity scci2
			ON sc.CustodianID = scci2.CustodianID AND scci2.IdentityType = 1
		INNER JOIN [Voice_Analytics].shared.location loc
			ON loc.LocationID = sc.LocationID
		LEFT JOIN Voice_Analytics.shared.TrunkLabelDescription tld
			ON c.TrunkLabel = tld.TrunkLabel
	 where o.status = 2 and c.locationId=@LocationId and cast(c.LocalStartTime as date) = @DataDate
 COMMIT
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	EXEC dbo.LogError

END CATCH

RETURN @retStatus

END





GO


