/****** Script for SelectTopNRows command from SSMS  ******/
-- SELECT * FROM [Voice_Analytics].[filter].[CustodianDateFollowed]
INSERT [Voice_Analytics].[filter].[CustodianDateFollowed] (CustodianId,DateFollowed)
SELECT 
distinct
cti.CustodianId,
--a2r.[TraderID] ,a2r.TraderName,
	  cast (a2r.LocalStartTime as date) calldate
  FROM [Audio2_Metadata].[report].[SamplingTracker] st WITH (NOLOCK)
  inner join [Audio2_Metadata].report.Audio2Report  a2r WITH (NOLOCK)
	on st.InteractionID = a2r.InteractionID
	inner join [Audio2_Metadata].[dbo].[CustodianTraderIdentity] cti WITH (NOLOCK)
	on cti.TraderID = a2r.traderid
  where SignalIdentifier  like '%Follow%' and cast (a2r.LocalStartTime as date) > '20180101'  and cast (a2r.LocalStartTime as date) < '20180322'
  order by 2 desc 

