USE [Voice_Data]
GO

/****** Object:  StoredProcedure [dbo].[uspCopyToPreAnalytics]    Script Date: 13/03/2018 11:15:44 ******/
DROP PROCEDURE [dbo].[uspCopyToPreAnalytics]
GO

/****** Object:  StoredProcedure [dbo].[uspCopyToPreAnalytics]    Script Date: 13/03/2018 11:15:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*------------------------------------------------------------------------------------------------------
Procedure:      dbo.spCopyToPreAnalytics
Author:         PBains

Description:   
Copies data from staging to pre-analytics as and when it arrives.
If the status is zero it hasn't been copied before. Sets it to 1, does the copy and then sets it to 2.
Copies metadata first and then trunk label data.

Call:
EXEC dbo.spCopyToPreAnalytics

Return Value:
	0 = success
	1 - 4: metadata copy failed. Look at Voics_Analytics.dbo.ErrorLog
	5 - 8: trunk label copy failed. Look at Voics_Analytics.dbo.ErrorLog

Change History
------------------------------
20180313 1.00	PB	Initial version
------------------------------------------------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[uspCopyToPreAnalytics]
AS
BEGIN
 
	SET NOCOUNT ON 
	DECLARE @retStatus INT
	DECLARE @currentDateTime datetime

	SET @currentDateTime = getdate()
	SET @retStatus = 0

BEGIN TRY
	BEGIN TRAN
	SET @retStatus = 1
	UPDATE dbo.Staging_Metadata SET CopyStatus = 1 WHERE CopyStatus = 0

	SET @retStatus = 2
	INSERT INTO dbo.PreAnalytics_Metadata (
		Location
		,CallID
		,CallParentID
		,TraderId
		,LocalStartTime
		,LocalStopTime
		,UTCStartTime
		,UTCStopTime
		,Duration
		,InitiatorTRID
		,MediaFileName
		,MediaPath
		,MediaFileId
		,TrunkLabel
		,DeviceId
		,InitialDeviceTypeID
		,FinalDeviceTypeID
		,CallDirection
		,Station
		,PhoneNumber
		,DialedNumber
		,ParticipantIdentityType
		,ParticipantIdentity
		,CopiedToAnalyticsDateTime
		,Status
		,Firstname
		,MiddleName
		,LastName
		,CallingLineIdentifier)
    
	SELECT 
		Location
		,CAST(CallId as BIGINT)
		,CAST(CrossCompleteID as BIGINT)
		,TraderId
		,LocalStartTime
		,LocalStopTime
		,UTCStartTime
		,UTCStopTime		
		,DATEPART(HOUR, cast (duration as time)) *3600 + DATEPART(MINUTE, cast (duration as time)) *60 + DATEPART(SECOND, cast (duration as time))
		,CallInitiatorTRID
		,MediaFileName
		,MediaFilePath
		,MediaFileId
		,Null as TrunkLabel
		,ISNULL(ParticipantDeviceID,DeviceID) AS DeviceID
		,ISNULL(ParticipantDeviceTypeID,DeviceTypeID) AS InitialDeviceTypeID
		,Null as FinalDeviceTypeId
		,CallDirection
		,ISNULL(Station,ParticipantStation) AS Station
		,ISNULL(PhoneNumber,ParticipantPhoneNumber) AS PhoneNumber
		,DialedNumber
		,null
		,null
		,getdate()
		,20
		,Firstname
		,MiddleName
		,LastName
		,CallerId
	FROM  dbo.Staging_Metadata
	WHERE CopyStatus = 1

	SET @retStatus = 3
	UPDATE dbo.Staging_Metadata SET CopyStatus = 2, CopiedToPreAnalytics = @currentDateTime WHERE CopyStatus = 1 

	SET @retStatus = 4
	COMMIT
	SET @retStatus = 0
END TRY
BEGIN CATCH
	INSERT Voice_Analytics.dbo.ErrorLog (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage) 
		SELECT ERROR_NUMBER(), ERROR_SEVERITY (), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), 'Error location:' + CAST(@retStatus as varchar(3)) + '::' + ERROR_MESSAGE()

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION
END CATCH


BEGIN TRAN
BEGIN TRY
	SET @retStatus = 5
	UPDATE dbo.Staging_TrunkLabel SET CopyStatus = 1 WHERE CopyStatus = 0

	SET @retStatus = 6
	INSERT INTO dbo.PreAnalytics_TrunkLabels
           (CallId
           ,FirstName
           ,LastName
           ,TraderId
           ,TrunkLabel
           ,DeviceTypeId
           ,Location
           ,DialledDigits
           ,Station
		   ,[Status]
	)
	SELECT DISTINCT CallID, FirstName, LastName,TraderId,TrunkLabel, deviceTypeID, Location, DialedDigits, Station, 20 
	FROM dbo.Staging_TrunkLabel WHERE CopyStatus = 1

	SET @retStatus = 7
	SET @currentDateTime = getdate()
	UPDATE dbo.Staging_TrunkLabel SET CopyStatus = 2, CopiedToPreAnalytics = @currentDateTime WHERE CopyStatus = 1 

	SET @retStatus = 8
	COMMIT
	SET @retStatus = 0
END TRY
BEGIN CATCH
	INSERT Voice_Analytics.dbo.ErrorLog (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage) 
		SELECT ERROR_NUMBER(), ERROR_SEVERITY (), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), 'Error location:' + CAST(@retStatus as varchar(3)) + '::' + ERROR_MESSAGE()

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION
END CATCH

RETURN @retStatus

END




GO


