
-- Calls per segment
WITH cteCallsPerSegemnt AS
(
SELECT sd.BusinessUnitID, sd.DepartmentID, count(*) TotalSegmentCalls, CEILING(count(*) * 0.005) HALFPERCENTTOSAMPLE
FROM audio.call c
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity sci
	ON cci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN shared.Custodian sc
	ON sci.CustodianID = sc.CustodianID
INNER JOIN shared.Department sd
	ON sc.DepartmentID = sd.DepartmentID
WHERE CAST (c.localstarttime as date) = '20180315' AND c.locationId = 2 AND ActualFilterUsedForIngest IS NULL
GROUP BY sd.BusinessUnitID, sd.DepartmentID
),

cteCallsPerFilterTypePerSegemnt AS
(
SELECT sd.BusinessUnitID, sd.DepartmentID, c.CallID, ROW_NUMBER () OVER (PARTITION BY sd.BusinessUnitID, sd.DepartmentID ORDER BY NewId()) Pos
FROM audio.call c
INNER JOIN audio.CallFiltersSatisfied cfs
	ON c.CallID = cfs.CallID
INNER JOIN filter.FilterType ft 
	ON cfs.FilterTypeId = ft.FilterTypeID
	AND ft.filterCategoryId  = 3
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity sci
	ON cci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN shared.Custodian sc
	ON sci.CustodianID = sc.CustodianID
INNER JOIN shared.Department sd
	ON sc.DepartmentID = sd.DepartmentID
WHERE CAST (c.localstarttime as date) = '20180315' AND c.locationId = 2 AND ActualFilterUsedForIngest IS NULL
)

SELECT b.businessunitid, b.departmentid, b.callid, s.HALFPERCENTTOSAMPLE, s.TotalSegmentCalls
FROM cteCallsPerFilterTypePerSegemnt b
INNER JOIN cteCallsPerSegemnt s 
	ON b.BusinessUnitID = s.businessUnitId
	AND b.DepartmentID = s.departmentId
WHERE b.Pos <= s.HALFPERCENTTOSAMPLE
order by 1,2




