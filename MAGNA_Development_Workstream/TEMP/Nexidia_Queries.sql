select top 100 sm.InteractionId, sm.sourcemediaid, smt.TagId, smt.tagsource , t.TagName, r.TagDisplayName, rc.RiskCategory, sma.Annotation
from [nexidia].[SourceMedia] sm
inner join [nexidia].[SourceMediaTag] smt
	on sm.SourceMediaId = smt.SourceMediaId
inner join [nexidia].[SourceMediaAnnotations] sma
	on sm.SourceMediaId = sma.SourceMediaId
inner join [nexidia].[Tags] t
	on smt.TagId = t.TagId
inner join [nexidia].[Relevance] r
	on sm.InteractionID = r.interactionid
inner join [nexidia].[RiskCategory] rc
	on r.InteractionID = rc.InteractionID
where sm.interactionid = 6532873024328238523
order by 1

select * from [nexidia].[SourceMedia] sm where sm.interactionid = 6532873024328238523
select * from [nexidia].[SourceMediaTag] smt inner join [nexidia].[Tags] t	on smt.TagId = t.TagId where SourceMediaId = 2278701
select * from [nexidia].SourceMediaAnnotations where SourceMediaId = 2278701
select * from [nexidia].[Relevance] r where interactionid = 6532873024328238523
select * from [nexidia].[RiskCategory] rc where interactionid = 6532873024328238523
select * from [nexidia].Audit a inner join [nexidia].[AuditType] at on a.AuditTypeId = at.AuditTypeId where a.SourceMediaId = 2278701
select * from [nexidia].EvaluationTemplate where SourceMediaId = 2278701
select * from [nexidia].IngestedSpeakerCalls where interactionid = 6532873024328238523
select * from [nexidia].MetadataCC where SourceMediaId = 2278701
select * from [nexidia].SourceMediaSearchTermScoredSegment stss inner join [nexidia].[SearchTerms] st on stss.SearchTermId = st.SearchTermID and stss.SearchTermListId =st.SearchTermListID 
	where SourceMediaId = 2278701

select * from [nexidia].[WorkFlowAlerts] where SourceMediaId = 2278701

select * from [nexidia].WorkflowStateLogs wsl  left join nexidia.Users u on wsl.UserId = u.EsiUserIdentity where SourceMediaId = 2278701 order by CreationDateUtc

select a.ServerUTCDateTime, a.ClientTimeOffset, a.notes, at.Description, u.DisplayName, u.UserGroup
from [nexidia].Audit a inner join [nexidia].[AuditType] at on a.AuditTypeId = at.AuditTypeId 
inner join nexidia.Users u on a.UserId = u.EsiUserIdentity
where a.SourceMediaId = 2278701 
order by 1

select top 10 *
from nexidia.Users u where displayname = 'Jeffrey Cabrera'

select distinct usergroup from nexidia.Users u

select workflowalerttypeidentity, count(*) from [nexidia].[WorkFlowAlerts] group by workflowalerttypeidentity order by 1
select min(creationdateutc) from [nexidia].[WorkFlowAlerts]


select * from [nexidia].[SourceMedia] sm where sm.interactionid = 6532833033489293073
select * from [nexidia].[WorkFlowAlerts] where SourceMediaId = 2279792
select * from [nexidia].WorkflowStateLogs wsl  left join nexidia.Users u on wsl.UserId = u.EsiUserIdentity where SourceMediaId = 2279792 order by CreationDateUtc
select * from [nexidia].MetadataCC where SourceMediaId = 2279792


select * from [nexidia].Audit a inner join [nexidia].[AuditType] at on a.AuditTypeId = at.AuditTypeId where a.SourceMediaId = 2495484

SourceMediaID	InteractionID
2495484	6538395145141826667

select * from nexidia.Users where EsiUserIdentity = 'E8F09374-C924-45BD-BFAD-7892365E847B'

select usergroup
, SUM([LDN FX]) LDN_FX 
, SUM([LDN FI]) LDN_FI 
, SUM([LDN BSM]) LDN_BSM 
, SUM([LDN EQ]) LDN_EQ
, SUM([LDN CF]) LDN_CF
, SUM([NYC FX]) NYC_FX
, SUM([NYC FI]) NYC_FI 
, SUM([NYC BSM]) NYC_BSM 
, SUM([NYC EQ]) NYC_EQ
, SUM([NYC CF]) NYC_CF
, SUM([HKG FX]) HKG_FX
, SUM([HKG FI]) HKG_FI 
, SUM([HKG BSM]) HKG_BSM 
, SUM([HKG EQ]) HKG_EQ
, SUM([HKG CF]) HKG_CF
from nexidia.Users 
where usergroup not in (
'Admin',
'Audio 2',
'Client',
'Error',
'Tech Solution',
'UA team')
group by usergroup
