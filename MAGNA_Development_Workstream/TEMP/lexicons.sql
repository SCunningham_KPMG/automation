select  distinct  sm.InteractionID, sm.sourcemediaid, sm.IngestFileName, sm.CallDateTime, sm.NexSignalID
FROM [Audio2_Metadata].report.[LexiconScores] as L  WITH (NOLOCK)
left join [Audio2_Metadata].Nexidia.SourceMedia as SM  WITH (NOLOCK)
	on L.SourceMediaId=Sm.SourceMediaId
INNER JOIN Voice_Analytics.filter.OutputNoFilter noload
	on sm.interactionId = noload.callid
INNER JOIN Voice_Analytics.audio.CallCustodianIdentity cci
	ON noload.CallID = cci.CallID
INNER JOIN Voice_Analytics.shared.CustodianIdentity sci
	ON cci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN Voice_Analytics.shared.Custodian sc
	ON sci.CustodianID = sc.CustodianID
INNER JOIN Voice_Analytics.shared.Location sl
	ON sc.LocationID = sl.LocationID
INNER JOIN Voice_Analytics.shared.Department sd
	ON sc.DepartmentID = sd.DepartmentID
INNER JOIN Voice_Analytics.shared.BusinessUnit sbu
	ON sd.BusinessUnitID = sbu.BusinessUnitID
INNER JOIN [Audio2_Metadata].dbo.LexiconParameters as LP  WITH (NOLOCK)
	ON  LP.CustodianAssetClass = sbu.Name
	AND LP.CustodianLocation = sl.City
	AND LP.SearchTermListID = L.SearchTermListID 
	AND LP.CallDateTime <= CAST(noload.LocalStartTime as date)
	AND LP.EndDate >= CAST(noload.LocalStartTime as date)
where l.score>=90
and sm.NexSignalID like '%No%'
AND SM.DurationinMS<3600000  --to exclude calls in excess of 1h

-- Set actual filter ingested, status
-- insert into output file
-- remove from no load file
HK__6537087872796789425_Summed.nmf

select * from Voice_Analytics.audio.callfile where callid = 6537087872796789425
select * from Voice_Analytics.audio.[file] where fileid = 63542
select count(*) from Voice_Analytics.filter.OutputNoFilter
