/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Metadata_Staging_eCommsId]
      [EventUID]
      ,[TimeStamp]
  FROM [Voice_Data].[ETL].[Metadata_Staging_eComms]
  where timestamp >= '20180201'

select edp.eventuid, participant, participantrole 
from [ETL].[EComms_DocumentParticipant] edp
where edp.eventuid IN 
(
SELECT [EventUID]
  FROM [Voice_Data].[ETL].[Metadata_Staging_eComms]
  where timestamp >= '20180201' and participant <> ''
)
AND edp.participant IN ( SELECT Custodianidentity FROM Voice_Analytics.shared.CustodianIdentity WHERE IdentityType = 4)
order by 1

select distinct e.eventuid, e.timestamp, participant, participantrole 
from [ETL].[Metadata_Staging_eComms] e
INNER JOIN [ETL].[EComms_DocumentParticipant] edp
	ON e.EventUID = edp.EventUID
where 
e.timestamp >= '20180201' AND edp.participant <> ''
AND edp.participant IN ( SELECT Custodianidentity FROM Voice_Analytics.shared.CustodianIdentity WHERE IdentityType = 4)
order by 2 desc


