USE [Voice_Analytics]
GO

INSERT INTO [shared].[CustodianIdentity]
           ([CustodianIdentity]
           ,[IdentityType]
           ,[EndDate]
           ,[StartDate]
           ,[CustodianID]
           ,[IsActive] )

 SELECT 
	  [Address] as CustodianIdentity
	  ,4 as IdentityType
	  , CASE WHEN c.EndDate <= getdate() then c.EndDate ELSE '99991231' END As EndDate
      ,'20150101' As StartDate
	  , ci.CustodianId As CustodianId
	  , CASE WHEN c.EndDate <= getdate() then 0 ELSE 1 END As IsActive
  FROM [Audio2_Metadata].[dbo].[CustodianIdentity] ci
  INNER JOIN [Voice_Analytics].shared.[Custodian] c
	ON ci.CustodianId = c.CustodianID

