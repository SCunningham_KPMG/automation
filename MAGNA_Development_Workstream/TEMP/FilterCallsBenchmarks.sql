-- All calls that fit benchmarks for all departments (segements)
SELECT c.CallId, c.LocationId, sc.DepartmentID, c.UTCStartTime, c.UTCEndTime, sc.DisplayName
FROM [Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN [filter].[Filter] f
	ON  c.UTCStartTime >= f.UTCStartDateTime
	AND c.UTCEndTime < f.UTCStopDateTime
	AND f.FilterTypeID IN (1,2)
	and f.departmentId = sc.DepartmentID
	and f.locationId = c.LocationID
WHERE c.UTCStartTime >= '20180213' and c.UTCEndTime < '20180214'
