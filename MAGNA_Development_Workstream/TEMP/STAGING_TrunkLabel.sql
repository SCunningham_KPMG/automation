 USE [Voice_Data]
GO

INSERT INTO [dbo].[Staging_TrunkLabel]
           ([CallID]
           ,[Station]
           ,[TraderId]
           ,[deviceTypeID]
           ,[TrunkLabel]
           ,[FirstName]
           ,[LastName]
           ,DialedDigits
		   ,Location
)

 Select 
	iInteractionId as CallId
	,nvcStation as Station
	,nvcAgentId as TraderId
	,tiDeviceTypeID as DeviceTypeId
	,nvcTrunkLabel as TrunkLabel
	,nvcFirstName as FirstName
	,nvcLastname as LastName
	,DialledDigits as DialedDigits
	,'LDN' as Location
 FROM [Voice_Data].[dbo].[LDN_TrunkLabel]
 

 
INSERT INTO [dbo].[Staging_TrunkLabel]
           ([CallID]
           ,[Station]
           ,[TraderId]
           ,[deviceTypeID]
           ,[TrunkLabel]
           ,[FirstName]
           ,[LastName]
           ,DialedDigits
		   ,Location
)

 Select 
	iInteractionId as CallId
	,nvcStation as Station
	,nvcAgentId as TraderId
	,tiDeviceTypeID as DeviceTypeId
	,nvcTrunkLabel as TrunkLabel
	,nvcFirstName as FirstName
	,nvcLastname as LastName
	,DialedDigits as DialedDigits
	,'HKG' as Location
 FROM [Voice_Data].[dbo].[HKG_TrunkLabel]


 
INSERT INTO [dbo].[Staging_TrunkLabel]
           ([CallID]
           ,[Station]
           ,[TraderId]
           ,[deviceTypeID]
           ,[TrunkLabel]
           ,[FirstName]
           ,[LastName]
           ,DialedDigits
		   ,Location
)

 Select 
	iInteractionId as CallId
	,nvcStation as Station
	,nvcAgentId as TraderId
	,tiDeviceTypeID as DeviceTypeId
	,nvcTrunkLabel as TrunkLabel
	,nvcFirstName as FirstName
	,nvcLastname as LastName
	,DialedDigits as DialedDigits
	,'NYC' as Location
 FROM [Voice_Data].[dbo].[NYC_TrunkLabel]
