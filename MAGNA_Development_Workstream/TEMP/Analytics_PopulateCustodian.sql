USE [Voice_Analytics]
GO
SET IDENTITY_INSERT [shared].[Custodian] ON 

INSERT INTO [shared].[Custodian]
           ([CustodianID]
		   ,[FirstName]
           ,[LastName]
		   ,[DisplayName]
           ,[StartDate]
           ,[EndDate]
           ,[IsActive]
           ,[IsAComm]
           ,[IsEComm]
		   ,[IsInScopeTraderFollow]
           ,[EmployeeNumber]
           ,[DepartmentID]
           ,[LocationID]
           ,[DateCreated])

  SELECT 
		[CustodianId],
		CASE
			WHEN  CHARINDEX(',',CustodianDisplayName) > 0 
			THEN SUBSTRING(CustodianDisplayName, CHARINDEX(',',CustodianDisplayName) + 1, LEN(CustodianDisplayName) - CHARINDEX(',',CustodianDisplayName)) 
			ELSE '' 
		END As FirstName,
		CASE
			WHEN  CHARINDEX(',',CustodianDisplayName) > 0 
			THEN LEFT(CustodianDisplayName,  CHARINDEX(',',CustodianDisplayName) -1) 
			ELSE '' 
		END As lastName,
		CustodianDisplayName,
		[StartDate],
		COALESCE([EndDate],'99991231'),
		IsInScope as IsActive,
		1 as IsAComm,
		0 as IsEComm,
		IsInScope,
		[EmployeeNo],
		d.DepartmentID as DepartmentId,
		loc.locationId as LocationId,
		getdate() as DateCreated
  FROM [Audio2_Metadata].[dbo].[Custodians_Staging] c
  left JOIN Voice_Analytics.[shared].[Location] loc
	on c.location = loc.city
  left join  Voice_Analytics.[shared].BusinessUnit b
	on b.Code = c.AssetClass
  left join Voice_Analytics.[shared].[Department] d
	on d.Name = 
	CASE
	WHEN CHARINDEX('-',[KPMG Combined Segment]) > 0 
		THEN SUBSTRING([KPMG Combined Segment], CHARINDEX('-',[KPMG Combined Segment]) + 1, LEN([KPMG Combined Segment]) - CHARINDEX(',',[KPMG Combined Segment]))
		ELSE 'BSM'
	END
	and b.BusinessUnitID = d.BusinessUnitID

WHERE d.DepartmentID is NOT null
AND loc.locationId IS NOT NULL
GO
SET IDENTITY_INSERT [shared].[Custodian] OFF

/*
SELECT c.* 
FROM [Audio2_Metadata].[dbo].[Custodians_Staging] c
LEFT JOIN Voice_Analytics.shared.Custodian cu
	ON c.CustodianId = cu.custodianId
	AND c.StartDate = cu.StartDate
	AND COALESCE(c.EndDate,'99991231') = COALESCE(cu.EndDate,'99991231')
WHERE cu.CustodianID IS NULL


select * from shared.custodian where custodianid = 627

update shared.custodian set Enddate = '20180329', IsActive=0, IsInScopeTraderFollow=0  where custodianid = 627
select * from shared.custodian
delete shared.custodianidentity
delete [filter].[CustodianDateFollowed]
delete shared.custodian
*/