select c.*, cnew.status
FROM [Audio2_Metadata].[report].[TraderInteractions] C
LEFT JOIN [Voice_Analytics].audio.call cnew
	ON c.InteractionID = cnew.callId
WHERE CAST(c.LocalStartTime as date) >= '20180322' AND CAST(c.LocalStartTime as date) < '20180404' AND C.custodianLocation = 'LDN'
AND cnew.callid is null

select cnew.*
FROM [Voice_Analytics].audio.call cnew
LEFT JOIN [Audio2_Metadata].[report].[TraderInteractions] C
	ON c.InteractionID = cnew.callId
WHERE CAST(cnew.LocalStartTime as date) >= '20180322' AND CAST(cnew.LocalStartTime as date) < '20180404'  AND Cnew.LocationID = 1 and cnew.Status <> 61 -- custodian not found
AND c.InteractionID is null


SELECT * from voice_data.dbo.PreAnalytics_Metadata where CallId IN
( select CAST(cnew.CallID as varchar(100))
FROM [Voice_Analytics].audio.call cnew
LEFT JOIN [Audio2_Metadata].[report].[TraderInteractions] C
	ON c.InteractionID = cnew.callId
WHERE CAST(cnew.LocalStartTime as date) >= '20180322' AND CAST(cnew.LocalStartTime as date) < '20180404'  AND Cnew.LocationID = 2 and cnew.Status <> 61 -- custodian not found
AND c.InteractionID is null)