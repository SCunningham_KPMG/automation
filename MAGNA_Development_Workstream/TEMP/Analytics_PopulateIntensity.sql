SELECT c.callId, Intensity.IntensityGroup, sc.CustodianID, c.LocalStartTime, c.LocalEndTime, c.DialledNumber, c.LocationId
FROM [Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN
(
	SELECT 
		sci.CustodianID, c.DialledNumber, 
		datepart (year, localstarttime) yrcall, 
		datepart (month, localstarttime) monthcall, 
		datepart (day, localstarttime) daycall, 
		datepart (hour, localstarttime) hourcall, min(localstarttime) mintime , max(localstarttime) maxtime, 
		MIN(NEWID()) as IntensityGroup
	FROM [Voice_Analytics].[audio].[Call] c
	INNER JOIN [audio].[CallCustodianIdentity] acci
		ON c.callId = acci.CallID
	INNER JOIN [shared].[CustodianIdentity] sci
		ON acci.CustodianIdentityID = sci.CustodianIdentityID
	INNER JOIN [shared].[Custodian] sc
		ON sci.CustodianID = sc.custodianId
	INNER JOIN [shared].Department d
		ON sc.DepartmentID = d.DepartmentID
	INNER JOIN [shared].[BusinessUnit] bu
		ON d.BusinessUnitID = bu.BusinessUnitID
	where DialledNumber <> ''
	group by 
		sci.CustodianID, c.DialledNumber, 
		datepart (year, localstarttime), 
		datepart (month, localstarttime), 
		datepart (day, localstarttime), 
		datepart (hour, localstarttime) 
	having count(*) > = 4
) intensity
	on sc.CustodianID = intensity.CustodianID
	and c.DialledNumber = intensity.DialledNumber
	and c.LocalStartTime >= intensity.mintime
	and c.LocalStartTime <= intensity.maxtime
order by 2