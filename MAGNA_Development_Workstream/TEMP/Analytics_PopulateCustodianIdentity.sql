USE Voice_Analytics
GO

SELECT * 
FROM [Audio2_Metadata]..[CustodianTraderIdentity] cti
LEFT JOIN Voice_Analytics.[shared].[CustodianIdentity] ci
	ON cti.CustodianId = ci.CustodianID
--	AND cti.startdate= ci.startdate
--	AND COALESCE(cti.enddate,'99991231') = COALESCE(ci.enddate,'99991231')
WHERE ci.CustodianID is null


INSERT INTO [shared].[CustodianIdentity]
           ([CustodianIdentity]
           ,[IdentityType]
           ,[EndDate]
           ,[StartDate]
           ,[CustodianID]
           ,[IsActive]
           ,[DateCreated])
SELECT 
		[TraderID],
		1,
		CASE WHEN EndDate IS NOT NULL THEN EndDate ELSE '99991231' END as EndDate,
		StartDate,
		[CustodianId],
		CASE WHEN EndDate Is NULL THEN 1 ELSE 0 END as IsActive,
		getdate() as DateCreated
 FROM [Audio2_Metadata]..[CustodianTraderIdentity]
GO

INSERT INTO [shared].[CustodianIdentity]
           ([CustodianIdentity]
           ,[IdentityType]
           ,[EndDate]
           ,[StartDate]
           ,[CustodianID]
           ,[IsActive]
           ,[DateCreated])
SELECT 
		MobileNo,
		2,
		CASE WHEN EndDate IS NOT NULL THEN EndDate ELSE '99991231' END as EndDate,
		StartDate,
		[CustodianId],
		CASE WHEN EndDate Is NULL THEN 1 ELSE 0 END as IsActive,
		getdate() as DateCreated
 FROM [Audio2_Metadata]..[CustodianMobileIdentity]
GO

INSERT INTO [shared].[CustodianIdentity]
           ([CustodianIdentity]
           ,[IdentityType]
           ,[EndDate]
           ,[StartDate]
           ,[CustodianID]
           ,[IsActive]
           ,[DateCreated])
SELECT 
		[CiscoNo],
		3,
		CASE WHEN EndDate IS NOT NULL THEN EndDate ELSE '99991231' END as EndDate,
		StartDate,
		[CustodianId],
		CASE WHEN EndDate Is NULL THEN 1 ELSE 0 END as IsActive,
		getdate() as DateCreated
 FROM [Audio2_Metadata]..[CustodianCiscoIdentity]
GO





