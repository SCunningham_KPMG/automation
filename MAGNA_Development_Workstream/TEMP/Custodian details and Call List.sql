SELECT sc.CustodianID, MAX(CAST(ac.LocalStartTime AS DATE)) LastCall  
FROM audio.call ac
INNER JOIN audio.CallCustodianIdentity acci
	ON ac.callId = acci.CallID
INNER JOIN shared.CustodianIdentity sc
	ON acci.CustodianIdentityID = sc.CustodianIdentityID
GROUP BY sc.CustodianID

SELECT 
	sc.CustodianID,
	sc.[EmployeeNumber], 
	sc.[DisplayName] as CustodianName, 
	sl.City as Location, 
	sbu.code as AssetClass, 
	sbu.code + case when sd.code <> '' then '-' + sd.code else '' end as Segment,
	sc.StartDate as StartDate,
	scci1.CustodianIdentity as TraderId,
	scci2.CustodianIdentity as MobileNo,
	scci3.CustodianIdentity as Cisco,
	REPLACE(scci5.CustodianIdentity, '@bloomberg.net','') as BloombergAlias,
	scci4.CustodianIdentity as Email
FROM [shared].[Custodian] sc
INNER JOIN [shared].[Location] sl
	ON sc.LocationID = sl.LocationID
INNER JOIN [shared].[Department] sd
	ON sc.DepartmentID = sd.DepartmentID
INNER JOIN [shared].[BusinessUnit] sbu
	ON sd.BusinessUnitID = sbu.BusinessUnitID
LEFT JOIN [shared].CustodianIdentity scci1
	ON sc.CustodianID = scci1.CustodianID
	and scci1.IdentityType = 1
	and scci1.isactive = 1
LEFT JOIN [shared].CustodianIdentity scci2
	ON sc.CustodianID = scci2.CustodianID
	and scci2.IdentityType = 2
	and scci2.IsActive = 1
LEFT JOIN [shared].CustodianIdentity scci3
	ON sc.CustodianID = scci3.CustodianID
	and scci3.IdentityType = 3
	and scci3.IsActive = 1
LEFT JOIN [shared].CustodianIdentity scci4
	ON sc.CustodianID = scci4.CustodianID
	and scci4.IdentityType = 4
	and scci4.IsActive = 1
	and scci4.CustodianIdentity like '%@%' AND scci4.CustodianIdentity not like '%@bloomberg.net'
LEFT JOIN [shared].CustodianIdentity scci5
	ON sc.CustodianID = scci5.CustodianID
	and scci5.IdentityType = 4
	and scci5.IsActive = 1
	and scci5.CustodianIdentity like '%@bloomberg.net'
WHERE sc.EndDate >= getdate()
ORDER BY 1

