/*
FPS Hits
CR Hits
DELETE FROM [report].[ReviewDashboardDailyStats]
Query 11
*/


/*
INSERT INTO [report].[ReviewDashboardDailyStats]
           ([IngestDate]
		   ,[ReviewDate]
           ,[Location]
           ,[AssetClass]
           ,[Segment]
           ,[Category]
           ,[SelectedForReview]
           ,[HasBeenFPReviewed]
		   ,[FPR_Relevant]
           ,[ReleasedForSPR]
           ,[HasBeenSPReviewed]
		   ,[SPR_Relevant]
           ,[ReviewerLocation]
           ,[NumberOfCalls]
           ,[Duration]
		   ,[FPS_Hits]
		   ,[CR_Hits]
           )
*/		
SELECT 
	cast(sm.mediacreationdate as date) IngestDate, 
	CAST (COALESCE(ar.FPR_LastReviewedOn,'99991231') as date) ReviewDate,
	ar.CustodianLocation Location,
	ar.CustodianAssetClass AssetClass,
	ar.Segment,
	cc.ShortName Category, 
	ar.SelectedForReview SelectedForReview, 
	ar.HasBeenFPReviewed HasBeenFPReviewed,
	COALESCE(ar.FPR_Relevant,0) FPR_Relevant,
	COALESCE(ar.ReleasedForSPR,0) ReleasedForSPR,
	ar.HasBeenSPReviewed HasBeenSPReviewed,
	CASE WHEN ar.spr_relevance = 'Relevant' THEN 1 ELSE 0 END AS SPR_Relevant,
	COALESCE(u.UserGroup,'Undefined') ReviewerLocation,
	count(*) NumberOfCalls,
	SUM(DurationInSeconds) as Duration,
	COALESCE(SUM(NumHits),0) AS FPS_Hits,
	SUM (CASE WHEN lf.FPRStatus = 'False Positive' THEN NumHits ELSE 0 END) AS CR_Hits
FROM [Audio2_Metadata].[report].[Audio2Report] ar
INNER JOIN [Audio2_Metadata].[nexidia].[SourceMedia] sm
	ON ar.InteractionID = sm.InteractionID
INNER JOIN [Voice_Analytics].report.[CallCategoryFilter] ccf
	ON ar.SignalIdentifiers = ccf.name
INNER JOIN [Voice_Analytics].[report].[CallCategory] cc
	ON ccf.CallCategoryId = cc.CallCategoryId
LEFT JOIN Audio2_Metadata.nexidia.Users u
	ON ar.FPR_ReviewedBy = u.DisplayName
LEFT JOIN [Audio2_Metadata].report.LexiconFeedBack lf
	ON sm.SourceMediaId = lf.SourceMediaId
WHERE cast(sm.mediacreationdate as date) >= '20180201'
AND ar.SignalIdentifiers is not null
GROUP BY 
	cast(sm.mediacreationdate as date), 
	CAST (ar.FPR_LastReviewedOn as date),
	ar.CustodianLocation,
	ar.CustodianAssetClass,
	ar.Segment,
	cc.ShortName,
	ar.SelectedForReview ,
	ar.HasBeenFPReviewed,
	FPR_Relevant,
	COALESCE(ar.ReleasedForSPR,0),
	ar.HasBeenSPReviewed,
	CASE WHEN ar.spr_relevance = 'Relevant' THEN 1 ELSE 0 END,
	COALESCE(u.UserGroup,'Undefined')
ORDER BY 1,2,3,4,5,6,7,8,9,10,11,12





/*

select top 1000 * FROM [Audio2_Metadata].[report].[Audio2Report] ar where HasBeenFPReviewed = 1 
and SelectedForReview = 1 and FPR_Relevant=1
order by localstarttime desc

DELETE FROM [report].[ReviewedByReviewDateDailyStats]

select spr_relevance, count(*) from [Audio2_Metadata].[report].[Audio2Report] where ReleasedForSPR=1 group by spr_relevance
*/

SELECT DISTINCT FPR_LastReviewedOn FROM [Audio2_Metadata].[report].[Audio2Report] ORDER BY 1

/*
INSERT INTO [report].[ReviewedByReviewDateDailyStats]
           ([ReviewDate]
           ,[Location]
           ,[AssetClass]
           ,[Segment]
           ,[Category]
           ,[ReviewerLocation]
		   ,[FPR_Relevant]
           ,[NumberOfCalls]
           ,[Duration]
           )

SELECT 
	COALESCE(FPR_LastReviewedOn,'99991231'), 
	ar.CustodianLocation Location,
	ar.CustodianAssetClass AssetClass,
	ar.Segment,
	cc.ShortName Category, 
	COALESCE(u.UserGroup,'Undefined') ReviewerLocation,
	COALESCE(ar.FPR_Relevant,0) FPR_Relevant,
	count(*) NumberOfCalls,
	SUM(DurationInSeconds) as Duration
FROM [Audio2_Metadata].[report].[Audio2Report] ar
INNER JOIN [Voice_Analytics].report.[CallCategoryFilter] ccf
	ON ar.SignalIdentifiers = ccf.name
INNER JOIN [Voice_Analytics].[report].[CallCategory] cc
	ON ccf.CallCategoryId = cc.CallCategoryId
LEFT JOIN Audio2_Metadata.nexidia.Users u
	ON ar.FPR_ReviewedBy = u.DisplayName
WHERE(FPR_LastReviewedOn >= '20180201' OR FPR_LastReviewedOn IS NULL)
AND ar.SignalIdentifiers is not null
GROUP BY 
	FPR_LastReviewedOn, 
	ar.CustodianLocation,
	ar.CustodianAssetClass,
	ar.Segment,
	cc.ShortName,
	COALESCE(u.UserGroup,'Undefined'),
	COALESCE(ar.FPR_Relevant,0)
ORDER BY 1,2,3,4,5,6
*/
-- Backlog

INSERT INTO [report].[ReviewDashboardBacklog]
           ([Location]
           ,[AssetClass]
           ,[Segment]
           ,[ReviewerLocation]
           ,[NumberOfCalls]
           ,[Duration]
           )
SELECT 
	ar.CustodianLocation Location,
	ar.CustodianAssetClass AssetClass,
	ar.Segment,
	COALESCE(u.UserGroup,'Undefined') ReviewerLocation,
	count(*) NumberOfCalls,
	SUM(DurationInSeconds) as Duration
FROM [Audio2_Metadata].[report].[Audio2Report] ar
INNER JOIN [Audio2_Metadata].[nexidia].[SourceMedia] sm
	ON ar.InteractionID = sm.InteractionID
LEFT JOIN Audio2_Metadata.nexidia.Users u
	ON ar.FPR_ReviewedBy = u.DisplayName
WHERE CAST(ar.LocalStartTime as DATE) <= '20180208'
AND ar.SignalIdentifiers is not null
AND	COALESCE(ar.HasBeenFPReviewed,0) =0
GROUP BY 
	ar.CustodianLocation,
	ar.CustodianAssetClass,
	ar.Segment,
	COALESCE(u.UserGroup,'Undefined')
ORDER BY 1,2,3,4


SELECT SUM(Duration) Duration, SUM(NumberOfCalls) NumberOfCalls FROM
[report].[ReviewDashboardBacklog]
 wh


select HasBeenFpReviewed, count(*) from [report].ReviewDashboardDailyStats group by  HasBeenFpReviewed
select SUM(Numberofcalls) from [report].[ReviewedByReviewDateDailyStats]





select top 10 * from  [Audio2_Metadata].[report].[Audio2Report] ar WHERE CAST(ar.LocalStartTime as DATE) < '20180208'
AND ar.SignalIdentifiers is not null
AND	COALESCE(ar.HasBeenFPReviewed,0) =1
AND SelectedForReview = 1
AND FPR_LastReviewedOn IS NULL
order by LocalStartTime desc

SELECT 
	count(*) NumberOfCalls,
	SUM(DurationInSeconds) as Duration
FROM [Audio2_Metadata].[report].[Audio2Report] ar
INNER JOIN [Audio2_Metadata].[nexidia].[SourceMedia] sm
	ON ar.InteractionID = sm.InteractionID
LEFT JOIN Audio2_Metadata.nexidia.Users u
	ON ar.FPR_ReviewedBy = u.DisplayName
WHERE CAST(ar.LocalStartTime as DATE) < '20180208'
AND ar.SignalIdentifiers is not null
AND	COALESCE(ar.HasBeenFPReviewed,0) =0
AND SelectedForReview = 1
AND FPR_LastReviewedOn <= '20180403'

