USE [Voice_Analytics]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [filter].[Filter](
	[FilterID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Timestamp] [time](7) NULL,
	[TimeZone] [varchar](4) NULL,
	[FilterTypeID] [int] NULL,
	[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_Benchmark_DateCreated]  DEFAULT (getdate()),
	[MinutesBefore] [int] NULL,
	[MinutesAfter] [int] NULL,
	[ExpiryTypeId] [int] NULL,
 CONSTRAINT [pk_BenchmarkNameList] PRIMARY KEY CLUSTERED 
(
	[FilterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [filter].[FilterType](
	[FilterTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](1000) NULL,
	[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_FilterType_DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_FilterType] PRIMARY KEY CLUSTERED 
(
	[FilterTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [filter].[FilterExpiryType](
	[FilterExpiryTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](1000) NULL,
	[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_FilterExpiryType_DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_FilterExpiryType] PRIMARY KEY CLUSTERED 
(
	[FilterExpiryTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

