USE [Voice_Analytics]
GO

ALTER TABLE [audio].[Call] DROP CONSTRAINT [FK_Call_TrunkLabel]
GO

ALTER TABLE [audio].[Call] DROP CONSTRAINT [FK_Call_Language]
GO

ALTER TABLE [audio].[Call] DROP CONSTRAINT [FK_Call_batch]
GO

ALTER TABLE [audio].[Call] DROP CONSTRAINT [DF_Call_Include]
GO

ALTER TABLE [audio].[Call] DROP CONSTRAINT [DF_Call_DateCreated]
GO

/****** Object:  Table [audio].[Call]    Script Date: 26/02/2018 13:09:30 ******/
DROP TABLE [audio].[Call]
GO

/****** Object:  Table [audio].[Call]    Script Date: 26/02/2018 13:09:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [audio].[Call](
	[CallID] [bigint] NOT NULL,
	[LocalStartTime] [datetime2](0) NULL,
	[UTCStartTime] [datetime2](0) NULL,
	[LocalEndTime] [datetime2](0) NULL,
	[UTCEndTime] [datetime2](0) NULL,
	[DurationInSeconds] [int] NULL,
	[TrunkLabelID] [int] NULL,
	[ParentID] [bigint] NULL,
	[Channel] [varchar](50) NULL,
	[ClientReference] [varchar](100) NULL,
	[DialledNumber] [varchar](100) NULL,
	[PhoneNumber] [varchar](100) NULL,
	[CLIANI] [varchar](20) NULL,
	[LanguageID] [int] NULL,
	[BatchID] [int] NULL,
	[DirectionId] [int] NULL,
	[InitiatorID] [int] NULL,
	[LocationID] [int] NOT NULL,
	[DateCreated] [smalldatetime] NULL,
	[Include] [bit] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Call] PRIMARY KEY CLUSTERED 
(
	[CallID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [audio].[Call] ADD  CONSTRAINT [DF_Call_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [audio].[Call] ADD  CONSTRAINT [DF_Call_Include]  DEFAULT ((1)) FOR [Include]
GO

ALTER TABLE [audio].[Call]  WITH CHECK ADD  CONSTRAINT [FK_Call_batch] FOREIGN KEY([BatchID])
REFERENCES [audio].[Batch] ([BatchID])
GO

ALTER TABLE [audio].[Call] CHECK CONSTRAINT [FK_Call_batch]
GO

ALTER TABLE [audio].[Call]  WITH CHECK ADD  CONSTRAINT [FK_Call_Language] FOREIGN KEY([LanguageID])
REFERENCES [shared].[Language] ([LanguageID])
GO

ALTER TABLE [audio].[Call] CHECK CONSTRAINT [FK_Call_Language]
GO

ALTER TABLE [audio].[Call]  WITH CHECK ADD  CONSTRAINT [FK_Call_TrunkLabel] FOREIGN KEY([TrunkLabelID])
REFERENCES [audio].[TrunkLabel] ([TrunkLabelID])
GO

ALTER TABLE [audio].[Call] CHECK CONSTRAINT [FK_Call_TrunkLabel]
GO


