USE [Voice_Analytics]
GO
DECLARE @callFollowDivisor INT
SELECT @callFollowDivisor = ConfigValue FROM dbo.Config WHERE configName = 'CallFollowDivisor'
SELECT @callFollowDivisor


INSERT INTO [working].[TF_NumberToFollow]
           ([LocationID]
           ,[BusinessUnitID]
           ,[DepartmentID]
           ,[NumberOfCustodians]
           ,[NumberToFollow]
           )

select c.locationId, bu.BusinessUnitID, c.departmentid, count(*) NumberCustodians, COUNT(*)/40 + 1 NumberToFollow
from [shared].[Custodian] c
inner join shared.Department d
	on c.DepartmentID = d.DepartmentID
inner join shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID
where c.isactive=1
and c.IsInScopeTraderFollow = 1
group by c.locationId, bu.BusinessUnitID, c.departmentid



USE [Voice_Analytics]
GO

INSERT INTO [working].[TF_LastFollowed]
           ([LocationID]
           ,[BusinessUnitID]
           ,[DepartmentID]
           ,[CustodianId]
           ,[LastFollowed]
)
select sc.LocationID, d.BusinessUnitID, d.departmentId, sc.CustodianID, MAX(ISNULL(fcdf.DateFollowed,'19000101')) LastFollowed
from audio.Call c
inner join [audio].[CallCustodianIdentity] ci
	on c.CallID = ci.CallID
inner join shared.CustodianIdentity sci
	on ci.CustodianIdentityID = sci.CustodianIdentityID
inner join shared.Custodian sc
	on sci.CustodianID = sc.CustodianID
	and sc.IsInScopeTraderFollow = 1
	and sc.IsActive = 1
inner join shared.Department d
	on sc.DepartmentID = d.DepartmentID
left join filter.custodiandatefollowed fcdf
	on sc.CustodianID = fcdf.custodianId
where c.LocalStartTime >= '20180214' and c.LocalEndTime < '20180215'
group by sc.LocationID, d.BusinessUnitID, d.departmentId, sc.CustodianID


select * from [working].[TF_NumberToFollow]

INSERT [filter].[CustodianDateFollowed] (CustodianId, DateFollowed) SELECT lf.custodianId, '20180214'
--SELECT lf.locationId, lf.businessunitid, lf.departmentid, lf.custodianId
FROM
(
	select 
		lf.LocationId, lf.BusinessUnitId, lf.DepartmentId, lf.custodianId, 
		row_number() over (partition by lf.LocationId, lf.BusinessUnitId, lf.DepartmentId order by lf.lastfollowed) seq
	from  [working].[TF_LastFollowed] lf
) lf 
inner join [working].[TF_NumberToFollow] ntf
	on lf.locationId = ntf.locationId
	and lf.businessunitid = ntf.businessunitid
	and lf.departmentid = ntf.departmentid

where lf.seq <= ntf.NumberToFollow
--order by 1,2,3

select * from [filter].[CustodianDateFollowed]
-- call follow for the day.
insert [audio].[CallFiltersSatisifed] (CallId, FilterTypeId)
	select c.CallId, 5
	from audio.Call c
	inner join [audio].[CallCustodianIdentity] ci
		on c.CallID = ci.CallID
	inner join shared.CustodianIdentity sci
		on ci.CustodianIdentityID = sci.CustodianIdentityID
	inner join  [filter].[CustodianDateFollowed] cdf
		on sci.CustodianID = cdf.CustodianId
		and (c.LocalStartTime >= '20180214' and c.LocalEndTime < '20180215')
		and cdf.status = 0