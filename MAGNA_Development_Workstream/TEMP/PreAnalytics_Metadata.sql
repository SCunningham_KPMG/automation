USE [Voice_Data]
GO

--ALTER TABLE [dbo].[PreAnalytcis_Metadata] DROP CONSTRAINT [DF__PreAnalyt__Statu__6383C8BA]
--GO

DROP TABLE [dbo].[PreAnalytics_Metadata]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreAnalytics_Metadata](
	[Location] [varchar](20) NOT NULL,
	[CallID] [bigint] NOT NULL,
	[CallParentID] [bigint] NULL,
	TraderId [int] NULL,
	[LocalStartTime] [datetime] NULL,
	[LocalStopTime] [datetime] NULL,
	[UTCStartTime] [datetime] NULL,
	[UTCStopTime] [datetime] NULL,
	[Duration] [int] NULL,
	[InitiatorTRID] [varchar](500) NULL,
	[MediaFileName] [varchar](8000) NULL,
	[MediaPath] [varchar](500) NULL,
	[MediaFileId] [int] NULL,
	[TrunkLabel] [varchar](500) NULL,
	[DeviceId] [int] NULL,
	[InitialDeviceTypeID] [int] NULL,
	[FinalDeviceTypeID] [int] NULL,
	[CallDirection] [varchar](500) NULL,
	[Station] [varchar](500) NULL,
	[PhoneNumber] [varchar](500) NULL,
	[DialedNumber] [varchar](500) NULL,
	[ParticipantIdentityType] [int] NULL,
	[ParticipantIdentity] [varchar](500) NULL,
	[CopiedToAnalyticsDateTime] [datetime] NULL,
	[Status] [int] NOT NULL,
	[FirstName] [varchar](500) NULL,
	[LastName] [varchar](500) NULL,
	[MiddleName] [varchar](500) NULL,
 CONSTRAINT [PK_SegmID] PRIMARY KEY CLUSTERED 
(
	[Location] ASC,
	[CallID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PreAnalytics_Metadata] ADD  DEFAULT ((0)) FOR [Status]
GO


