USE Voice_Analytics
GO

SET IDENTITY_INSERT [shared].[BusinessUnit] ON 

INSERT [shared].[BusinessUnit] ([BusinessUnitID], [Name], [Code], [IsActive], [DateCreated]) VALUES (1, N'FX', 'FX', 1, NULL)
INSERT [shared].[BusinessUnit] ([BusinessUnitID], [Name], [Code], [IsActive], [DateCreated]) VALUES (2, N'FI', 'FI', 1, NULL)
INSERT [shared].[BusinessUnit] ([BusinessUnitID], [Name], [Code], [IsActive], [DateCreated]) VALUES (3, N'BSM', 'BSM', 1, NULL)
INSERT [shared].[BusinessUnit] ([BusinessUnitID], [Name], [Code], [IsActive], [DateCreated]) VALUES (4, N'Equities', 'EQ', 1, NULL)
INSERT [shared].[BusinessUnit] ([BusinessUnitID], [Name], [Code], [IsActive], [DateCreated]) VALUES (5, N'CF', 'CF', 1, NULL)
SET IDENTITY_INSERT [shared].[BusinessUnit] OFF