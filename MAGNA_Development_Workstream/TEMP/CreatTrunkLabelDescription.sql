INSERT Voice_Analytics.shared.TrunkLabelDescription (TrunkLabel, FullTrunkLabel)
SELECT [Trunk label], MIN([Full trunk label]) TrunkDesc FROM Audio2_Metadata.dbo.TrunkLabelDescriptions 
WHERE [Trunk label] <> ''
GROUP BY [Trunk label]