/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ReportID]
      ,[Location]
      ,[AssetClass]
      ,[SendTo]
      ,[CC]
      ,[StartDate]
      ,[EndDate]
      ,[IsBAU]
      ,[IsEnhancedSurveillance]
      ,[IsInternal]
      ,[SubjectLine]
      ,[EmailBody]
      ,[ReportName]
      ,[AttachReport]
  FROM [Audio2_Metadata].[SSRS].[ReportRecipients]
  where ReportName like '%sme%'

 -- Change
 update [Audio2_Metadata].[SSRS].[ReportRecipients] SET
	SendTo = 'nuno.hortensio@hsbc.com;dorota.t.swies@hsbc.com' WHERE reportId IN (22,114,115,116) 
 -- Rollback
 update [Audio2_Metadata].[SSRS].[ReportRecipients] SET
	SendTo = 'nuno.hortensio@hsbc.com' WHERE reportId IN (22,114,115,116) 

-- Check table
select * from  [Audio2_Metadata].[SSRS].[ReportRecipients] WHERE reportId IN (22,114,115,116) 