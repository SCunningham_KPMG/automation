-- Out of hours
INSERT [audio].[CallFiltersSatisifed] (CallId, FilterTypeId)
SELECT  c.CallId, 6 --c.LocationID, c.LocalStartTime, ooh.*
FROM [Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
	AND ( DateName(dw,c.LocalStartTime) not in ('Saturday','Sunday') OR  DateName(dw,c.LocalEndTime) not in ('Saturday','Sunday'))
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN [shared].Department d
	ON sc.DepartmentID = d.DepartmentID
INNER JOIN [shared].[BusinessUnit] bu
	ON d.BusinessUnitID = bu.BusinessUnitID
inner join [filter].OutOfHours ooh
	ON (cast(c.LocalStartTime as time) <= ooh.LocalStartTime or cast(c.LocalEndTime as time) > ooh.localEndTime OR
		cast(c.LocalStartTime as time) > ooh.LocalEndTime or cast(c.LocalEndTime as time) < ooh.localStartTime
	)
	and ooh.businessUnitId = d.BusinessUnitID
	and ooh.locationId = c.LocationID
	and ooh.IsActive = 1
WHERE c.[status] = 0

-- weekends and holidays
INSERT [audio].[CallFiltersSatisifed] (CallId, FilterTypeId)
SELECT  c.CallId, 7 -- , c.LocalStartTime, c.LocalEndTime --c.LocationID, c.LocalStartTime, ooh.*
FROM [Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN [shared].Department d
	ON sc.DepartmentID = d.DepartmentID
INNER JOIN [shared].[BusinessUnit] bu
	ON d.BusinessUnitID = bu.BusinessUnitID
left join [filter].Holiday hol
	ON (cast(c.LocalStartTime as date) = hol.HolidayDate or cast(c.LocalEndTime as date) = hol.HolidayDate)
	and hol.LocationID = c.LocationID
	and hol.Active = 1
WHERE c.[status] = 0
	AND ( DateName(dw,c.LocalStartTime) in ('Saturday','Sunday') OR  DateName(dw,c.LocalEndTime) in ('Saturday','Sunday') or hol.HolidayDate is not null)

-- Intra-desk
INSERT [audio].[CallFiltersSatisifed] (CallId, FilterTypeId)
SELECT  c.CallId, 8 -- , c.LocalStartTime, c.LocalEndTime
FROM audio.call c
WHERE c.parentId IN
(
SELECT c.parentid--, d.DepartmentID, bu.BusinessUnitID, c.locationId
FROM 
[Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN [shared].Department d
	ON sc.DepartmentID = d.DepartmentID
INNER JOIN [shared].[BusinessUnit] bu
	ON d.BusinessUnitID = bu.BusinessUnitID
group by c.locationId, bu.BusinessUnitID, d.DepartmentID, c.parentid
HAVING COUNT(*) > 1
)

