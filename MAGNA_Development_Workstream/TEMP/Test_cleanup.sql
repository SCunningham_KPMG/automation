USE Voice_Analytics
GO

/*
select callid into #t from audio.call where CAST(LocalStartTime as date) < '20180321'

select  status, count(*) from audio.call group by status

update audio.call set status = 62 where status in (74,75)

delete [audio].[CallCustodianIdentity] where callid in (select callid from #t)
delete [audio].[CallFiltersSatisfied] where filterTypeId in (8,10)
delete [audio].[File] where fileid in  (select fileid from audio.callfile where callid in (select callid from #t))
delete [audio].CallFile where callid in (select callid from #t)
delete from audio.call where callid  in (select callid from #t)
*/

/*
USE Voice_Analytics
GO
truncate table [audio].[CallCustodianIdentity]
truncate table [audio].[CallFile]
truncate table [audio].[File]
truncate table [audio].[CallFiltersSatisfied]
truncate table [audio].[CallFilterIntensityGroup]
delete [audio].[Call]


truncate table [filter].[OutputFile]
truncate table [filter].[OutputNoFilter]
truncate table [filter].[CustodianDateFollowed]

delete [dbo].[LocationDataStatus]

truncate table Voice_data.[dbo].[PreAnalytics_Metadata]
truncate table Voice_data.[dbo].[PreAnalytics_TrunkLabels]

update voice_data.[dbo].[Staging_Metadata] set CopyStatus = 0
update voice_data.[dbo].[Staging_TrunkLabel] set CopyStatus = 0
update voice_data.[dbo].[DataflowStatus] set MetadataCopiedToPreAnalytics = NULL, TrunkDataCopiedToPreAnalytics=null where MetadataCopiedToPreAnalytics <> '19000101'
delete dbo.errorlog
*/
select  replace(filename, 'Metadata_Metadata','Metadata') , * from voice_data.[dbo].[DataflowStatus]
order by replace(filename, 'Metadata_Metadata','Metadata') desc, 5
