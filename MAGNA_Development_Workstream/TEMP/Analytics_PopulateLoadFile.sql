USE [Voice_Analytics]
GO
INSERT INTO [filter].[outputFile] (
           [CallID], filterTypeId )

SELECT [CallID], max(filterTypeId)
FROM [Voice_Analytics].[audio].[CallFiltersSatisifed] 
WHERE Status is NULL
GROUP BY CallId

select * from [Voice_Analytics].[filter].[outputFile] where status = 1




UPDATE [Voice_Analytics].[filter].[outputFile]
SET MediaFileName = f.name,
localStartTime = c.LocalStartTime,
localEndTime = c.localEndTime,
durationInSeconds =  c.DurationInSeconds,
deviceType = dt.name,
trunkLabel = c.TrunkLabel,
traderId = scci2.CustodianIdentity,
traderName = sc.DisplayName,
combinedBuAndDepartment = bu.code + case when d.code <> '' then '-' + d.code else '' end,
callDirection = cd.Name,
dialedNumber = c.DialledNumber,
CallLineIdentifier = c.CLIANI,
PhoneNumber = c.PhoneNumber,
SignalIdentifiers = loc.City + '-' + bu.Code + '-' + ft.code,
Comment = c.ParentID,
UseForLoadFile = 1,
location = loc.city,
Status = 1
FROM [Voice_Analytics].[filter].[outputFile] o
INNER JOIN audio.Call c 
	ON o.callId = c.CallID
INNER JOIN audio.CallDirection cd
	ON c.DirectionId = cd.CallDirectionID
LEFT JOIN audio.CallFile cf
	ON c.callId = cf.CallID
LEFT JOIN audio.[File] f
	ON cf.FileID = f.fileId
INNER JOIN filter.FilterType ft
		ON o.filterTypeId = ft.filterTypeId
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity scci
	ON cci.CustodianIdentityID = scci.CustodianIdentityID
LEFT JOIN audio.deviceType dt
	on cci.DeviceTypeID = dt.DeviceTypeID
INNER JOIN shared.Custodian sc
	ON scci.CustodianID = sc.CustodianID
INNER JOIN shared.Department d
	on sc.DepartmentID = d.DepartmentID
INNER JOIN shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
LEFT JOIN shared.CustodianIdentity scci2
	ON sc.CustodianID = scci2.CustodianID AND scci2.IdentityType = 1
INNER JOIN shared.location loc
	ON loc.LocationID = sc.LocationID
WHERE o.Status = 0


-- ---------------------------------------------------------------------------------------------------------------------------------------
SELECT * from audio.call where callid = 6521658473490493514

SELECT *
FROM [Voice_Analytics].[filter].[outputFile] o
INNER JOIN audio.Call c 
	ON o.callId = c.CallID
INNER JOIN audio.CallDirection cd
	ON c.DirectionId = cd.CallDirectionID
INNER JOIN audio.CallFile cf
	ON c.callId = cf.CallID
INNER JOIN audio.[File] f
	ON cf.FileID = f.fileId
INNER JOIN filter.FilterType ft
		ON o.filterTypeId = ft.filterTypeId
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity scci
	ON cci.CustodianIdentityID = scci.CustodianIdentityID
INNER JOIN shared.Custodian sc
	ON scci.CustodianID = sc.CustodianID
INNER JOIN shared.Department d
	on sc.DepartmentID = d.DepartmentID
INNER JOIN shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
LEFT JOIN shared.CustodianIdentity scci2
	ON sc.CustodianID = scci2.CustodianID AND scci2.IdentityType = 1
INNER JOIN shared.location loc
	ON loc.LocationID = sc.LocationID
WHERE o.callid = 6521658473490493514

select * from shared.custodian where custodianId = 1608
select * from shared.custodianIdentity where custodianId = 1608

WHERE o.Status = 0


SELECT o.[CallID], f.name as MediaFileName, c.LocalStartTime, c.LocalEndTime, c.DurationInSeconds, '' as devicetype, c.TrunkLabel, 
scci2.CustodianIdentity as TraderId, sc.DisplayName as TraderName,
bu.code + case when d.code <> '' then '-' + d.code else '' end as CominedBUandDepartment,  
cd.Name as CallDirection, 
c.DialledNumber, c.CLIANI as CallLineIdentifier, c.PhoneNumber as PhoneNumber, loc.City + '-' + bu.Code + '-' + ft.code as SignalIdentifiers, o.[CallID] as Comment, 1 as UseForLoadFile, 1 as Status
FROM [Voice_Analytics].[filter].[outputFile] o
INNER JOIN audio.Call c 
	ON o.callId = c.CallID
INNER JOIN audio.CallDirection cd
	ON c.DirectionId = cd.CallDirectionID
INNER JOIN audio.CallFile cf
	ON c.callId = cf.CallID
INNER JOIN audio.[File] f
	ON cf.FileID = f.fileId
	INNER JOIN filter.FilterType ft
		ON o.filterTypeId = ft.filterTypeId
INNER JOIN audio.CallCustodianIdentity cci
	ON c.CallID = cci.CallID
INNER JOIN shared.CustodianIdentity scci
	ON cci.CustodianIdentityID = scci.CustodianIdentityID
INNER JOIN shared.Custodian sc
	ON scci.CustodianID = sc.CustodianID
INNER JOIN shared.Department d
	on sc.DepartmentID = d.DepartmentID
INNER JOIN shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
INNER JOIN shared.CustodianIdentity scci2
	ON sc.CustodianID = scci2.CustodianID AND scci2.IdentityType = 1
INNER JOIN shared.location loc
	ON loc.LocationID = sc.LocationID
WHERE o.Status = 0

