-- All calls that fit benchmarks for all departments (segements)

INSERT [audio].[CallFiltersSatisifed] (CallId, FilterTypeId)

--SELECT c.CallId, c.LocationId, sc.DepartmentID, c.UTCStartTime, c.UTCEndTime, sc.DisplayName
SELECT DISTINCT c.CallId, f.FilterTypeID
FROM [Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN [filter].[Filter] f
	ON  c.UTCStartTime >= f.UTCStartDateTime
	AND c.UTCEndTime < f.UTCStopDateTime
	AND f.FilterTypeID IN (1,2)
	and f.departmentId = sc.DepartmentID
	and f.locationId = c.LocationID
WHERE c.[status] = 0


--c.UTCStartTime >= '20180213' and c.UTCEndTime < '20180214'

INSERT [audio].[CallFiltersSatisifed] (CallId, FilterTypeId)

--SELECT c.CallId, c.LocationId, sc.DepartmentID, c.UTCStartTime, c.UTCEndTime, sc.DisplayName
SELECT DISTINCT c.CallId, f.FilterTypeID
FROM [Voice_Analytics].[audio].[Call] c
INNER JOIN [audio].[CallCustodianIdentity] acci
	ON c.callId = acci.CallID
INNER JOIN [shared].[CustodianIdentity] sci
	ON acci.CustodianIdentityID = sci.CustodianIdentityID
INNER JOIN [shared].[Custodian] sc
	ON sci.CustodianID = sc.custodianId
INNER JOIN [shared].Department d
	ON sc.DepartmentID = d.DepartmentID
INNER JOIN [shared].[BusinessUnit] bu
	ON d.BusinessUnitID = bu.BusinessUnitID
INNER JOIN [filter].[Filter] f
	ON  c.UTCStartTime >= f.UTCStartDateTime
	AND c.UTCEndTime < f.UTCStopDateTime
	AND f.FilterTypeID IN (3,4)
	and f.businessUnitId = d.BusinessUnitID
	and f.locationId = c.LocationID
WHERE c.[status] = 0
