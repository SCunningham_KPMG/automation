/****** Script for SelectTopNRows command from SSMS  ******/
DECLARE @curenntDateTime DATETIME

-- Set status to 1 to say that we are about to copy the data.
UPDATE [dbo].[Staging_Metadata] SET CopyStatus = 1 WHERE CopyStatus = 0

SET @curenntDateTime = getdate()

INSERT INTO [Voice_Data].[dbo].[PreAnalytics_Metadata] (
	  [Location]
      ,[CallID]
      ,[CallParentID]
	  ,[TraderId]
      ,[LocalStartTime]
      ,[LocalStopTime]
      ,[UTCStartTime]
      ,[UTCStopTime]
      ,[Duration]
      ,[InitiatorTRID]
      ,[MediaFileName]
      ,[MediaPath]
      ,[MediaFileId]
      ,[TrunkLabel]
	  ,[DeviceId]
      ,[InitialDeviceTypeID]
      ,[FinalDeviceTypeID]
      ,[CallDirection]
      ,[Station]
      ,[PhoneNumber]
      ,[DialedNumber]
      ,[ParticipantIdentityType]
      ,[ParticipantIdentity]
      ,[CopiedToAnalyticsDateTime]
      ,[Status]
	  ,Firstname
	  ,MiddleName
	  ,LastName,
	  CallingLineIdentifier)
    
  SELECT Location
	,CAST(CallId as BIGINT)
	,CAST(CrossCompleteID as BIGINT)
	,[TraderId]
	,[LocalStartTime]
    ,[LocalStopTime]
    ,[UTCStartTime]
    ,[UTCStopTime]		
	,DATEPART(HOUR, cast (duration as time)) *3600 + DATEPART(MINUTE, cast (duration as time)) *60 + DATEPART(SECOND, cast (duration as time))
	,[CallInitiatorTRID]
	,[MediaFileName]
	,[MediaFilePath]
	,[MediaFileId]
	,Null as TrunkLabel
	,ISNULL([ParticipantDeviceID],[DeviceID]) AS DeviceID
	,ISNULL([ParticipantDeviceTypeID],[DeviceTypeID]) AS InitialDeviceTypeID
	,Null as FinalDeviceTypeId
	,CallDirection
	,ISNULL([Station],[ParticipantStation]) AS Station
	,ISNULL([PhoneNumber],[ParticipantPhoneNumber]) AS PhoneNumber
	,DialedNumber
	,null
	,null
	,getdate()
	,0
	,Firstname
	,MiddleName
	,LastName
	,CallerId

	FROM  [dbo].[Staging_Metadata]
	WHERE CopyStatus = 1

	UPDATE [dbo].[Staging_Metadata] SET CopyStatus = 2, CopiedToPreAnalytics = @curenntDateTime WHERE CopyStatus = 1 



UPDATE [dbo].[Staging_TrunkLabel] SET CopyStatus = 1 WHERE CopyStatus = 0


USE [Voice_Data]
GO

INSERT INTO [dbo].[PreAnalytics_TrunkLabels]
           ([CallId]
           ,[FirstName]
           ,[LastName]
           ,[TraderId]
           ,[TrunkLabel]
           ,[DeviceTypeId]
           ,[Location]
           ,[DialledDigits]
           ,[Station]
)
SELECT DISTINCT CallID, FirstName, LastName,TraderId,TrunkLabel, deviceTypeID, Location, DialedDigits, Station FROM [dbo].[Staging_TrunkLabel] WHERE CopyStatus = 1

DECLARE @currentDateTime2 datetime
SET @currentDateTime2 = getdate()
UPDATE [dbo].[Staging_TrunkLabel] SET CopyStatus = 2, CopiedToPreAnalytics = @currentDateTime2 WHERE CopyStatus = 1 






