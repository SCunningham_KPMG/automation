USE [Voice_Data]
GO

CREATE PROCEDURE [ETL].[AddNewRowsToStaging_TrunkLabels]

AS

BEGIN

	SET NOCOUNT ON

	BEGIN TRY

		BEGIN TRAN

			INSERT INTO 
				[dbo].[Staging_TrunkLabel]
			   (
				   [CallID]
				   ,[Station]
				   ,[TraderId]
				   ,[deviceTypeID]
				   ,[TrunkLabel]
				   ,[FirstName]
				   ,[LastName]
				   ,[Location]
				   ,[DialedDigits]
				)

			SELECT DISTINCT
				[iInteractionID] AS CallId
				,[nvcStation] AS Station
				,[nvcAgentId] AS TraderId
				,[tiDeviceTypeID] AS [deviceTypeID]
				,[nvcTrunkLabel] AS [TrunkLabel]
				,[nvcFirstName] AS [FirstName]
				,[nvcLastName] AS [LastName]
				,'NYC' AS [Location]
				,[DialedDigits] AS [DialedDigits]
			FROM 
				[dbo].[NYC_TrunkLabel]
			WHERE
				CopiedToStaging IS NULL

		UNION

			SELECT DISTINCT
				[iInteractionID] AS CallId
				,[nvcStation] AS Station
				,[nvcAgentId] AS TraderId
				,[tiDeviceTypeID] AS [deviceTypeID]
				,[nvcTrunkLabel] AS [TrunkLabel]
				,[nvcFirstName] AS [FirstName]
				,[nvcLastName] AS [LastName]
				,'LDN' AS [Location]
				,[DialledDigits] AS [DialedDigits]
			FROM 
				[dbo].[LDN_TrunkLabel]
			WHERE
				CopiedToStaging IS NULL

		UNION

			SELECT DISTINCT
				[iInteractionID] AS CallId
				,[nvcStation] AS Station
				,[nvcAgentId] AS TraderId
				,[tiDeviceTypeID] AS [deviceTypeID]
				,[nvcTrunkLabel] AS [TrunkLabel]
				,[nvcFirstName] AS [FirstName]
				,[nvcLastName] AS [LastName]
				,'HKG' AS [Location]
				,[DialedDigits] AS [DialedDigits]
			FROM 
				[dbo].[HKG_TrunkLabel]
			WHERE
				CopiedToStaging IS NULL

		COMMIT

	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK

	END CATCH

END
GO