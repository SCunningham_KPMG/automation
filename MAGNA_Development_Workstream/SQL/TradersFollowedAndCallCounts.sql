DECLARE @theDate as date, @locationId as INT
SET @theDate = '20180411'
SET @locationId = 1

select c.DisplayName CustodianName, bu.Name AssetClass ,d.Name Segment, count(*) CallsMade
from Voice_Analytics.[filter].[CustodianDateFollowed] cdf
inner join Voice_Analytics.shared.Custodian c
	ON cdf.CustodianId = c.CustodianID
inner join Voice_Analytics.shared.Department d
	ON c.DepartmentID = d.DepartmentID
inner join Voice_Analytics.shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
inner join Voice_Analytics.shared.CustodianIdentity sci
	ON sci.CustodianID = c.CustodianID
inner join Voice_Analytics.audio.CallCustodianIdentity aci
	ON sci.CustodianIdentityID = aci.CustodianIdentityID
inner join Voice_Analytics.audio.call ca
	ON aci.CallID = ca.CallID
where c.LocationID = @locationId and cdf.datefollowed = @theDate and cast(ca.localstarttime as date) = @theDate
group by c.DisplayName, bu.Name ,d.Name
order by AssetClass,segment



select c.DisplayName, cdf.DateFollowed, d.name, bu.name 
from [filter].[CustodianDateFollowed] cdf
inner join Voice_Analytics.shared.Custodian c
	ON cdf.CustodianId = c.CustodianID
inner join Voice_Analytics.shared.Department d
	ON c.DepartmentID = d.DepartmentID
inner join Voice_Analytics.shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
where c.LocationID=@locationId
order by bu.name,  datefollowed desc





