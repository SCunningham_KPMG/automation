USE [Voice_Data]
GO

/****** Object:  StoredProcedure [dbo].[SampleLoadFileSource]    Script Date: 16/03/2018 13:22:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SampleLoadFileSource]

AS

BEGIN

	SET NOCOUNT ON

	SELECT DISTINCT
			[CallID]
		  ,[MediaFileName]
		  ,[LocalStartTime]
		  ,[LocalEndTime]
		  ,[DurationInSeconds]
		  ,[DeviceType]
		  ,[TrunkLabel]
		  ,[TraderID]
		  ,[TraderName]
		  ,[CombinedBUandDepartment]
		  ,[CallDirection]
		  ,[DialedNumber]
		  ,[CallLineIdentifier]
		  ,[PhoneNumber]
		  ,[SignalIdentifiers]
		  ,[Comment]
		  ,[Location]
		  ,[FilterTypeId]
		  ,[UseForLoadFile]
		  ,[Status]
		  ,[CreateDateTime]
		  ,b.[FileName]
		  ,b.[Directory]
		  ,b.[Directory_UNC]
		  ,a.[ReleaseFileID]
	  FROM 
		[Voice_Analytics].filter.[OutputFile] a
	  LEFT JOIN
		Voice_Data.dbo.vwFileList b
	  ON
		LTRIM(RTRIM(a.MediaFileName)) = LTRIM(RTRIM(b.FileName))
	WHERE
		a.Status = 1

END


GO


