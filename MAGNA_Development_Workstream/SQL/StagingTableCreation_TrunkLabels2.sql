USE [Voice_Data]
GO

/****** Object:  StoredProcedure [ETL].[AddNewRowsToStaging_TrunkLabels]    Script Date: 08/03/2018 09:00:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [ETL].[AddNewRowsToStaging_TrunkLabels]

AS

BEGIN

	SET NOCOUNT ON

	BEGIN TRY

		BEGIN TRAN

			INSERT INTO 
				[dbo].[Staging_TrunkLabel]
			   (
				   [CallID]
				   ,[Station]
				   ,[TraderId]
				   ,[deviceTypeID]
				   ,[TrunkLabel]
				   ,[FirstName]
				   ,[LastName]
				   ,[Location]
				   ,[DialedDigits]
				)

			SELECT DISTINCT
				[iInteractionID] AS CallId
				,[nvcStation] AS Station
				,[nvcAgentId] AS TraderId
				,[tiDeviceTypeID] AS [deviceTypeID]
				,[nvcTrunkLabel] AS [TrunkLabel]
				,[nvcFirstName] AS [FirstName]
				,[nvcLastName] AS [LastName]
				,'NYC' AS [Location]
				,[DialedDigits] AS [DialedDigits]
			FROM 
				[dbo].[NYC_TrunkLabel]
			WHERE
				CopiedToStaging IS NULL

		UNION

			SELECT DISTINCT
				[iInteractionID] AS CallId
				,[nvcStation] AS Station
				,[nvcAgentId] AS TraderId
				,[tiDeviceTypeID] AS [deviceTypeID]
				,[nvcTrunkLabel] AS [TrunkLabel]
				,[nvcFirstName] AS [FirstName]
				,[nvcLastName] AS [LastName]
				,'LDN' AS [Location]
				,[DialledDigits] AS [DialedDigits]
			FROM 
				[dbo].[LDN_TrunkLabel]
			WHERE
				CopiedToStaging IS NULL

		UNION

			SELECT DISTINCT
				[iInteractionID] AS CallId
				,[nvcStation] AS Station
				,[nvcAgentId] AS TraderId
				,[tiDeviceTypeID] AS [deviceTypeID]
				,[nvcTrunkLabel] AS [TrunkLabel]
				,[nvcFirstName] AS [FirstName]
				,[nvcLastName] AS [LastName]
				,'HKG' AS [Location]
				,[DialedDigits] AS [DialedDigits]
			FROM 
				[dbo].[HKG_TrunkLabel]
			WHERE
				CopiedToStaging IS NULL


		-- =======================================================================
		-- Update the rows we have just inserted so that we do not copy them again
		-- =======================================================================

		UPDATE
			[NYC_TrunkLabel]
		SET
			CopiedToStaging = GETDATE()
		WHERE
			CopiedToStaging IS NULL

		UPDATE
			[LDN_TrunkLabel]
		SET
			CopiedToStaging = GETDATE()
		WHERE
			CopiedToStaging IS NULL

		UPDATE
			[HKG_TrunkLabel]
		SET
			CopiedToStaging = GETDATE()
		WHERE
			CopiedToStaging IS NULL

		COMMIT

		RETURN 0

	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK

		RETURN -1

	END CATCH

END

GO


