USE [Voice_Data]
GO

ALTER PROCEDURE ETL.TidyInitialLoad_TrunkLabels

AS

BEGIN

	-- =====================================================
	-- Author: SJCunningham
	-- =====================================================
	-- Date: 28/02/2018
	-- =====================================================
	-- Purpose:
	--
	--	SP to remove:
	--
	--	Blank lines (summary rows from supplied text files)
	--	Duplicates (from re-supplied files)
	-- =====================================================
	-- Revision history:
	--
	-- v 1.0 - First cut
	-- =====================================================

	SET NOCOUNT ON

-- HKG --------------------------------------------------------------------------------------------------------------------

--SELECT 
--	*
--INTO
--	dbo.HKG_DuplicateTrunksCheck


BEGIN TRY

BEGIN TRAN

DELETE
	a
 FROM
	Voice_Data.dbo.[HKG_TrunkLabel] a
WHERE
	EXISTS
	(
	SELECT 
		*
	FROM 
		(
			SELECT 	
				[iInteractionID]
				, RANK() OVER (PARTITION BY 
				[iInteractionID]
				      ,[tiCallDirectionTypeID]
      ,[nvcStation]
      ,[nvcPhoneNumber]
      ,[nvcAgentId]
      ,[bitIsFirstUser]
      ,[bitIsInteractionInitiator]
      ,[tiDeviceTypeID]
      ,[nvcCTIAgentName]
      ,[vcTrunkGroup]
      ,[vcTrunkNumber]
      ,[nvcTrunkLabel]
      ,[nvcFirstName]
      ,[nvcLastName]
      ,[Initiator]
      ,[dtInteractionGMTStartTime]
      ,[dtInteractionGMTStopTime]
      ,[dtInteractionLocalStartTime]
      ,[dtInteractionLocalStopTime]
      ,[Duration]
      ,[iInteractionOriginalID]
      ,[iInteractionID2]
      ,[iContactID]
      ,[RuleDefaultSystemScore]
      ,[RuleDefaultSystemEmotion]
      ,[RuleDefaultProductivity]
      ,[RuleDefaultQuality]
      ,[dtModifyDate]
      ,[tiLanguageId]
      ,[biOriginalCallId]
      ,[nvcCustomerId]
      ,[nvcCaseId]
      ,[iFCRDiff]
      ,[dtContactGMTStartTime]
      ,[iOriginalSiteID]
      ,[biPrevContactID]
      ,[iPrevSiteID]
      ,[DDILabel]
      ,[CLI]
      ,[DialedDigits]
      ,[nvcExportRule]
      ,[iEtkExported]
      ,[BDI_AccountName]
      ,[BDI_AccountNumber]
      ,[BDI_CallConnectTime]
      ,[BDI_CallEndTime]
      ,[BDI_CallStartTime]
      ,[BDI_DialledNumber]
      ,[BDI_Direction]
      ,[BDI_DiversionNumber]
      ,[BDI_Email]
      ,[BDI_FirstName]
      ,[BDI_LastName]
      ,[BDI_MediaDestinationIP]
      ,[BDI_MediaDestinationPort]
      ,[BDI_NewSequence]
      ,[BDI_RecordedNumber]
      ,[BDI_Sequence]
      ,[BDI_SIPDestinationIP]
      ,[BDI_SIPDestinationPort]
      ,[BDI_Source]
      ,[BDI_Status]
      ,[FromHeaderData]
      ,[BDI_CreationTime]
      ,[BeforeAgentName]
      ,[AfterAgentName]
      ,[BeforeUserID]
      ,[AfterUserID]
      ,[Confidence]
      ,[nvcDialedNumber]
      ,[vcArchivePath]
      ,[vcArchiveUniqueId] ORDER BY NEWID()) AS rn
			FROM 
				Voice_Data.[dbo].[HKG_TrunkLabel] -- WHERE iInteractionID = '6521443883917840150'

		) AS X
	WHERE 
			rn > 1
			AND X.[iInteractionID] = A.[iInteractionID] --AND SegmentID = '6521937942847490831'
		)
				AND
	FileId > (
				SELECT 
					MIN(FileId) 
				FROM 
					Voice_Data.dbo.[HKG_TrunkLabel] b 
				WHERE 
					b.[iInteractionID] = a.[iInteractionID] 
				GROUP BY 
					[iInteractionID]
			)

COMMIT

END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK

END CATCH








-- NYC --------------------------------------------------------------------------------------------------------------------

--SELECT 
--	*
--INTO
--	dbo.NYC_DuplicateTrunksCheck

BEGIN TRY

BEGIN TRAN
DELETE
	a
 FROM
	Voice_Data.dbo.[NYC_TrunkLabel] a
WHERE
	EXISTS
	(
	SELECT 
		*
	FROM 
		(
			SELECT 	
				[iInteractionID]
				, RANK() OVER (
				
				
				PARTITION BY 
      [iInteractionID]
      ,[tiCallDirectionTypeID]
      ,[nvcStation]
      ,[nvcPhoneNumber]
      ,[nvcAgentId]
      ,[bitIsFirstUser]
      ,[bitIsInteractionInitiator]
      ,[tiDeviceTypeID]
      ,[nvcCTIAgentName]
      ,[vcTrunkGroup]
      ,[vcTrunkNumber]
      ,[nvcTrunkLabel]
      ,[nvcFirstName]
      ,[nvcLastName]
      ,[Initiator]
      ,[dtInteractionGMTStartTime]
      ,[dtInteractionGMTStopTime]
      ,[dtInteractionLocalStartTime]
      ,[dtInteractionLocalStopTime]
      ,[Duration]
      ,[iInteractionOriginalID]
      ,[iInteractionID2]
      ,[iContactID]
      ,[RuleDefaultSystemScore]
      ,[RuleDefaultSystemEmotion]
      ,[RuleDefaultProductivity]
      ,[RuleDefaultQuality]
      ,[dtModifyDate]
      ,[tiLanguageId]
      ,[biOriginalCallId]
      ,[nvcCustomerId]
      ,[nvcCaseId]
      ,[iFCRDiff]
      ,[dtContactGMTStartTime]
      ,[iOriginalSiteID]
      ,[biPrevContactID]
      ,[iPrevSiteID]
      ,[CLI]
      ,[DDILabel]
      ,[DialedDigits]
      ,[IsVoiceSupressed]
      ,[ITSAnywhereTurretType]
      ,[iEtkExported]
      ,[nvcExportRule]
      ,[BDI_NewSequence]
      ,[BDI_Status]
      ,[BDI_Direction]
      ,[BDI_CreationTime]
      ,[BDI_AccountName]
      ,[BDI_AccountNumber]
      ,[BDI_DialledNumber]
      ,[BDI_DiversionNumber]
      ,[BDI_CallStartTime]
      ,[BDI_CallConnectTime]
      ,[BDI_CallEndTime]
      ,[BDI_SIPDestinationIP]
      ,[BDI_SIPDestinationPort]
      ,[BDI_MediaDestinationIP]
      ,[BDI_MediaDestinationPort]
      ,[BDI_Source]
      ,[BDI_RecordedNumber]
      ,[BDI_FirstName]
      ,[BDI_LastName]
      ,[BDI_Email]
      ,[FromHeaderData]
      ,[BeforeAgentName]
      ,[AfterAgentName]
      ,[BeforeUserID]
      ,[AfterUserID]
      ,[Confidence]
      ,[nvcDialedNumber]
      ,[vcArchivePath]
      ,[vcArchiveUniqueId]

 ORDER BY NEWID()) AS rn
			FROM 
				Voice_Data.[dbo].[NYC_TrunkLabel] -- WHERE iInteractionID = '6521443883917840150'

		) AS X
	WHERE 
			rn > 1
			AND X.[iInteractionID] = A.[iInteractionID] --AND SegmentID = '6521937942847490831'
		)
				AND
	FileId > (
				SELECT 
					MIN(FileId) 
				FROM 
					Voice_Data.dbo.[NYC_TrunkLabel] b 
				WHERE 
					b.[iInteractionID] = a.[iInteractionID] 
				GROUP BY 
					[iInteractionID]
			)

COMMIT
END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK

END CATCH


-- SELECT * FROM NYC_TrunkLabel WHERE iInteractionID = '6521651872486468898'





-- LDN --------------------------------------------------------------------------------------------------------------------

--SELECT 
--	*
--INTO
--	dbo.LDN_DuplicateTrunksCheck


BEGIN TRY

	BEGIN TRAN

		DELETE
			a
		 FROM
			Voice_Data.dbo.[LDN_TrunkLabel] a
		WHERE
			EXISTS
			(
			SELECT 
				*
			FROM 
				(
					SELECT 	
						[iInteractionID]
						, RANK() OVER (
				
				
						PARTITION BY 

				[iInteractionID]
			  ,[tiCallDirectionTypeID]
			  ,[nvcStation]
			  ,[nvcPhoneNumber]
			  ,[nvcAgentId]
			  ,[bitIsFirstUser]
			  ,[bitIsInteractionInitiator]
			  ,[tiDeviceTypeID]
			  ,[nvcCTIAgentName]
			  ,[vcTrunkGroup]
			  ,[vcTrunkNumber]
			  ,[nvcTrunkLabel]
			  ,[nvcFirstName]
			  ,[nvcLastName]
			  ,[Initiator]
			  ,[dtInteractionGMTStartTime]
			  ,[dtInteractionGMTStopTime]
			  ,[dtInteractionLocalStartTime]
			  ,[dtInteractionLocalStopTime]
			  ,[Duration]
			  ,[iInteractionOriginalID]
			  ,[iInteractionID2]
			  ,[iContactID]
			  ,[RuleDefaultSystemScore]
			  ,[RuleDefaultSystemEmotion]
			  ,[RuleDefaultProductivity]
			  ,[RuleDefaultQuality]
			  ,[dtModifyDate]
			  ,[tiLanguageId]
			  ,[biOriginalCallId]
			  ,[nvcCustomerId]
			  ,[nvcCaseId]
			  ,[iFCRDiff]
			  ,[dtContactGMTStartTime]
			  ,[iOriginalSiteID]
			  ,[biPrevContactID]
			  ,[iPrevSiteID]
			  ,[CallerName]
			  ,[ConnectedName]
			  ,[CalledName]
			  ,[CLI]
			  ,[DialledDigits]
			  ,[iEtkExported]
			  ,[nvcExportRule]
			  ,[BeforeAgentName]
			  ,[AfterAgentName]
			  ,[BeforeUserID]
			  ,[AfterUserID]
			  ,[Confidence]
			  ,[BDI_Sequence]
			  ,[BDI_Status]
			  ,[BDI_Direction]
			  ,[BDI_CreationTime]
			  ,[BDI_AccountName]
			  ,[BDI_AccountNumber]
			  ,[BDI_DialledNumber]
			  ,[BDI_DiversionNumber]
			  ,[BDI_CallStartTime]
			  ,[BDI_CallConnectTime]
			  ,[BDI_CallEndTime]
			  ,[BDI_SIPDestinationIP]
			  ,[BDI_SIPDestinationPort]
			  ,[BDI_MediaDestinationIP]
			  ,[BDI_MediaDestinationPort]
			  ,[BDI_Source]
			  ,[BDI_RecordedNumber]
			  ,[BDI_FirstName]
			  ,[BDI_LastName]
			  ,[BDI_Email]
			  ,[BDI_NewSequence]
			  ,[FromHeaderData]
			  ,[nvcDialedNumber]
			  ,[vcArchivePath]
			  ,[vcArchiveUniqueId]

		 ORDER BY NEWID()) AS rn
					FROM 
						Voice_Data.[dbo].[LDN_TrunkLabel] -- WHERE iInteractionID = '6521443883917840150'

				) AS X
			WHERE 
					rn > 1
					AND X.[iInteractionID] = A.[iInteractionID] --AND SegmentID = '6521937942847490831'
				)
						AND
			FileId > (
						SELECT 
							MIN(FileId) 
						FROM 
							Voice_Data.dbo.[LDN_TrunkLabel] b 
						WHERE 
							b.[iInteractionID] = a.[iInteractionID] 
						GROUP BY 
							[iInteractionID]
					)

		COMMIT
END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK

END CATCH

-- SELECT * FROM LDN_TrunkLabel WHERE iInteractionID = '6521976653387733591'

END