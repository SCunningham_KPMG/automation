USE [Voice_Data]
GO

/****** Object:  StoredProcedure [ETL].[AddNewRowsToStaging_Metadata]    Script Date: 14/03/2018 10:23:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [ETL].[AddNewRowsToStaging_Metadata]

AS

BEGIN

	SET NOCOUNT ON

	BEGIN TRY

		DECLARE @metadataAdded DATETIME = GETDATE()

		BEGIN TRAN

			-- =============================================================================================================================================
			-- Add all newly loaded rows to Staging Metadata
			-- =============================================================================================================================================

			INSERT INTO 
				[dbo].[Staging_Metadata]
				(
					[CallId]
					,[CallDirection]
					,[Station]
					,[ParticipantStation]
					,[PhoneNumber]
					,[ParticipantPhoneNumber]
					,[TraderId]
					,[DeviceID]
					,[ParticipantDeviceID]
					,[DeviceTypeID]
					,[ParticipantDeviceTypeID]
					,[ParticipantCTITraderName]
					,[TrunkGroup]
					,[ParticipantTrunkGroup]
					,[TrunkNumber]
					,[ParticipantTrunkNumber]
					,[FirstName]
					,[MiddleName]
					,[LastName]
					,[FullName]
					,[CallInitiatorTRID]
					,[LocalStartTime]
					,[LocalStopTime]
					,[UTCStartTime]
					,[UTCStopTime]
					,[Duration]
					,[CrossCompleteID]
					,[DialedNumber]
					,[MediaFileId]
					,[MediaFilePath]
					,[MediaFileName]
					,[Status]
					,[RecordingLogger]
					,[SegmentSwitchCallID]
					,[CallerId]
					,[Location]
					,[CreatedDateTime]
					,[FileId]
				)
			SELECT 
				CallId
				,CallDirection
				,[Station]
				,[ParticipantStation]
				,PhoneNumber
				,ParticipantPhoneNumber
				,TraderId
				,[DeviceID]
				,[ParticipantDeviceID]
				,[DeviceTypeID]
				,[ParticipantDeviceTypeID]
				,ParticipantCTITraderName
				,[TrunkGroup]
				,[ParticipantTrunkGroup]
				,[TrunkNumber]
				,[ParticipantTrunkNumber]
				,[FirstName]
				,[MiddleName]
				,[LastName]
				,[FullName]
				,CallInitiatorTRID
				,LocalStartTime
				,LocalStopTime
				,UTCStartTime
				,UTCStopTime
				,Duration
				,[CrossCompleteID]
				,DialedNumber
				,MediaFileId
				,MediaFilePath
				,MediaFileName
				,[Status]	
				,RecordingLogger	
				,SegmentSwitchCallID	
				,CallerId
				,Location
				,CreatedDateTime
				,FileId
			FROM
			(
				SELECT
					[SegmentID] as CallId
					,[SegmentCallDirectionTypeID] as CallDirection
					,[Station]
					,[ParticipantStation]
					,[Phone-Number] as PhoneNumber
					,[ParticipantPhone-Number] as ParticipantPhoneNumber
					,ParticipantAgentID as TraderId
					,[DeviceID]
					,[ParticipantDeviceID]
					,[DeviceTypeID]
					,[ParticipantDeviceTypeID]
					,[ParticipantCTIAgentName] ParticipantCTITraderName
					,[TrunkGroup]
					,[ParticipantTrunkGroup]
					,[TrunkNumber]
					,[ParticipantTrunkNumber]
					,[FirstName]
					,[MiddleName]
					,[LastName]
					,[FullName]
					,[SegmentInitiatorUserID] AS CallInitiatorTRID
					,[InternalSegmentClientStartTime] AS LocalStartTime
					,[InternalSegmentClientStopTime] AS LocalStopTime
					,[SegmentStartTime] AS UTCStartTime
					,[SegmentStopTime] AS UTCStopTime
					,[SegmentDuration] AS Duration
					,[CrossCompleteID]
					,[SegmentDialedNumber] AS DialedNumber
					,[FileId] as MediaFileId
					,[Path] as MediaFilePath
					,[FileName] as MediaFileName
					,[Status]	
					,RecordingLogger	
					,SegmentSwitchCallID	
					,CallerId
					,'HKG' as Location -- 'HKG' , 'LDN', 'NYC'
					,@metadataAdded as CreatedDateTime -- The datetime this record was created in staging
					,[FileId]
				FROM 
					[Voice_Data].[dbo].[HKG_MetaData]
				WHERE 
					CopiedToStaging IS NULL -- Only copy across rows that are not flagged as already copied. These must be new or deliberately set for re-transmission.

		UNION

			SELECT
				[SegmentID] as CallId
				,[SegmentCallDirectionTypeID] as CallDirection
				,[Station]
				,[ParticipantStation]
				,[Phone-Number] as PhoneNumber
				,[ParticipantPhone-Number] as ParticipantPhoneNumber
				,ParticipantAgentID as TraderId
				,[DeviceID]
				,[ParticipantDeviceID]
				,[DeviceTypeID]
				,[ParticipantDeviceTypeID]
				,[ParticipantCTIAgentName] ParticipantCTITraderName
				,[TrunkGroup]
				,[ParticipantTrunkGroup]
				,[TrunkNumber]
				,[ParticipantTrunkNumber]
				,[FirstName]
				,[MiddleName]
				,[LastName]
				,[FullName]
				,[SegmentInitiatorUserID] AS CallInitiatorTRID
				,[InternalSegmentClientStartTime] AS LocalStartTime
				,[InternalSegmentClientStopTime] AS LocalStopTime
				,[SegmentStartTime] AS UTCStartTime
				,[SegmentStopTime] AS UTCStopTime
				,[SegmentDuration] AS Duration
				,[CrossCompleteID]
				,[SegmentDialedNumber] AS DialedNumber
				,[FileId] as MediaFileId
				,[Path] as MediaFilePath
				,[FileName] as MediaFileName
				,[Status]	
				,RecordingLogger	
				,SegmentSwitchCallID	
				,CLIANI AS [CallerID]
				,'LDN' as Location -- 'HKG' , 'LDN', 'NYC'
				,@metadataAdded as CreatedDateTime -- The datetime this record was created in staging
				,[FileId]
			  FROM 
				[Voice_Data].[dbo].[LDN_MetaData]
			  WHERE 
				CopiedToStaging IS NULL -- Only copy across rows that are not flagged as already copied. These must be new or deliberately set for re-transmission.

		UNION
  
			SELECT
				   [SegmentID] as CallId
				  ,[SegmentCallDirectionTypeID] as CallDirection
				  ,[Station]
				  ,[ParticipantStation]
				  ,[Phone-Number] as PhoneNumber
				  ,[ParticipantPhone-Number] as ParticipantPhoneNumber
				  ,ParticipantAgentID as TraderId
				  ,[DeviceID]
				  ,[ParticipantDeviceID]
				  ,[DeviceTypeID]
				  ,[ParticipantDeviceTypeID]
				  ,[ParticipantCTIAgentName] ParticipantCTITraderName
				  ,[TrunkGroup]
				  ,[ParticipantTrunkGroup]
				  ,[TrunkNumber]
				  ,[ParticipantTrunkNumber]
				  ,[FirstName]
				  ,[MiddleName]
				  ,[LastName]
				  ,[FullName]
				  ,[SegmentInitiatorUserID] AS CallInitiatorTRID
				  ,[InternalSegmentClientStartTime] AS LocalStartTime
				  ,[InternalSegmentClientStopTime] AS LocalStopTime
				  ,[SegmentStartTime] AS UTCStartTime
				  ,[SegmentStopTime] AS UTCStopTime
				  ,[SegmentDuration] AS Duration
				  ,[CrossCompleteID]
				  ,[SegmentDialedNumber] AS DialedNumber
				  ,[FileId] as MediaFileId
				  ,[Path] as MediaFilePath
				  ,[FileName] as MediaFileName
				  ,[Status]	
				  ,RecordingLogger	
				  ,SegmentSwitchCallID	
				  ,CLI AS [CallerID]
				  ,'NYC' as Location -- 'HKG' , 'LDN', 'NYC'
				  ,@metadataAdded as CreatedDateTime -- The datetime this record was created in staging
				  ,[FileId]
				FROM 
					[Voice_Data].[dbo].[NYC_MetaData]
				WHERE 
					CopiedToStaging IS NULL -- Only copy across rows that are not flagged as already copied. These must be new or deliberately set for re-transmission.

		) AS Staging

			-- =============================================================================================================================================
			-- Update the CopiedToStaging date flags we've just used to determine the rows to copy to the current date so that they won't be picked up again
			-- =============================================================================================================================================

			UPDATE
				[Voice_Data].[dbo].[HKG_MetaData]
			SET
				CopiedToStaging = @metadataAdded
			WHERE 
				CopiedToStaging IS NULL

			UPDATE
				[Voice_Data].[dbo].[LDN_MetaData]
			SET
				CopiedToStaging = @metadataAdded
			WHERE 
				CopiedToStaging IS NULL

			UPDATE
				[Voice_Data].[dbo].[NYC_MetaData]
			SET
				CopiedToStaging = @metadataAdded
			WHERE 
				CopiedToStaging IS NULL


		-- ==========================================================================
		-- Record each location's addition to Staging in the dbo.DataflowStatus table
		-- ==========================================================================

		INSERT INTO 
			[dbo].[DataflowStatus]
			(
				[FileId]
				,[Filename]
				,[MetadataCopiedToStaging]
				--,[TrunkdataCopiedToStaging]
				,[Location]
				,[NumerOfRows]
				--,[CopiedToPreAnalytics]
				--,[CopiedToAnalytics]
				--,[AnalyticsPostProcessing]
				--,[FiltersApplied]
				--,[SamplingComplete]
				--,[ReadyForIngest]
			)
		SELECT 
			a.FileId
			,b.FileName
			,@metadataAdded AS DateMetadaAdded
			,a.Location
			,COUNT(*) AS RowsAdded
		FROM 
			[dbo].[Staging_Metadata] a
		INNER JOIN
			[dbo].[FileLog] b 
		ON 
			a.FileId = b.FileID
		WHERE
			NOT EXISTS
			(
				SELECT
					*
				FROM
					dbo.DataflowStatus c
				WHERE
					c.FileId = b.FileId
			)
		GROUP BY
			a.FileId
			,b.FileName
			,a.Location


		-- ================
		-- Make the changes
		-- ================
		COMMIT

	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK
	END CATCH

END

GO


