
-- 26 rows loaded to Voice_Data
-- 26 rows loaded to Audio2_Metadata (BAU)
SELECT 
	* 
FROM
	HKG_MetaData vhk
LEFT JOIN
	Audio2_Metadata.ETL.Metadata_New_Format_Staging bau
ON
	vhk.SegmentID = bau.SegmentID
WHERE
	vhk.FileId = 652



-- 254 rows loaded to Voice_Data
-- 254 rows loaded to Audio2_Metadata (BAU)
SELECT 
	* 
FROM
	LDN_MetaData vldn
LEFT JOIN
	Audio2_Metadata.ETL.Metadata_New_Format_Staging bau
ON
	vldn.SegmentID = bau.SegmentID
WHERE
	vldn.FileId = 633

-- 367 rows loaded to Voice_Data
-- 367 rows loaded to Audio2_Metadata (BAU)
SELECT 
	* 
FROM
	NYC_MetaData vnyc
LEFT JOIN
	Audio2_Metadata.ETL.Metadata_New_Format_Staging bau
ON
	vnyc.SegmentID = bau.SegmentID
WHERE
	vnyc.FileId = 640


--SELECT DISTINCT FileId FROM HKG_MetaData
--ORDER BY
--FileId DESC

--SELECT DISTINCT FileId FROM LDN_MetaData
--ORDER BY
--FileId DESC

--SELECT DISTINCT FileId FROM NYC_MetaData
--ORDER BY
--FileId DESC

SELECT DISTINCT FileId FROM
HKG_TrunkLabel
ORDER BY FileID desc

-- 155 rows loaded to Voice_Data
-- 155 rows loaded to Audio2_Metadata (BAU)
SELECT
	* 
FROM 
	HKG_TrunkLabel th
LEFT JOIN
	Audio2_Metadata.ETL.TrunkLabels tl
ON
	th.iInteractionID = tl.SegmentId
WHERE 
	FileId = 626
AND
	th.tiDeviceTypeID != 19

-- 247 rows loaded to Voice_Data
-- 247 rows loaded to Audio2_Metadata (BAU)
SELECT
	* 
FROM 
	LDN_TrunkLabel tl
LEFT JOIN
	Audio2_Metadata.ETL.TrunkLabels tlb
ON
	tl.iInteractionID = tlb.SegmentId
WHERE 
	FileId = 629
AND
	tl.tiDeviceTypeID != 19

SELECT DISTINCT FileId FROM
LDN_TrunkLabel
ORDER BY FileID desc

-- 80 rows loaded to Voice_Data
-- 80 rows loaded to Audio2_Metadata (BAU)
SELECT
	* 
FROM 
	NYC_TrunkLabel tn
LEFT JOIN
	Audio2_Metadata.ETL.TrunkLabels tl
ON
	tn.iInteractionID = tl.SegmentId
WHERE 
	FileId = 638
AND
	tn.tiDeviceTypeID != 19


SELECT DISTINCT FileId FROM
NYC_TrunkLabel
ORDER BY FileID desc

