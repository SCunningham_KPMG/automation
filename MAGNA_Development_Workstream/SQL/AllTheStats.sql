-- Parameters
DECLARE @location VARCHAR(3), @TheDate DATE, @locationId INT
SET @location = 'NYC' -- 'LDN', 'NYC' , 'HKG'
SET @TheDate = '20180328'
SET @locationId = 2 --  1 = LDN, 2 = NYC, 3 = HKG

-- QUERY 1 : Run procesure EXEC Voice_Data.dbo.uspPreAnalytics an then run the following query to see what has been copied where for the
--			 location set above on the date set above.
-- Voice Data Staging Records and Status
SELECT 'Staging Number of Records' As Process, count(*) NumberOfRecords
FROM [Voice_Data].[dbo].[Staging_Metadata]
where location = @location
and cast(localstarttime as date) = @TheDate

SELECT 'Staging By Status' As Process, CopyStatus, Status, count(*) NumberOfRecords
FROM [Voice_Data].[dbo].[Staging_Metadata]
where location = @location
and cast(localstarttime as date) = @TheDate
group by copystatus, status

-- Pre-Analytics
SELECT  'Pre-Analytics Number of Records', count(*) NumberOfRecords, count(AudioFileExists) AudioFileExists
FROM [Voice_Data].[dbo].PreAnalytics_Metadata
where location = @location and cast(LocalStartTime as date) = @TheDate

SELECT  'Pre-Analytics by Status', status, s.name, count(*) NumberOfRecords
FROM [Voice_Data].[dbo].PreAnalytics_Metadata m
left join Voice_data.dbo.DataCallStatus s
	ON m.Status = s.statusId
where location = @location and cast(LocalStartTime as date) = @TheDate
group by status, s.name
order by status

-- confirm the calls have been copied to the Analytics database
select 'Analytics number of records', count(*) NumberOfRecords
from Voice_Analytics.audio.call c
where locationId = @locationId and cast(LocalStartTime as date) = @TheDate

-- group by status
select 'Analytics by Status', s.name Status, count(*) NumberOfRecords
from Voice_Analytics.audio.[call] c
left join Voice_data.dbo.DataCallStatus s
	ON c.Status = s.statusId
where locationId = @locationId and (cast(LocalStartTime as date) = @TheDate) 
group by status, s.name
ORDER BY status

-- by filter type satisfied
select 'Filters Satisfied', cfs.FilterTypeId, ft.Name, count(*) NumberofRecords
FROM Voice_Analytics.audio.call c
INNER JOIN Voice_Analytics.audio.CallFiltersSatisfied cfs
	ON c.callid=cfs.CallID
INNER JOIN Voice_Analytics.filter.FilterType ft
	ON ft.FilterTypeID = cfs.FilterTypeId
where locationId = @locationId and (cast(LocalStartTime as date) = @TheDate or cast(LocalEndTime as date) = @TheDate)
GROUP BY cfs.FilterTypeId, ft.name
ORDER BY 2,3

select 'Actual Filter Used For Ingest', ActualFilterUsedForIngest, fc.Name, count(*) NumberOfCalls, seq as FilterSequence
from Voice_Analytics.audio.call c
inner join Voice_Analytics.[filter].[FilterCategory] fc
	on c.ActualFilterUsedForIngest = fc.FilterCategoryID
where locationId = @locationId and (cast(LocalStartTime as date) = @TheDate or cast(LocalEndTime as date) = @TheDate)
group by ActualFilterUsedForIngest, fc.Name, seq
order by fc.seq


select 'Last Followed', cdf.custodianId CustodianId, c.DisplayName CustodianName, bu.Name AssetClass ,d.Name Segment, MAX(cdf.DateFollowed) LastFollowed
from Voice_Analytics.[filter].[CustodianDateFollowed] cdf
inner join Voice_Analytics.shared.Custodian c
	ON cdf.CustodianId = c.CustodianID
inner join Voice_Analytics.shared.Department d
	ON c.DepartmentID = d.DepartmentID
inner join Voice_Analytics.shared.BusinessUnit bu
	ON d.BusinessUnitID = bu.BusinessUnitID
where c.LocationID = @locationId
group by cdf.custodianId, c.DisplayName, bu.Name ,d.Name
order by 6 desc, bu.Name ,d.Name

-- Bnnchmarks applicable
select 'Benchmarks valid for location and date', bu.name AssetClass, d.name Segment ,  b.name Benchmark, f.UTCStartDateTime, f.UTCStopDateTime
from Voice_Analytics.filter.filter f
inner join Voice_Analytics.shared.department d
	on f.DepartmentId = d.DepartmentID
inner join Voice_Analytics.shared.BusinessUnit bu
	on d.BusinessUnitID = bu.BusinessUnitID
inner join Voice_Analytics.filter.Benchmark b
	ON f.OriginalFilterId = b.BenchmarkID
 where f.UTCStartDateTime >= @TheDate and f.UTCStartDateTime < DATEADD (DD,1, @TheDate)
and f.LocationId=@locationId and f.FilterTypeID in (1,2)
order by 1,2,3,4,5,6

-- Market numbers applicable
-- Bnnchmarks applicable
select 'Market Numbers valid for location and date', bu.name AssetClass , b.name MarketNumbersName, f.UTCStartDateTime, f.UTCStopDateTime
from Voice_Analytics.filter.filter f
inner join Voice_Analytics.shared.BusinessUnit bu
	on f.BusinessUnitId = bu.BusinessUnitID
inner join Voice_Analytics.filter.MarketEventData b
	ON f.OriginalFilterId = b.MarketEventDataId
 where f.UTCStartDateTime >= @TheDate and f.UTCStartDateTime <  DATEADD (DD,1, @TheDate)
and f.LocationId=@locationId and f.FilterTypeID=3
order by 1,2,3,4,5


select 'Records in ingest table' ,count(*) 
from Voice_Analytics.filter.OutputFile where location = @location and cast(LocalStartTime as date) = @TheDate

select fc.FilterCategoryID ,fc.Name, count(*) 
from Voice_Analytics.filter.OutputFile o
inner join Voice_Analytics.filter.FilterCategory fc
	on o.FilterTypeId = fc.FilterCategoryID
where location = @location and cast(LocalStartTime as date) = @TheDate 
group by fc.FilterCategoryID, fc.name


select * from Voice_Analytics.audio.call where status in (80)
