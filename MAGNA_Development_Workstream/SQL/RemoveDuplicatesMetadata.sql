USE Voice_Data
GO

ALTER PROCEDURE ETL.TidyInitialLoad_Metadata

AS

BEGIN

	-- =====================================================
	-- Author: SJCunningham
	-- =====================================================
	-- Date: 28/02/2018
	-- =====================================================
	-- Purpose:
	--
	--	SP to remove:
	--
	--	Blank lines (summary rows from supplied text files)
	--	Duplicates (from re-supplied files)
	-- =====================================================
	-- Revision history:
	--
	-- v 1.0 - First cut
	-- =====================================================

	SET NOCOUNT ON

	/*
	SELECT * FROM Voice_Data.dbo.HKG_MetaData
	WHERE SegmentID IS NULL

	SELECT * FROM Voice_Data.dbo.LDN_MetaData
	WHERE SegmentID IS NULL

	SELECT * FROM Voice_Data.dbo.NYC_MetaData
	WHERE SegmentID IS NULL
	*/

	-- Remove summary rows

	DELETE FROM Voice_Data.dbo.HKG_MetaData
	WHERE SegmentID IS NULL

	DELETE FROM Voice_Data.dbo.LDN_MetaData
	WHERE SegmentID IS NULL

	DELETE FROM Voice_Data.dbo.NYC_MetaData
	WHERE SegmentID IS NULL

	--DECLARE @rowsFound BIGINT

	--		SELECT 
	--			@rowsFound = COUNT(*)
	--		FROM 
	--			(
	--				SELECT 	
	--					[SegmentID]
	--					, RANK() OVER (PARTITION BY [SegmentID]
	--					,[SegmentCallDirectionTypeID]
	--					,ISNULL([Station],[ParticipantStation]) 
	--					,ISNULL([Phone-Number],[ParticipantPhone-Number]) 
	--					,[ParticipantAgentID]
	--					,ISNULL([DeviceID],[ParticipantDeviceID]) 
	--					,ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) 
	--					,[ParticipantCTIAgentName]
	--					,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) 
	--					,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) 
	--					,[FirstName]
	--					,[LastName]
	--					,[SegmentInitiatorUserID]
	--					,[InternalSegmentClientStartTime]
	--					,[InternalSegmentClientStopTime]
	--					,[SegmentStartTime]
	--					,[SegmentStopTime]
	--					,[SegmentDuration]
	--					,[CrossCompleteID]
	--					,[SegmentDialedNumber]
	--					,[FileName] ORDER BY NEWID()) AS rn
	--				FROM 
	--					Voice_Data.dbo.HKG_MetaData
	--			) AS X
	--		WHERE 
	--			rn > 1

	--SELECT @rowsFound

	---------------------------- HKG ----------------------------

	BEGIN TRY

		BEGIN TRAN
		DELETE
			a
		 FROM
			Voice_Data.dbo.HKG_MetaData a
		WHERE
			EXISTS
			(
			SELECT 
				*
			FROM 
				(
				SELECT 	
						[SegmentID]
						, RANK() OVER (PARTITION BY [SegmentID]
						,[SegmentCallDirectionTypeID]
						,ISNULL([Station],[ParticipantStation]) 
						,ISNULL([Phone-Number],[ParticipantPhone-Number]) 
						,[ParticipantAgentID]
						,ISNULL([DeviceID],[ParticipantDeviceID]) 
						,ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) 
						,[ParticipantCTIAgentName]
						,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) 
						,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) 
						,[FirstName]
						,[LastName]
						,[SegmentInitiatorUserID]
						,[InternalSegmentClientStartTime]
						,[InternalSegmentClientStopTime]
						,[SegmentStartTime]
						,[SegmentStopTime]
						,[SegmentDuration]
						,[CrossCompleteID]
						,[SegmentDialedNumber]
						,[FileName] ORDER BY NEWID()) AS rn
					FROM 
						Voice_Data.dbo.HKG_MetaData
				) AS X
				WHERE 
					rn > 1
				AND 
					X.SegmentID = A.SegmentID --AND SegmentID = '6521937942847490831'
				)
		AND
			FileId > (
						SELECT 
							MIN(FileId) 
						FROM 
							Voice_Data.dbo.HKG_MetaData b 
						WHERE 
							b.SegmentID = a.SegmentID 
						GROUP BY 
							SegmentID
					)

		COMMIT

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT != 0
			ROLLBACK
	END CATCH

	---------------------------- LDN ----------------------------

	BEGIN TRY

		BEGIN TRAN
		DELETE
			a
		 FROM
			Voice_Data.dbo.LDN_MetaData a
		WHERE
			EXISTS
			(
			SELECT 
				*
			FROM 
				(
					SELECT 	
						[SegmentID]
						, RANK() OVER (PARTITION BY [SegmentID]
						,[SegmentCallDirectionTypeID]
						,ISNULL([Station],[ParticipantStation]) 
						,ISNULL([Phone-Number],[ParticipantPhone-Number]) 
						,[ParticipantAgentID]
						,ISNULL([DeviceID],[ParticipantDeviceID]) 
						,ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) 
						,[ParticipantCTIAgentName]
						,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) 
						,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) 
						,[FirstName]
						,[LastName]
						,[SegmentInitiatorUserID]
						,[InternalSegmentClientStartTime]
						,[InternalSegmentClientStopTime]
						,[SegmentStartTime]
						,[SegmentStopTime]
						,[SegmentDuration]
						,[CrossCompleteID]
						,[SegmentDialedNumber]
						,[FileName] ORDER BY NEWID()) AS rn
					FROM 
						Voice_Data.dbo.LDN_MetaData
				) AS X
			WHERE 
					rn > 1
					AND X.SegmentID = A.SegmentID --AND SegmentID = '6521937942847490831'
				)
		AND
			FileId > (
						SELECT 
							MIN(FileId) 
						FROM 
							Voice_Data.dbo.LDN_MetaData b 
						WHERE 
							b.SegmentID = a.SegmentID 
						GROUP BY 
							SegmentID
					)

		COMMIT

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT != 0
			ROLLBACK
	END CATCH

	---------------------------- NY ----------------------------

	BEGIN TRY

		BEGIN TRAN

			DELETE
				a
			 FROM
				Voice_Data.dbo.NYC_MetaData a
			WHERE
				EXISTS
				(
				SELECT 
					*
				FROM 
					(
						SELECT 	
							[SegmentID]
							, RANK() OVER (PARTITION BY [SegmentID]
							,[SegmentCallDirectionTypeID]
							,ISNULL([Station],[ParticipantStation]) 
							,ISNULL([Phone-Number],[ParticipantPhone-Number]) 
							,[ParticipantAgentID]
							,ISNULL([DeviceID],[ParticipantDeviceID]) 
							,ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) 
							,[ParticipantCTIAgentName]
							,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) 
							,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) 
							,[FirstName]
							,[LastName]
							,[SegmentInitiatorUserID]
							,[InternalSegmentClientStartTime]
							,[InternalSegmentClientStopTime]
							,[SegmentStartTime]
							,[SegmentStopTime]
							,[SegmentDuration]
							,[CrossCompleteID]
							,[SegmentDialedNumber]
							,[FileName] ORDER BY NEWID()) AS rn
						FROM 
							Voice_Data.dbo.NYC_MetaData
					) AS X
				WHERE 
						rn > 1
						AND X.SegmentID = A.SegmentID --AND SegmentID = '6521937942847490831'
					)
			AND
				FileId > (
							SELECT 
								MIN(FileId) 
							FROM 
								Voice_Data.dbo.NYC_MetaData b 
							WHERE 
								b.SegmentID = a.SegmentID 
							GROUP BY 
								SegmentID
						)

		COMMIT

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT != 0
			ROLLBACK
	END CATCH


	--SELECT @rowsFound


	--	SELECT 
	--		@rowsFound = COUNT(*)
	--	FROM 
	--		(
	--			SELECT 	
	--				[SegmentID]
	--				, RANK() OVER (PARTITION BY [SegmentID]
	--				,[SegmentCallDirectionTypeID]
	--				,ISNULL([Station],[ParticipantStation]) 
	--				,ISNULL([Phone-Number],[ParticipantPhone-Number]) 
	--				,[ParticipantAgentID]
	--				,ISNULL([DeviceID],[ParticipantDeviceID]) 
	--				,ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) 
	--				,[ParticipantCTIAgentName]
	--				,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) 
	--				,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) 
	--				,[FirstName]
	--				,[LastName]
	--				,[SegmentInitiatorUserID]
	--				,[InternalSegmentClientStartTime]
	--				,[InternalSegmentClientStopTime]
	--				,[SegmentStartTime]
	--				,[SegmentStopTime]
	--				,[SegmentDuration]
	--				,[CrossCompleteID]
	--				,[SegmentDialedNumber]
	--				,[FileName] ORDER BY NEWID()) AS rn
	--			FROM 
	--				Voice_Data.dbo.NYC_MetaData
	--		) AS X
	--	WHERE 
	--			rn > 1

	--SELECT @rowsFound



END