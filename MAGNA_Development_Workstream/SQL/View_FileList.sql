USE [Voice_Data]
GO

/****** Object:  View [dbo].[vwFileList]    Script Date: 16/03/2018 13:47:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwFileList]

AS

	SELECT 
		[RecordId]
		,[FileId]
		,[Directory]
		,LTRIM(RTRIM([FileName])) AS [FileName]
		,[FileSize]
		,REPLACE([Directory], 'F:\Mount_Root\', '\\VFSRNEX01P001\') AS [Directory_UNC]
	FROM 
		[dbo].[HKG_FileList]

	UNION

		SELECT 
			[RecordId]
			,[FileId]
			,[Directory]
			,[FileName]
			,[FileSize]
			,REPLACE([Directory], 'F:\Mount_Root\', '\\VFSRNEX01P001\') AS [Directory_UNC]
		FROM 
			[dbo].[LDN_FileList]

		UNION

		SELECT 
			[RecordId]
			,[FileId]
			,[Directory]
			,[FileName]
			,[FileSize]
			,REPLACE([Directory], 'F:\Mount_Root\', '\\VFSRNEX01P001\') AS [Directory_UNC]
		FROM 
			[dbo].[NYC_FileList]

GO


