USE [Voice_Data]
GO


SELECT *, 'TRUNCATE TABLE ' + ss.name + '.' + so.name
 FROM sys.objects so INNER JOIN sys.schemas ss
 ON so.schema_id = ss.schema_id
WHERE type = 'U'
ORDER BY so.Name

/*

TRUNCATE TABLE dbo.EventLog -- 0
TRUNCATE TABLE SSIS.EventLog -- 0 
TRUNCATE TABLE ETL.FileDelivery -- 0
TRUNCATE TABLE SSIS.FileDetailLog -- 0
TRUNCATE TABLE dbo.FileLog -- 0
TRUNCATE TABLE ETL.FileProcessing -- 0

TRUNCATE TABLE dbo.HKG_FileList
TRUNCATE TABLE dbo.HKG_Manifest
TRUNCATE TABLE dbo.HKG_MetaData
TRUNCATE TABLE dbo.HKG_TrunkLabel

TRUNCATE TABLE dbo.LDN_FileList
TRUNCATE TABLE dbo.LDN_Manifest
TRUNCATE TABLE dbo.LDN_MetaData
TRUNCATE TABLE dbo.LDN_TrunkLabel

TRUNCATE TABLE dbo.NYC_FileList
TRUNCATE TABLE dbo.NYC_Manifest
TRUNCATE TABLE dbo.NYC_MetaData
TRUNCATE TABLE dbo.NYC_TrunkLabel

*/