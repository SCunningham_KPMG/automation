USE [Voice_Data]
GO

/****** Object:  StoredProcedure [dbo].[getNextReleaseId]    Script Date: 16/03/2018 11:15:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[getNextReleaseId]
	@releaseId VARCHAR(20) OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @releaseValue INT

	SELECT @releaseValue = NEXT VALUE FOR [dbo].[ReleaseId]

	SELECT @releaseId = 'N' + RIGHT('0000' + LTRIM(RTRIM(CAST(@releaseValue AS VARCHAR(10)))), 4)

	RETURN 0

END

GO


