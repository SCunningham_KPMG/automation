USE [Voice_Data]
GO

CREATE PROCEDURE ETL.UpdateTrunkLabelDecisionGate

AS

	BEGIN
		-- =====================================================
		-- Author: SJCunningham
		-- =====================================================
		-- Date: 14/03/2018
		-- =====================================================
		-- Purpose:
		--
		--	SP to check if sufficent trunk label data has been
		--	applied to metadata rows. If so, update the data
		-- flow table gate.
		-- =====================================================
		-- Revision history:
		--
		-- v 1.0 - First cut
		-- =====================================================

		SET NOCOUNT ON

		DECLARE @percTol DECIMAL(8,2)
	
		BEGIN TRY

			SET @percTol = (SELECT ParameterValue FROM [dbo].[DataflowParameters] WHERE ParameterName = 'TrunkLabelMatchVariance')

			IF @@ROWCOUNT = 0
				BEGIN
					SELECT 'Parameter not found'
					-- RETURN -1
				END

		END TRY
		BEGIN CATCH

		END CATCH

		BEGIN TRY
			--====================
			-- Define our datasets
			--====================
			;
			WITH cteTotals (FileId, NumberOfMetadataCalls, NumberOfTrunkLabelCalls)
			AS
			(		
				SELECT 
					a.FileId
					,COUNT(DISTINCT b.CallId) AS NumberOfMetadataCalls
					,COUNT(DISTINCT c.CallID) AS NumberOfTrunkLabelCalls
				FROM
					dbo.DataflowStatus a
				INNER JOIN
					[dbo].[Staging_Metadata] b
				ON
					a.FileId = b.FileId
				LEFT JOIN
					[dbo].[Staging_TrunkLabel] c
				ON
					b.CallId = c.CallID
				WHERE
					a.TrunkdataCopiedToStaging IS NULL
				GROUP BY
					a.FileId
			)
			,cteChecks (FileId, NumberOfMetadataCalls, NumberOfTrunkLabelCalls, Result)
			AS
			(
			SELECT
				FileId
				,NumberOfMetadataCalls
				,NumberOfTrunkLabelCalls
				,CASE 
					WHEN 
						NumberOfMetadataCalls > 0.00 -- Avoid div by zero
					THEN
						(NumberOfTrunkLabelCalls * 1.00) / (NumberOfMetadataCalls * 1.00) * 100.00 
					ELSE
						0 -- 'No trunks'
					END
						AS Result
				FROM
					cteTotals
			)
			,cteDecision (FileId, NumberOfMetadataCalls, NumberOfTrunkLabelCalls, Result, Decision, DateCopied)
			AS
			(
				SELECT
					FileId
					,NumberOfMetadataCalls
					,NumberOfTrunkLabelCalls
					,Result
					,CASE 
						WHEN 100.00 - Result < @percTol
						THEN 'Pass'
						ELSE 'Fail'
					END AS Decision
					,CASE 
						WHEN 100.00 - Result < @percTol
						THEN GETDATE()
						ELSE NULL
					END AS DateToStamp
				FROM
					cteChecks
			)

			--================================================================================
			-- For all rows that have a pass decision, update the TrunkLabel copied date field
			--================================================================================
			UPDATE
				b
			SET
				b.TrunkdataCopiedToStaging = a.DateCopied
			FROM 
				cteDecision a
			INNER JOIN
				dbo.DataflowStatus b
			ON
				a.FileId = b.FileId
			WHERE
				a.Decision = 'Pass'
			AND
				b.TrunkdataCopiedToStaging IS NULL

		END TRY
		BEGIN CATCH
		END CATCH

END