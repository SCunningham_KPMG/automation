USE Voice_Data
GO



/****** Object:  StoredProcedure [dbo].[postLoadDataClean]    Script Date: 21/02/2018 14:55:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[postLoadDataClean]
	@lastLoadBatchId	INT
	,@location			VARCHAR(10)
AS

BEGIN

	SET NOCOUNT ON
	SET DATEFORMAT DMY

	-- Step 0 --

	DECLARE @rowsFound INT = 0;
	DECLARE @loadBatchId INT = 0;
	DECLARE @audioVoiceMasterId INT = 0;

	BEGIN TRY

		------------------------------------------
		-- Step 1 : Checking tables
		------------------------------------------
 
		--check for duplicates IN BASE Table
		SELECT 
			@rowsFound = COUNT(*)
		FROM 
			(
				SELECT 	
					[SegmentID]
					, RANK() OVER (PARTITION BY [SegmentID]
					,[SegmentCallDirectionTypeID]
					,ISNULL([Station],[ParticipantStation]) 
					,ISNULL([Phone-Number],[ParticipantPhone-Number]) 
					,[ParticipantAgentID]
					,ISNULL([DeviceID],[ParticipantDeviceID]) 
					,ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) 
					,[ParticipantCTIAgentName]
					,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) 
					,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) 
					,[FirstName]
					,[LastName]
					,[SegmentInitiatorUserID]
					,[InternalSegmentClientStartTime]
					,[InternalSegmentClientStopTime]
					,[SegmentStartTime]
					,[SegmentStopTime]
					,[SegmentDurationInSeconds]
					,[CrossCompleteID]
					,[SegmentDialedNumber]
					,[FileName] ORDER BY NEWID()) AS rn
				FROM 
					Voice_Data.dbo.HKG_MetaData
			) AS X
		WHERE 
			rn > 1

		--NONE


		IF @rowsFound != 0
		BEGIN

			-- Write to the log and return error to package.

			INSERT INTO [ETL].[LoadController]
				(
					[DateTime]
					,[LoadBatchId]
					,[LoadStage]
					,[LoadProcess]
					,[Message]
					,[State]
					,[AudioVoiceMasterId]
				)
			VALUES
			   (
				   GETDATE()
				   ,@loadBatchId
				   ,7
				   ,'SQL - Post Load Clean: Duplicates detected'
				   ,CAST(@rowsFound AS VARCHAR(20)) + ' duplicate(s) found during processing.'
				   ,-1
				   ,@audioVoiceMasterId
			   )

			RETURN -2

		END


		----------------------------------------------------------
		-- STAGING table
		-- Extract AgentID FROM SBUS AND MOB [FirstName]
		-- Add CustodianDisplayName
		-- 
		---------------------------------------------------------

		-- NEW
		-- Instead of drop and recreate, truncate
		--IF OBJECT_ID('wip.Metadata_New', 'U') IS NOT NULL
		--	DROP TABLE wip.Metadata_New; 

		TRUNCATE TABLE wip.Metadata_New;

		INSERT INTO 
			[wip].[Metadata_New]
			(
				[SegmentID]
				,[CallDirection]
				,[CallDirectionID]
				,[Station]
				,[PhoneNumber]
				,[AgentID]
				,[DeviceID]
				,[DeviceTypeID]
				,[NewDeviceTypeID]
				,[DeviceType]
				,[ParticipantCTIAgentName]
				,[TrunkGroup]
				,[TrunkNumber]
				,[TrunkLabel]
				,[FirstName]
				,[LastName]
				,[InitiatorTRID]
				,[InteractionLocalStartTime]
				,[InteractionLocalStopTime]
				,[InteractionGMTStartTime]
				,[InteractionGMTStopTime]
				,[SegmentDurationInSeconds]
				,[CrossCompleteID]
				,[DialedNumber]
				,[FileName]
				,[CustodianDisplayName]
				,[KPMGSegment]
				,[LoadId]
				,[Path]
				,[Status]
				,[CLIANI]
				,[FullName]
				,[MiddleName]
				,[nvcExportRule]
				,[SegmentDuration]
				,[RecordingChannel]
				,[RecordingLogger]
				,[RecordingMediaTypeID]
				,[SegmentSwitchCallID]
				,[Location]
				,[UniqueRowId]
			)
		SELECT 
			[SegmentID]
			,[SegmentCallDirectionTypeID] AS CallDirection
			, null AS CallDirectionID
			,ISNULL([Station],[ParticipantStation]) AS Station
			,ISNULL([Phone-Number],[ParticipantPhone-Number]) AS PhoneNumber
			,[ParticipantAgentID] AS AgentID
			,ISNULL([DeviceID],[ParticipantDeviceID]) AS DeviceID
			,ISNULL([ParticipantDeviceTypeID],[DeviceTypeID]) AS DeviceTypeID
			,CASE 
				WHEN LastName = 'SBUS' THEN 0
					WHEN (LastName = 'MOB' or deviceid=30) 
							OR (Location='NY' and
									( Middlename like '%MOB%' or deviceid=30)
									)
							THEN 1
				ELSE CAST('3' + CAST(ISNULL([DeviceTypeID],[ParticipantDeviceTypeID]) AS CHAR(3)) AS INT) 
			END AS NewDeviceTypeID
			, '' AS DeviceType
			,[ParticipantCTIAgentName]
			,ISNULL([TrunkGroup],[ParticipantTrunkGroup]) AS TrunkGroup
			,ISNULL([TrunkNumber] ,[ParticipantTrunkNumber]) AS TrunkNumber
			,[TrunkLabel]
			,[FirstName] AS FirstName
			,[LastName]
			,[SegmentInitiatorUserID] AS InitiatorTRID
			,[InternalSegmentClientStartTime] AS InteractionLocalStartTime
			,[InternalSegmentClientStopTime] AS InteractionLocalStopTime
			,[SegmentStartTime] AS InteractionGMTStartTime
			,[SegmentStopTime] AS InteractionGMTStopTime
			,[SegmentDurationInSeconds]
			,[CrossCompleteID]
			,[SegmentDialedNumber] AS DialedNumber
			, REPLACE([FileName],' ','') AS [FileName]
			, '' AS CustodianDisplayName
			, '' AS KPMGSegment
			,LoadId	
			, [Path]
			, [Status]	
			, CLIANI	
			, FullName	
			, MiddleName	
			, nvcExportRule	
			, SegmentDuration	
			, RecordingChannel	
			, RecordingLogger	
			, RecordingMediaTypeID	
			, SegmentSwitchCallID	
			, Location
			, UniqueRowId
		FROM  
			[Audio2_Metadata].[dbo].[Metadata_Staging_Master]
		WHERE 
			-- LoadId > @lastLoadBatchId -- Only for batches beyond the last successful import
			LoadId IN -- Only for batches that are currently set to outstanding (0) for the c
			(
				SELECT 
					LoadBatchId 
				FROM 
					[ETL].[LoadBatchStatus] lbs 
				WHERE 
					lbs.Loaded = 0 
				AND 
					lbs.Location = @location
			)
		AND
			Location = @location

		 --------------SBUS Trader ID---------------

		UPDATE  
			wip.Metadata_New
		SET 
			AgentID = RIGHT(RTRIM(FirstName), 4) 
			,Station = RIGHT(RTRIM(Station), 4)
		WHERE 
			LastName = 'SBUS'
		 --(11631 row(s) affected)

		 --------------MOB Trader ID---------------------
		 -- !!!!!CHANGED FOR NY - RS - 20160602!!!!!
--LDN
		UPDATE  wip.Metadata_New
		SET 	FirstName = RTRIM(FirstName)


		UPDATE  	wip.Metadata_New
		SET 
			AgentID = ISNULL(TI.TraderID,'')
			,CustodianDisplayName = ISNULL(S.CustodianDisplayName,'')
			,KPMGSegment = ISNULL(S.[KPMG Combined Segment],'')
		--SELECT *
		FROM 
			wip.Metadata_New AS X
		LEFT JOIN dbo.CustodianMobileIdentity AS C ON X.FirstName = C.[MobileNo]
					AND  
						X.InteractionLocalStartTime >= C.StartDate
					AND 
						(X.InteractionLocalStartTime < C.EndDate OR C.EndDate IS NULL)

					AND C.Location='LDN'	--new addition

		LEFT JOIN 	dbo.Custodians_staging AS S ON 	C.CustodianID=S.CustodianID
		LEFT JOIN 	dbo.CustodianTraderIdentity AS TI 	ON 	C.CustodianID = TI.CustodianID
				AND  
					X.InteractionLocalStartTime >= TI.StartDate
				AND 
					(X.InteractionLocalStartTime < TI.EndDate OR TI.EndDate IS NULL)
				and left(X.Location,2)=left(TI.location,2)
		WHERE 
			(X.LastName = 'MOB' or DeviceID=30)
		AND X.Location='LDN'
		AND C.MobileNo IS NOT NULL

 --NY

  UPdate   wip.Metadata_New
set AgentID=isnull(TI.TraderID,''),
 CustodianDisplayName=isnull(S.CustodianDisplayName,'')
 , KPMGSegment=isnull(S.[KPMG Combined Segment],'')

 --select *
 from  wip.Metadata_New as X
left join dbo.CustodianMobileIdentity as C on X.FirstName=C.[MobileNo]
					and  X.InteractionLocalStartTime>=C.StartDate
					and (X.InteractionLocalStartTime<C.EndDate or C.EndDate is null)
										AND C.Location='NYC'	--new addition
left join dbo.Custodians_staging as S on C.CustodianID=S.CustodianID
left join dbo.CustodianTraderIdentity as TI on C.CustodianID=TI.CustodianID
					and  X.InteractionLocalStartTime>=TI.StartDate
					and (X.InteractionLocalStartTime<TI.EndDate or TI.EndDate is null)
					and left(X.Location,2)=left(TI.location,2)

 where (X.MiddleName like '%MOB%' or DeviceID=30)  --column different from LDN data - yet to check raw metadata
		and X.Location='NY'
 		AND C.MobileNo IS NOT NULL



-- HKG

		UPDATE  	wip.Metadata_New
		SET 
			AgentID = ISNULL(TI.TraderID,'')
			,CustodianDisplayName = ISNULL(S.CustodianDisplayName,'')
			,KPMGSegment = ISNULL(S.[KPMG Combined Segment],'')
		--SELECT *
		FROM 
			wip.Metadata_New AS X
		LEFT JOIN dbo.CustodianMobileIdentity AS C ON X.Station = C.[MobileNo]
					AND  
						X.InteractionLocalStartTime >= C.StartDate
					AND 
						(X.InteractionLocalStartTime < C.EndDate OR C.EndDate IS NULL)

					AND C.Location='HKG'	--new addition

		LEFT JOIN 	dbo.Custodians_staging AS S ON 	C.CustodianID=S.CustodianID
		LEFT JOIN 	dbo.CustodianTraderIdentity AS TI 	ON 	C.CustodianID = TI.CustodianID
				AND  
					X.InteractionLocalStartTime >= TI.StartDate
				AND 
					(X.InteractionLocalStartTime < TI.EndDate OR TI.EndDate IS NULL)
				and left(X.Location,2)=left(TI.location,2)
		WHERE 
			DeviceID=30
		AND X.Location='HKG'
		AND C.MobileNo IS NOT NULL

------------------------------------------------------------------------





--Cisco updates
--added on 20160609 by RS

 
  UPdate  wip.Metadata_New
set AgentID=isnull(TI.TraderID,''),
 CustodianDisplayName=isnull(S.CustodianDisplayName,'')
 , KPMGSegment=isnull(S.[KPMG Combined Segment],'')

 --select *
 from wip.Metadata_New as X
left join dbo.CustodianCiscoIdentity as C on X.FirstName=C.[CiscoNo]
					and  X.InteractionLocalStartTime>=C.StartDate
					and (X.InteractionLocalStartTime<C.EndDate or C.EndDate is null)

					and left(X.Location,2)=left(C.location,2)

left join dbo.Custodians_staging as S on C.CustodianID=S.CustodianID
left join dbo.CustodianTraderIdentity as TI on C.CustodianID=TI.CustodianID
					and  X.InteractionLocalStartTime>=TI.StartDate
					and (X.InteractionLocalStartTime<TI.EndDate or TI.EndDate is null)
					and left(X.Location,2)=left(TI.location,2)
 where X.LastName='EXT'
 and X.Location='LDN'
 and C.CiscoNo is not null


 
  UPdate  wip.Metadata_New
set AgentID=isnull(TI.TraderID,''),
 CustodianDisplayName=isnull(S.CustodianDisplayName,'')
 , KPMGSegment=isnull(S.[KPMG Combined Segment],'')

 --select *
 from wip.Metadata_New as X
left join dbo.CustodianCiscoIdentity as C on X.Station=C.[CiscoNo]
					and  X.InteractionLocalStartTime>=C.StartDate
					and (X.InteractionLocalStartTime<C.EndDate or C.EndDate is null)

					and left(X.Location,2)=left(C.location,2)

left join dbo.Custodians_staging as S on C.CustodianID=S.CustodianID
left join dbo.CustodianTraderIdentity as TI on C.CustodianID=TI.CustodianID
					and  X.InteractionLocalStartTime>=TI.StartDate
					and (X.InteractionLocalStartTime<TI.EndDate or TI.EndDate is null)
					and left(X.Location,2)=left(TI.location,2)
 where X.Location='NY'
 and InteractionLocalStartTime>'20170116'
 and C.CiscoNo is not null
 and KPMGSegment=''
		 
		 ----------------Custodian DisplayName AND Segment for TRID------------
	
		UPDATE 
			wip.Metadata_New
		SET 
			CustodianDisplayName = ISNULL(C.CustodianDisplayName, '')
			,KPMGSegment = ISNULL(C.[KPMG Combined Segment], '')
		FROM 
			wip.Metadata_New AS M
		LEFT JOIN 	dbo.CustodianTraderIdentity AS TI ON 	M.agentID = TI.TraderID 
				AND 
					M.InteractionLocalStartTime >= TI.StartDate
				AND 
					(M.InteractionLocalStartTime < TI.EndDate OR TI.EndDate IS NULL)

				and left(M.Location,2)=left(TI.location,2)

		LEFT JOIN 	dbo.Custodians_Staging AS C ON 	TI.CustodianId = C.CustodianID
		WHERE   (M.Location='LDN' 
				and (M.LastName not in ('EXT','MOB') )
				and KPMGSEgment='' 
				 )
			OR 
				(M.Location='NY' 
				and M.MiddleName not like '%MOB%'
				and KPMGSEgment='' 
				 )
--=============  Adding HKG ===============
			OR
				(M.Location='HKG'
				and KPMGSegment=''
				AND DeviceID != '30')

		 --(282091 row(s) affected)

		 --------------Call direction ID----------------------
		UPDATE 
			wip.Metadata_New
		SET 
			CallDirectionID = isnull(D.callDirectionID,'')
		FROM 
			wip.Metadata_New AS M
		LEFT JOIN 
			dbo.CallDirection AS D 
		ON 
			M.CallDirection = D.CallDirectionDescription
		 --(282091 row(s) affected)

		 ----------------DeviceType----------------------
		UPDATE 
			wip.Metadata_New
		SET 
			DeviceType = ISNULL(C.DeviceTypeDescription, '')
		FROM 
			wip.Metadata_New AS M
		LEFT JOIN 
			dbo.DeviceType AS C 
		ON 
			M.NewDeviceTypeID = C.DeviceTypeId
		 --(282091 row(s) affected)


		 -------- DISKFILENAME - missing files ----------------
		UPDATE 
			wip.Metadata_New
		SET 
			[Filename] = NULL
		WHERE 
			[FileName] LIKE '%---%'
		 --(4808 row(s) affected)

		 -------------------------------------------------------------------------
		-- UPDATE DialedNumbers BY removing dashes  ?? --
		-------------------------------------------------------------------------
		UPDATE  
			wip.Metadata_New
		SET 
			DialedNumber = 
							CASE 
								WHEN CHARINDEX('-',DialedNumber) >= 1 
								THEN LEFT (DialedNumber, CHARINDEX('-',DialedNumber)-1)
								ELSE DialedNumber 
							END 

		UPDATE  
			wip.Metadata_New
		SET 
			DialedNumber = LTRIM(RTRIM(DialedNumber)) 
		--(282091 row(s) affected)

		  -------------------------------------------------------------------------
		-- UPDATE trunk labels AND clean the rest
		-------------------------------------------------------------------------

		UPDATE ETL.TrunkLabels SET TraderId = '' WHERE TraderId = 'User'

		UPDATE 
			wip.Metadata_New
		SET 
			TrunkLabel = LTRIM(RTRIM(b.trunklabel))
		FROM 
			wip.Metadata_New AS E
		LEFT JOIN 
			(
				SELECT DISTINCT 
					SegmentID
					,TraderID
					,LTRIM(RTRIM(TrunkLabel)) AS trunklabel
				FROM 
					ETL.TrunkLabels
				WHERE 
					TrunkLabel <> ''
			) AS b 
		ON     
			e.SegmentID = b.SegmentID 
		AND 
			e.AgentID = b.TraderId
		WHERE 
			b.SegmentID IS NOT NULL

		UPDATE  
			wip.Metadata_New
		SET 
			TrunkLabel = '' 
		WHERE 
			TrunkLabel IS NULL
		--(81110 row(s) affected)




		INSERT INTO 
			[ETL].[Metadata_Final_With_Media_Filename]
			(
				[InteractionID]
				,[CallDirectionTypeID]
				,[Station]
				,[PhoneNumber]
				,[AgentId]
				,[IsFirstUser]
				,[IsInteractionInitiator]
				,[DeviceTypeID]
				,[CTIAgentName]
				,[TrunkGroup]
				,[TrunkNumber]
				,[TrunkLabel]
				,[FirstName]
				,[LastName]
				,[Initiator]
				,[InteractionGMTStartTime]
				,[InteractionGMTStopTime]
				,[InteractionLocalStartTime]
				,[InteractionLocalStopTime]
				,[DurationInSeconds]
				,[InteractionOriginalID]
				,[DialedNumber]
				,[DiskFileName]
				,[CustodianDisplayName]
				,[LoadPhase]
				,[KPMGSegment]
				,[MiddleName]
				,[CLIANI]
				,[Location]
			)
		SELECT 
			SegmentID AS [InteractionID]
			,[CallDirectionID]
			,[Station]
			,[PhoneNumber]
			,[AgentId]
			,'' AS [IsFirstUser]
			,'' AS [IsInteractionInitiator]
			,NewDeviceTypeID AS [DeviceTypeID]
			,ParticipantCTIAgentName  AS [CTIAgentName]
			,[TrunkGroup]
			,[TrunkNumber]
			,[TrunkLabel]
			,[FirstName]
			,[LastName]
			, InitiatorTRID  AS [Initiator]
			,[InteractionGMTStartTime]
			,[InteractionGMTStopTime]
			,[InteractionLocalStartTime]
			,[InteractionLocalStopTime]
			,SegmentDurationInSeconds AS  [DurationInSeconds]
			,CrossCompleteID AS   [InteractionOriginalID]
			,[DialedNumber]
			,[FileName] AS [DiskFileName]
			,A.[CustodianDisplayName]
			,CONVERT(VARCHAR(30), GETDATE(), 121) -- '20160303 - Feb Load 15'
			,A.KPMGSegment
			,A.MiddleName
			,A.CLIANI
			,A.Location
		FROM 
			wip.Metadata_New AS A
		WHERE 
			DeviceType <> 'Speaker'
		AND 
			SegmentDurationInSeconds>=3
		AND 
			REPLACE(trunkLabel, ' ', '') NOT IN ('BBC1','BBC2','BLMBTV','CH4','CH5','CNBC','CNN','HSBCTV1','HSBCTV2','ITV','NEWS24','SKYNEWS','RAD4') 
		AND 
			SegmentID NOT IN 
						(
							SELECT DISTINCT 
								InteractionID 
							FROM 
								ETL.Metadata_Final_With_Media_Filename
						)
		--(44688 row(s) affected)


		--=============================
		-- NY, swith the firstname and
		-- lastname
		--=============================
		UPDATE
			a
		SET
			a.LastName = a.FirstName
			,a.FirstName = a.LastName
		FROM
			wip.Metadata_New a
		WHERE
			Location = 'NY'
		AND
			FirstName IN ('TRID','SBUS','MOB','EXT')

		--===============================
		-- For NY, if MOB's in the middle
		-- name, switch last name and 
		-- middle name
		--===============================

		UPDATE
			a
		SET
			a.LastName = a.MiddleName
			,a.MiddleName = a.LastName
		FROM
			wip.Metadata_New a
		WHERE
			Location = 'NY'
		AND
			(MiddleName LIKE '%MOB%' or Middlename like '%tru%phone%')

		--========================================================
		-- Update the filenames in the event that a re-extract has
		-- occurred and the physical media filenames have changed
		--========================================================

		UPDATE
			E
		SET 
			E.DiskFileName = UPPER(LTRIM(RTRIM(MSM.[FileName])))
		FROM 
			[ETL].[Metadata_Final_With_Media_Filename] AS E
		INNER JOIN 
			dbo.Metadata_Staging_Master MSM
		ON     
			E.InteractionID = MSM.SegmentID
		WHERE 
			UPPER(LTRIM(RTRIM(E.DiskFileName))) != UPPER(LTRIM(RTRIM(MSM.[FileName])))
		AND
			E.InteractionGMTStartTime = MSM.SegmentStartTime
		AND
			-- MSM.LoadId > @lastLoadBatchId
		MSM.LoadId IN
			(SELECT LoadBatchId FROM [ETL].[LoadBatchStatus] lbs WHERE lbs.Loaded = 0 AND lbs.Location = @location)
		AND
			MSM.Location = @location

		----------------------------------------

		-- UPDate previously ingested data
		--ETL
 
		UPDATE 
			[ETL].[Metadata_Final_With_Media_Filename]
		SET 
			TrunkLabel = LTRIM(RTRIM(b.trunklabel))
		FROM 
			[ETL].[Metadata_Final_With_Media_Filename] AS E
		LEFT JOIN 
			(
				SELECT DISTINCT 
					SegmentID
					,TraderID
					,LTRIM(RTRIM(TrunkLabel)) AS trunklabel
				FROM 
					ETL.TrunkLabels
				WHERE 
					TrunkLabel <> ''
			) AS b 
		ON     
			e.InteractionID = b.SegmentID
		WHERE 
			b.SegmentID IS NOT NULL


		UPDATE  
			[ETL].[Metadata_Final_With_Media_Filename]
		SET 
			TrunkLabel = '' 
		WHERE 
			TrunkLabel IS NULL



		--DROP NEWS CHANNELs (retain 212 calls already ingested for Review; )
		DELETE FROM 
			ETL.Metadata_Final_With_Media_Filename
		WHERE 
			InteractionID
		IN 
			(
				SELECT  
					InteractionID 
				FROM 
					ETL.Metadata_Final_With_Media_Filename AS M
				LEFT JOIN 
					dbo.ProductionMediaFiles AS P 
				ON 
					M.[DiskFileName] = P.MediaFilename
				WHERE 
					REPLACE(trunkLabel, ' ', '') IN ('BBC1','BBC2','BLMBTV','CH4','CH5','CNBC','CNN','HSBCTV1','HSBCTV2','ITV','NEWS24','SKYNEWS','RAD4') 
				AND 
				--	InteractionLocalStartTime >= '20160201'
				--AND 
					(P.MediaFilename IS NULL OR P.ProductionId = 'N0067')
			)
		----------------------------------------------------------------------------------------

		RETURN 0

	END TRY

	BEGIN CATCH

		RETURN -1

	END CATCH

END











GO


