
DECLARE @DataDate date
DECLARE @LocationId int
Declare @LocDescr varchar(3)

SET @DataDate='20180406'
SET @LocationId=1
SET @LocDescr = case when @LocationID=1 then 'LDN'
				when @LocationID=2 then 'NYC'
				when @LocationID=3 then 'HKG' end

select BusinessUnitID, count(*), count(distinct CallId)
from (

	select  distinct  sm.InteractionID CallId, sm.sourcemediaid, sd.BusinessUnitID
	FROM [Audio2_Metadata].report.[LexiconScores] as L  WITH (NOLOCK)
	left join [Audio2_Metadata].Nexidia.SourceMedia as SM  WITH (NOLOCK)
		on L.SourceMediaId=Sm.SourceMediaId
	INNER JOIN Voice_Analytics.filter.OutputNoFilter noload
		on sm.interactionId = noload.callid
	INNER JOIN Voice_Analytics.audio.CallCustodianIdentity cci
		ON noload.CallID = cci.CallID
	INNER JOIN Voice_Analytics.shared.CustodianIdentity sci
		ON cci.CustodianIdentityID = sci.CustodianIdentityID
	INNER JOIN Voice_Analytics.shared.Custodian sc
		ON sci.CustodianID = sc.CustodianID
	INNER JOIN Voice_Analytics.shared.Location sl
		ON sc.LocationID = sl.LocationID
	INNER JOIN Voice_Analytics.shared.Department sd
		ON sc.DepartmentID = sd.DepartmentID
	INNER JOIN Voice_Analytics.shared.BusinessUnit sbu
		ON sd.BusinessUnitID = sbu.BusinessUnitID
	INNER JOIN [Audio2_Metadata].dbo.LexiconParameters as LP  WITH (NOLOCK)
		ON  LP.CustodianAssetClass = sbu.code
		AND LP.CustodianLocation = sl.City
		AND LP.SearchTermListID = L.SearchTermListID 
		AND LP.CallDateTime <= CAST(noload.LocalStartTime as date)
		AND LP.EndDate >= CAST(noload.LocalStartTime as date)
	where l.score>=90
	AND CAST(sm.CallDateTime as DATE) = @DataDate -- w.g. '2018047'
	AND sc.LocationID = @LocationId -- 1 = LDN, 2=NYC, 3=HKG
	and sm.NexSignalID like '%No%'
	AND SM.DurationinMS < 3600000  --to exclude calls in excess of 1h

	) as K
	group by BusinessUnitID


	select custodianassetclass, count(*)
	from report.audio2report
	where signalidentifiers='lexicons'
	and cast(localstarttime as date)=@Datadate
	and CustodianLocation=@LocDescr
	group by CustodianAssetClass


	
--	select  distinct  sm.InteractionID CallId, sm.sourcemediaid, SM.IngestFileName
--	FROM [Audio2_Metadata].report.[LexiconScores] as L  WITH (NOLOCK)
--	left join [Audio2_Metadata].Nexidia.SourceMedia as SM  WITH (NOLOCK)
--		on L.SourceMediaId=Sm.SourceMediaId
--	INNER JOIN Voice_Analytics.filter.OutputNoFilter noload
--		on sm.interactionId = noload.callid
--	INNER JOIN Voice_Analytics.audio.CallCustodianIdentity cci
--		ON noload.CallID = cci.CallID
--	INNER JOIN Voice_Analytics.shared.CustodianIdentity sci
--		ON cci.CustodianIdentityID = sci.CustodianIdentityID
--	INNER JOIN Voice_Analytics.shared.Custodian sc
--		ON sci.CustodianID = sc.CustodianID
--	INNER JOIN Voice_Analytics.shared.Location sl
--		ON sc.LocationID = sl.LocationID
--	INNER JOIN Voice_Analytics.shared.Department sd
--		ON sc.DepartmentID = sd.DepartmentID
--	INNER JOIN Voice_Analytics.shared.BusinessUnit sbu
--		ON sd.BusinessUnitID = sbu.BusinessUnitID
--	INNER JOIN [Audio2_Metadata].dbo.LexiconParameters as LP  WITH (NOLOCK)
--		ON  LP.CustodianAssetClass = sbu.Name
--		AND LP.CustodianLocation = sl.City
--		AND LP.SearchTermListID = L.SearchTermListID 
--		AND LP.CallDateTime <= CAST(noload.LocalStartTime as date)
--		AND LP.EndDate >= CAST(noload.LocalStartTime as date)
--	where l.score>=90
--	AND CAST(sm.CallDateTime as DATE) = @DataDate -- w.g. '2018047'
--	AND sc.LocationID = @LocationId -- 1 = LDN, 2=NYC, 3=HKG
--	and sm.NexSignalID like '%No%'
--	AND SM.DurationinMS < 3600000  --to exclude calls in excess of 1h

--and sm.IngestFileName not in (select IngestFileName COLLATE Latin1_General_CI_AS
--from 
--				Audio2_Metadata.nexidia.lexicons_Sample)

--select *
--from report.audio2report
--	where signalidentifiers='lexicons'
--	and cast(localstarttime as date)=@Datadate
--	and CustodianLocation=@LocDescr
--	and filename COLLATE Latin1_General_CI_AS
--	 not in (
				
--	select  distinct SM.IngestFileName
--	FROM [Audio2_Metadata].report.[LexiconScores] as L  WITH (NOLOCK)
--	left join [Audio2_Metadata].Nexidia.SourceMedia as SM  WITH (NOLOCK)
--		on L.SourceMediaId=Sm.SourceMediaId
--	INNER JOIN Voice_Analytics.filter.OutputNoFilter noload
--		on sm.interactionId = noload.callid
--	INNER JOIN Voice_Analytics.audio.CallCustodianIdentity cci
--		ON noload.CallID = cci.CallID
--	INNER JOIN Voice_Analytics.shared.CustodianIdentity sci
--		ON cci.CustodianIdentityID = sci.CustodianIdentityID
--	INNER JOIN Voice_Analytics.shared.Custodian sc
--		ON sci.CustodianID = sc.CustodianID
--	INNER JOIN Voice_Analytics.shared.Location sl
--		ON sc.LocationID = sl.LocationID
--	INNER JOIN Voice_Analytics.shared.Department sd
--		ON sc.DepartmentID = sd.DepartmentID
--	INNER JOIN Voice_Analytics.shared.BusinessUnit sbu
--		ON sd.BusinessUnitID = sbu.BusinessUnitID
--	INNER JOIN [Audio2_Metadata].dbo.LexiconParameters as LP  WITH (NOLOCK)
--		ON  LP.CustodianAssetClass = sbu.Name
--		AND LP.CustodianLocation = sl.City
--		AND LP.SearchTermListID = L.SearchTermListID 
--		AND LP.CallDateTime <= CAST(noload.LocalStartTime as date)
--		AND LP.EndDate >= CAST(noload.LocalStartTime as date)
--	where l.score>=90
--	AND CAST(sm.CallDateTime as DATE) = @DataDate -- w.g. '2018047'
--	AND sc.LocationID = @LocationId -- 1 = LDN, 2=NYC, 3=HKG
--	and sm.NexSignalID like '%No%'
--	AND SM.DurationinMS < 3600000  --to exclude calls in excess of 1h
--	)