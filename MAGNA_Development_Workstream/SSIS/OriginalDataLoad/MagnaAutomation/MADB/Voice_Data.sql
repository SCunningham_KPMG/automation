USE [Voice_Data]
GO
/****** Object:  Schema [SSIS]    Script Date: 07/02/2018 11:04:30 ******/
CREATE SCHEMA [SSIS]
GO
/****** Object:  Table [dbo].[EventLog]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventLog](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[FileID] [int] NOT NULL,
	[EventType] [varchar](20) NULL,
	[EventDate] [datetime2](7) NULL,
	[Action] [varchar](200) NULL,
	[EventMessage] [varchar](max) NULL,
 CONSTRAINT [PK_EventLog_EventID_U_C] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FileLog]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FileLog](
	[FileID] [int] IDENTITY(1,1) NOT NULL,
	[FileTypeID] [smallint] NOT NULL,
	[FileName] [varchar](200) NOT NULL,
	[FileSize] [bigint] NULL,
	[FileCreationDate] [datetime2](7) NULL,
	[FullPath] [varchar](1000) NULL,
	[DateLogged] [datetime2](7) NULL,
	[DateAnalysed] [datetime2](7) NULL,
	[DateComplete] [datetime2](7) NULL,
	[isComplete] [bit] NULL,
 CONSTRAINT [PK_FileDetailLog_FileID_U_C] PRIMARY KEY CLUSTERED 
(
	[FileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FileType]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FileType](
	[FileTypeID] [smallint] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[ExpectedNamingConvention] [varchar](50) NOT NULL,
	[LocationDirectory] [varchar](50) NOT NULL,
	[FileTypeIdentifierText] [varchar](50) NOT NULL,
	[Delimiter] [varchar](10) NOT NULL,
	[TextQualifier] [varchar](10) NOT NULL,
	[DestinationTable] [varchar](100) NULL,
 CONSTRAINT [PK_FileType_FileTypeID_U_C] PRIMARY KEY CLUSTERED 
(
	[FileTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FileTypeSpecification]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FileTypeSpecification](
	[FileTypeID] [int] NOT NULL,
	[ExpectedColumnPosition] [int] NOT NULL,
	[ExpectedColumnName] [varchar](250) NOT NULL,
	[isRequired] [bit] NOT NULL,
 CONSTRAINT [PK_FileSpec_FileTypeID_ColumnPos_U_C] PRIMARY KEY CLUSTERED 
(
	[FileTypeID] ASC,
	[ExpectedColumnPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LDN_TrunkLabel]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LDN_TrunkLabel](
	[RecordId] [bigint] IDENTITY(1,1) NOT NULL,
	[FileId] [int] NULL,
	[iInteractionID] [varchar](max) NULL,
	[tiCallDirectionTypeID] [varchar](max) NULL,
	[nvcStation] [varchar](max) NULL,
	[nvcPhoneNumber] [varchar](max) NULL,
	[nvcAgentId] [varchar](max) NULL,
	[bitIsFirstUser] [varchar](max) NULL,
	[bitIsInteractionInitiator] [varchar](max) NULL,
	[tiDeviceTypeID] [varchar](max) NULL,
	[nvcCTIAgentName] [varchar](max) NULL,
	[vcTrunkGroup] [varchar](max) NULL,
	[vcTrunkNumber] [varchar](max) NULL,
	[nvcTrunkLabel] [varchar](max) NULL,
	[nvcFirstName] [varchar](max) NULL,
	[nvcLastName] [varchar](max) NULL,
	[Initiator] [varchar](max) NULL,
	[dtInteractionGMTStartTime] [varchar](max) NULL,
	[dtInteractionGMTStopTime] [varchar](max) NULL,
	[dtInteractionLocalStartTime] [varchar](max) NULL,
	[dtInteractionLocalStopTime] [varchar](max) NULL,
	[Duration] [varchar](max) NULL,
	[iInteractionOriginalID] [varchar](max) NULL,
	[iContactID] [varchar](max) NULL,
	[RuleDefaultSystemScore] [varchar](max) NULL,
	[RuleDefaultSystemEmotion] [varchar](max) NULL,
	[RuleDefaultProductivity] [varchar](max) NULL,
	[RuleDefaultQuality] [varchar](max) NULL,
	[dtModifyDate] [varchar](max) NULL,
	[tiLanguageId] [varchar](max) NULL,
	[biOriginalCallId] [varchar](max) NULL,
	[nvcCustomerId] [varchar](max) NULL,
	[nvcCaseId] [varchar](max) NULL,
	[iFCRDiff] [varchar](max) NULL,
	[dtContactGMTStartTime] [varchar](max) NULL,
	[iOriginalSiteID] [varchar](max) NULL,
	[biPrevContactID] [varchar](max) NULL,
	[iPrevSiteID] [varchar](max) NULL,
	[CallerName] [varchar](max) NULL,
	[ConnectedName] [varchar](max) NULL,
	[CalledName] [varchar](max) NULL,
	[CLI] [varchar](max) NULL,
	[DialledDigits] [varchar](max) NULL,
	[iEtkExported] [varchar](max) NULL,
	[nvcExportRule] [varchar](max) NULL,
	[BeforeAgentName] [varchar](max) NULL,
	[AfterAgentName] [varchar](max) NULL,
	[BeforeUserID] [varchar](max) NULL,
	[AfterUserID] [varchar](max) NULL,
	[Confidence] [varchar](max) NULL,
	[BDI_Sequence] [varchar](max) NULL,
	[BDI_Status] [varchar](max) NULL,
	[BDI_Direction] [varchar](max) NULL,
	[BDI_CreationTime] [varchar](max) NULL,
	[BDI_AccountName] [varchar](max) NULL,
	[BDI_AccountNumber] [varchar](max) NULL,
	[BDI_DialledNumber] [varchar](max) NULL,
	[BDI_DiversionNumber] [varchar](max) NULL,
	[BDI_CallStartTime] [varchar](max) NULL,
	[BDI_CallConnectTime] [varchar](max) NULL,
	[BDI_CallEndTime] [varchar](max) NULL,
	[BDI_SIPDestinationIP] [varchar](max) NULL,
	[BDI_SIPDestinationPort] [varchar](max) NULL,
	[BDI_MediaDestinationIP] [varchar](max) NULL,
	[BDI_MediaDestinationPort] [varchar](max) NULL,
	[BDI_Source] [varchar](max) NULL,
	[BDI_RecordedNumber] [varchar](max) NULL,
	[BDI_FirstName] [varchar](max) NULL,
	[BDI_LastName] [varchar](max) NULL,
	[BDI_Email] [varchar](max) NULL,
	[BDI_NewSequence] [varchar](max) NULL,
	[FromHeaderData] [varchar](max) NULL,
	[nvcDialedNumber] [varchar](max) NULL,
	[vcArchivePath] [varchar](max) NULL,
	[vcArchiveUniqueId] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [SSIS].[PackageConfig]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [SSIS].[PackageConfig](
	[VariableName] [varchar](200) NOT NULL,
	[Value] [varchar](200) NULL,
	[DataType] [varchar](50) NULL,
	[Purpose] [varchar](500) NULL,
 CONSTRAINT [PK_PackageConfig_VariableName_U_C] PRIMARY KEY CLUSTERED 
(
	[VariableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[FileType] ([FileTypeID], [Name], [ExpectedNamingConvention], [LocationDirectory], [FileTypeIdentifierText], [Delimiter], [TextQualifier], [DestinationTable]) VALUES (1, N'LDN Trunk Label Extract', N'TrunkLabelddmmyyyy', N'LDN', N'TrunkLabel', N',', N'"', N'dbo.LDN_TrunkLabel')
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 1, N'iInteractionID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 2, N'tiCallDirectionTypeID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 3, N'nvcStation', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 4, N'nvcPhoneNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 5, N'nvcAgentId', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 6, N'bitIsFirstUser', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 7, N'bitIsInteractionInitiator', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 8, N'tiDeviceTypeID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 9, N'nvcCTIAgentName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 10, N'vcTrunkGroup', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 11, N'vcTrunkNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 12, N'nvcTrunkLabel', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 13, N'nvcFirstName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 14, N'nvcLastName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 15, N'Initiator', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 16, N'dtInteractionGMTStartTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 17, N'dtInteractionGMTStopTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 18, N'dtInteractionLocalStartTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 19, N'dtInteractionLocalStopTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 20, N'Duration', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 21, N'iInteractionOriginalID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 22, N'iInteractionID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 23, N'iContactID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 24, N'RuleDefaultSystemScore', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 25, N'RuleDefaultSystemEmotion', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 26, N'RuleDefaultProductivity', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 27, N'RuleDefaultQuality', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 28, N'dtModifyDate', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 29, N'tiLanguageId', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 30, N'biOriginalCallId', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 31, N'nvcCustomerId', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 32, N'nvcCaseId', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 33, N'iFCRDiff', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 34, N'dtContactGMTStartTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 35, N'iOriginalSiteID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 36, N'biPrevContactID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 37, N'iPrevSiteID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 38, N'CallerName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 39, N'ConnectedName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 40, N'CalledName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 41, N'CLI', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 42, N'DialledDigits', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 43, N'iEtkExported', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 44, N'nvcExportRule', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 45, N'BeforeAgentName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 46, N'AfterAgentName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 47, N'BeforeUserID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 48, N'AfterUserID', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 49, N'Confidence', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 50, N'BDI_Sequence', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 51, N'BDI_Status', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 52, N'BDI_Direction', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 53, N'BDI_CreationTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 54, N'BDI_AccountName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 55, N'BDI_AccountNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 56, N'BDI_DialledNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 57, N'BDI_DiversionNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 58, N'BDI_CallStartTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 59, N'BDI_CallConnectTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 60, N'BDI_CallEndTime', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 61, N'BDI_SIPDestinationIP', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 62, N'BDI_SIPDestinationPort', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 63, N'BDI_MediaDestinationIP', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 64, N'BDI_MediaDestinationPort', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 65, N'BDI_Source', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 66, N'BDI_RecordedNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 67, N'BDI_FirstName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 68, N'BDI_LastName', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 69, N'BDI_Email', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 70, N'BDI_NewSequence', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 71, N'FromHeaderData', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 72, N'nvcDialedNumber', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 73, N'vcArchivePath', 1)
INSERT [dbo].[FileTypeSpecification] ([FileTypeID], [ExpectedColumnPosition], [ExpectedColumnName], [isRequired]) VALUES (1, 74, N'vcArchiveUniqueId', 1)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_BulkCopyTimeOut', N'0', NULL, NULL)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_EmailTo', N'Test@Test.com', NULL, NULL)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_FileCompletePath', N'C:\Development\Voice_Data\Metadata\Complete', NULL, NULL)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_FileErrorPath', N'C:\Development\Voice_Data\Metadata\Error', NULL, NULL)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_FilePendingPath', N'C:\Development\Voice_Data\Metadata\Pending', NULL, NULL)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_FileWaitTime', N'0', NULL, NULL)
INSERT [SSIS].[PackageConfig] ([VariableName], [Value], [DataType], [Purpose]) VALUES (N'dv_SmtpServer', N'SMTP', NULL, NULL)
/****** Object:  StoredProcedure [dbo].[usp_FileLog]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_FileLog]
	@FileTypeId SMALLINT,
	@FileName VARCHAR(200),
	@FileSize BIGINT,
	@FileCreationDate DATETIME,
	@FullPath VARCHAR(1000),
	@isComplete BIT = 0
AS


INSERT INTO dbo.FileLog
(
	FileTypeId, FileName, FileSize,FileCreationDate, FullPath, DateLogged, isComplete
)
OUTPUT inserted.FileID
VALUES
(@FileTypeID, @FileName,@FileSize, @FileCreationDate,@FullPath, GETDATE(), @isComplete)



GO
/****** Object:  StoredProcedure [dbo].[usp_GetEvents]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetEvents]
	@FileID INT,
	@RunID INT
AS

SET NOCOUNT ON;

--//TODO:  

SELECT 
	Type,
	CONVERT(VARCHAR(10),Dated,103) AS Date,
	CONVERT(VARCHAR(8),Timed) AS Time,
	CategoryType AS Category,
	CategorySubType AS Event,
	EventMessage As Message
FROM dbo.EventLog
WHERE
	SourceID = @FileID
AND RunID = @RunID

GO
/****** Object:  StoredProcedure [dbo].[usp_GetFileTypes]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetFileTypes]

AS

SELECT FileTypeID,Name,ExpectedNamingConvention, LocationDirectory, FileTypeIdentifierText,Delimiter, TextQualifier, DestinationTable
FROM
	dbo.FileType

GO
/****** Object:  StoredProcedure [dbo].[usp_GetFileTypeSpecification]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetFileTypeSpecification]
	@FileTypeID INT
AS

--// Set options
SET NOCOUNT ON;

--//TODO:  Error handling

SELECT ExpectedColumnPosition,ExpectedColumnName,isRequired
FROM
	dbo.FileTypeSpecification
WHERE FileTypeID = @FileTypeID;

GO
/****** Object:  StoredProcedure [dbo].[usp_GetVariable]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetVariable]
	@VariableName VARCHAR(200)
AS

SET NOCOUNT ON

--// TODO:  Error handling.  If variable does not exist!
SELECT Value FROM SSIS.PackageConfig
WHERE VariableName = @VariableName

GO
/****** Object:  StoredProcedure [dbo].[usp_LogEvent]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_LogEvent]
	@Type VARCHAR(20),
	@RunID INT,
	@CategoryType VARCHAR(200),
	@CategorySubType VARCHAR(200),
	@SourceId INT,
	@EventMessage VARCHAR(MAX)
	
AS

SET NOCOUNT ON;

--//TODO:  

INSERT INTO EventLog(RunID,Type, Dated, Timed,CategoryType,CategorySubType, SourceID,EventMessage, CreatedBy)
VALUES
	(@RunID, @Type,GETDATE(),GETDATE(), @CategoryType, @CategorySubType, @SourceId, @EventMessage, SUSER_NAME())

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateFileDetail]    Script Date: 07/02/2018 11:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_UpdateFileDetail]
	@FileID INT,
	@RemapDate DATETIME = NULL,
	@isComplete BIT = 0
AS

--//Set options
SET NOCOUNT ON;


UPDATE dbo.FileDetailLog
SET
	RemapDate = @RemapDate
WHERE
	FileID = @FileID

GO
