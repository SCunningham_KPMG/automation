﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandpit
{
    class Program
    {
        internal static bool DirectoryIsEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        static void Main(string[] args)
        {
            try
            {
                DoSomeStuff();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                System.Environment.Exit(-1);
            }

            Console.WriteLine("Now I'm there...");

        }

        private static void DoSomeStuff()
        {
            try
            {
                Console.WriteLine("Now I'm here...");

                while (!DirectoryIsEmpty(@"C:\EmptyFolder"))
                {
                    Console.WriteLine("Files found! Looping");
                    System.Threading.Thread.Sleep(6000);
                }

                Console.WriteLine(1 / int.Parse("0"));

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                throw;
            }
        }

    }
}
