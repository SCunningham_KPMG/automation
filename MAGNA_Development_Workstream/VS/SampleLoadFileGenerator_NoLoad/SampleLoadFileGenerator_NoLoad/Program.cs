﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SampleLoadFileGenerator_NoLoad
{
    class Program
    {

        static void writeLog(string infoToWrite, string logFileLocation)
        {
            string logFile = string.Format(@"{0}\{1}{2}.log", logFileLocation, "SampleFileGenerator_NoLoad_", DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture));
            try
            {
                using (StreamWriter swExceptions = new StreamWriter(logFile, true))
                {
                    swExceptions.WriteLine(string.Format(@"{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), infoToWrite));
                }
                Console.WriteLine(string.Format(@"{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), infoToWrite));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error writing to log file: {0}", ex);
            }
        }

        static void Main(string[] args)
        {

            string conn = ConfigurationManager.AppSettings.Get("VoiceDDB");
            string sampleDir = ConfigurationManager.AppSettings.Get("SampleDir");
            string copyFiles = ConfigurationManager.AppSettings.Get("CopyFiles"); // Can be toggled to generate a meta file without the file copy.
            string logFileLocation = ConfigurationManager.AppSettings.Get("LogFileLocation");
            string runLocal = ConfigurationManager.AppSettings.Get("RunLocal");

            string releaseId;

            DataTable dt = new DataTable();

            // Get the list of candidate calls

            Console.WriteLine("Getting list of calls...");
            writeLog("Getting list of calls...", logFileLocation);

            using (SqlConnection sqlConn = new SqlConnection(conn))
            {
                sqlConn.Open();

                using (SqlDataAdapter sqlDA = new SqlDataAdapter(@"dbo.SampleLoadFileSource_NoLoad", sqlConn))
                {
                    sqlDA.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDA.SelectCommand.CommandTimeout = 1800;
                    sqlDA.Fill(dt);
                }
            }

            int batchSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("BatchSize"));
            int batchPauseInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings.Get("BatchPauseInSeconds"));
            int batchPauseInMilliseconds = Convert.ToInt32((TimeSpan.FromSeconds(batchPauseInSeconds).TotalMilliseconds));
            int copiedThisBatch = 0;

            //================================
            // Get a release id for this batch
            //================================

            Console.WriteLine("Getting release ID...");
            writeLog("Getting release ID...", logFileLocation);

            using (SqlConnection relConn = new SqlConnection(conn))
            {
                relConn.Open();

                using (SqlCommand relComm = new SqlCommand(@"dbo.getNextReleaseId", relConn))
                {
                    relComm.CommandType = CommandType.StoredProcedure;
                    relComm.Parameters.Add("@releaseId", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

                    relComm.ExecuteNonQuery();

                    releaseId = relComm.Parameters["@releaseId"].Value.ToString();
                }
            }

            // Perform the copies first - any issues will mean the row stays set to 1 ready for anoter pass
            foreach (DataRow dr in dt.Rows)
            {
                string path;

                if (runLocal == "YES")
                {
                    path = dr["Directory"].ToString();
                }
                else
                {
                    path = dr["Directory_UNC"].ToString();
                }

                string fileName = dr["FileName"].ToString();

                try
                {

                    // Copy the file to the import directory if copying is switched on - could be we only want to generate the meta file
                    if (copyFiles.ToUpper() == "YES")
                    {
                        Console.WriteLine(string.Format(@"Copying: {0}\{1} to {2}\{3}", path, fileName, sampleDir, fileName));
                        writeLog(string.Format(@"Copying: {0}\{1} to {2}\{3}", path, fileName, sampleDir, fileName), logFileLocation);

                        File.Copy(string.Format(@"{0}\{1}", path, fileName), string.Format(@"{0}\{1}", sampleDir, fileName), true);

                        copiedThisBatch++;

                        if (copiedThisBatch == batchSize)
                        {
                            Console.WriteLine(string.Format(@"Batch size {0} reached. Pausing for {1} second(s)", batchSize, batchPauseInSeconds));
                            writeLog(string.Format(@"Batch size {0} reached. Pausing for {1} second(s)", batchSize, batchPauseInSeconds), logFileLocation);
                            copiedThisBatch = 0;
                            Thread.Sleep(batchPauseInMilliseconds);
                            writeLog("Resuming...", logFileLocation);
                        }
                    }

                    // Set the status of this row to 2 (copied)
                    dr["Status"] = 2;
                    // Stamp the row with the release Id
                    dr["ReleaseFileID"] = releaseId;

                    writeLog("Status and release id written", logFileLocation);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format(@"Error copying: {0}\{1} to {2}\{3}: {4}", path, fileName, sampleDir, fileName, ex.ToString()));
                    writeLog(string.Format(@"Error copying: {0}\{1} to {2}\{3}: {4}", path, fileName, sampleDir, fileName, ex.ToString()), logFileLocation);
                }

            } // End ForEach

            // Write the metadata file

            string dateForReleaseFile = DateTime.Now.ToString("yyyyMMdd");
            string sampleFile = string.Format(@"{0}\Nexidia_Sample_{1}-{2}.txt", sampleDir, dateForReleaseFile, releaseId);

            Console.WriteLine("Writing metadata file...");
            writeLog("Writing metadata file...", logFileLocation);

            try
            {

                using (StreamWriter sw = new StreamWriter(sampleFile, false, System.Text.Encoding.GetEncoding(1252)))
                {

                    sw.WriteLine("\"AudioFileName\"|\"CallId\"|\"StartDateTime\"|\"EndDateTime\"|\"Duration\"|\"DeviceType\"|\"TrunkLabel\"|\"Trader ID\"|\"TraderName\"|\"Desk\"|\"DirectionType\"|\"DialedNumber\"|\"CLI\"|\"PhoneNumber\"|\"SignalIdentifiers\"|\"Comment\"");

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Status"].ToString() == "2")
                        {
                            sw.WriteLine(string.Format("\"{0}\"|\"{1}\"|\"{2}\"|\"{3}\"|\"{4}\"|\"{5}\"|\"{6}\"|\"{7}\"|\"{8}\"|\"{9}\"|\"{10}\"|\"{11}\"|\"{12}\"|\"{13}\"|\"{14}\"|\"{15}\""
                                , dr["MediaFileName"].ToString() // 0
                                , dr["CallID"].ToString() // 1
                                , string.Format(@"{0:dd\MM\yyyy HH:mm:ss}", dr["LocalStartTime"].ToString()) // 2
                                , string.Format(@"{0:dd\MM\yyyy HH:mm:ss}", dr["LocalEndTime"].ToString()) // 3
                                , dr["DurationInSeconds"].ToString() // 4
                                , dr["DeviceType"].ToString() // 5
                                , dr["TrunkLabel"].ToString() // 6
                                , dr["TraderID"].ToString() // 7
                                , dr["TraderName"].ToString() // 8
                                , dr["CombinedBUandDepartment"].ToString() // 9
                                , dr["CallDirection"].ToString() // 10
                                , dr["DialedNumber"].ToString() // 11
                                , dr["CallLineIdentifier"].ToString() // 12
                                , dr["PhoneNumber"].ToString() // 13
                                , dr["SignalIdentifiers"].ToString() // 14
                                , dr["Comment"].ToString() // 15
                                ));
                        }
                    }

                } // End Streamwriter

                writeLog("SUCCESS - Finished writing metadata file.", logFileLocation);
            }
            catch (Exception ex)
            {
                writeLog( string.Format("ERROR - Writing metadata file: {0}", ex.Message), logFileLocation);
            }

            // Write the changes (status 2 changes) back to the DB

            string fbConn = ConfigurationManager.AppSettings.Get("VoiceDDB");

            writeLog("INFO - Writing status and release id information back to SQL...", logFileLocation);
            try
            {

                using (SqlConnection sqlFBConn = new SqlConnection(fbConn))
                {
                    sqlFBConn.Open();

                    // Clear the staging table
                    string sqlCom = @"TRUNCATE TABLE [ETL].[OutputFile_WriteBack]";

                    SqlCommand sqlClear = new SqlCommand(sqlCom, sqlFBConn);

                    sqlClear.ExecuteNonQuery();

                    // Copy new
                    using (SqlBulkCopy sqlBC = new SqlBulkCopy(sqlFBConn))
                    {
                        sqlBC.DestinationTableName = "[ETL].[OutputFile_WriteBack]";
                        sqlBC.WriteToServer(dt);
                    }

                    // Update the statuses and release id
                    sqlCom = @"UPDATE
	                                b
                                SET
	                                b.Status = a.Status
                                    ,b.ReleaseFileID = a.ReleaseFileID
                                FROM
	                                [ETL].[OutputFile_WriteBack] a
                                INNER JOIN
	                                Voice_Analytics.filter.[OutputNoFilter] b
                                ON
	                                a.CallID = b.CallID
                                WHERE
	                                a.Status != b.Status";

                    SqlCommand sqlUpdate = new SqlCommand(sqlCom, sqlFBConn);
                    sqlUpdate.ExecuteNonQuery();

                }

                writeLog("SUCCESS - Finished writing status and release id information back to SQL.", logFileLocation);

            }
            catch (Exception ex)
            {
                writeLog(string.Format("Error writing to SQL: {0}", ex.ToString()), logFileLocation);
            }
        }
    }
}
