﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileArchiver
{
    class Program
    {
        static void Main(string[] args)
        {

            string testDir = @"C:\Temp";

            string conn = ConfigurationManager.AppSettings.Get("TestDB");

            using (SqlConnection sqlConn = new SqlConnection(conn))
            {
                sqlConn.Open();

                using (SqlDataAdapter sqlDA = new SqlDataAdapter(@"SELECT * FROM [ETL].[FileDelivery]", sqlConn))
                {
                    DataTable dtFilesToMove = new DataTable();

                    sqlDA.Fill(dtFilesToMove);
                }

            }

        }
    }
}
