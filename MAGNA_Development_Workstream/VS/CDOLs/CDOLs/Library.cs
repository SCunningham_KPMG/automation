﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDOLs
{
    public static class Library
    {

        public static bool checkDB()
        {

            // Test database connection

            string sqlConnection = ConfigurationManager.AppSettings.Get("DBConnection");

            try
            {

                using (SqlConnection sqlConn = new SqlConnection(sqlConnection))
                {

                    sqlConn.Open();

                    using (SqlCommand sqlComm = new SqlCommand("SELECT TOP 1 * FROM sys.objects", sqlConn))
                    {

                        sqlComm.ExecuteNonQuery();

                    }
                }

                return true;
            }
            catch
            {
                return false;
            }

        }

        public static void fswObserver_Renamed(Observer oRen)
        {

            writeLog(DateTime.Now, string.Format("{0} - {1} was RENAMED", DateTime.Now.ToLongTimeString(), oRen.pathToWatch), oRen.logDirectory, @"FileWatch.log", oRen.loggingMode, 3);

            return;
        }

        public static void fswObserver_Deleted(Observer oDeleted)
        {

            try
            {
                writeLog(DateTime.Now, string.Format("{0} - {1} was DELETED ", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), oDeleted.pathToWatch)
                    , oDeleted.logDirectory, @"FileWatch.log", oDeleted.loggingMode, 3);

                // Safe here to store details of the files as, even if created, the Changed method is fired (several times!)

                DirectoryInfo di = new DirectoryInfo(oDeleted.pathToWatch);
                oDeleted.filesInDir = di.GetFiles("*.*", SearchOption.AllDirectories);

                // If a fresh snapshot, store the comparison set
                if (oDeleted.filesInDirComparisonSet == null || oDeleted.filesInDirComparisonSet.Length == 0)
                {
                    oDeleted.filesInDirComparisonSet = oDeleted.filesInDir;
                }

                // Inital check - file count
                if (oDeleted.filesInDir.Count() != oDeleted.filesInDirComparisonSet.Count())
                {
                    writeLog(DateTime.Now, string.Format("File count has changed  - next checks"), oDeleted.logDirectory, @"FileWatch.log", oDeleted.loggingMode, 3);

                    oDeleted.hasChangedCount = true;
                }
                else
                {
                    oDeleted.hasChangedCount = false;
                }

                // Get the max Last Access datetime value from the current file list
                foreach (FileInfo fiMaxLastAccessTime in oDeleted.filesInDir)
                {
                    if (fiMaxLastAccessTime.LastAccessTime > oDeleted.filesInDirMaxLastAccessTime)
                    {
                        oDeleted.filesInDirMaxLastAccessTime = fiMaxLastAccessTime.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Now compare against the max Last Access datetime value from the stored file list
                // DateTime test = DateTime.MinValue;
                foreach (FileInfo fiMaxLastAccessTimeinDirComp in oDeleted.filesInDirComparisonSet)
                {
                    if (fiMaxLastAccessTimeinDirComp.LastAccessTime > oDeleted.filesInDirComparisonSetMaxLastAccessTime)
                    {
                        oDeleted.filesInDirComparisonSetMaxLastAccessTime = fiMaxLastAccessTimeinDirComp.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Compare the winners of the two loops
                if (oDeleted.filesInDirMaxLastAccessTime > oDeleted.filesInDirComparisonSetMaxLastAccessTime)
                {
                    // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                    oDeleted.hasChangedLastAccessTime = true;

                    writeLog(DateTime.Now, string.Format("DELETED - {0} - Latest File Last Access Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToLongTimeString()
                        , oDeleted.pathToWatch, oDeleted.filesInDirMaxLastAccessTime, oDeleted.filesInDirComparisonSetMaxLastAccessTime), oDeleted.logDirectory, @"FileWatch.log", oDeleted.loggingMode, 3);
                }


                // Get the max Last Access datetime value from the current file list
                // DateTime filesInDirMaxLastWriteTime = DateTime.MinValue;
                foreach (FileInfo fiMaxLastWriteTime in oDeleted.filesInDir)
                {
                    if (fiMaxLastWriteTime.LastWriteTime > oDeleted.filesInDirMaxLastWriteTime)
                    {
                        oDeleted.filesInDirMaxLastWriteTime = fiMaxLastWriteTime.LastWriteTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Now compare against the max Last Access datetime value from the stored file list
                foreach (FileInfo fiMaxLastWriteTimeinDirComp in oDeleted.filesInDirComparisonSet)
                {
                    if (fiMaxLastWriteTimeinDirComp.LastWriteTime > oDeleted.filesInDirComparisonSetMaxLastWriteTime)
                    {
                        oDeleted.filesInDirComparisonSetMaxLastWriteTime = fiMaxLastWriteTimeinDirComp.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Compare the winners of the two loops
                if (oDeleted.filesInDirMaxLastWriteTime > oDeleted.filesInDirComparisonSetMaxLastWriteTime)
                {
                    // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                    oDeleted.hasChangedLastWriteTime = true;
                    oDeleted.filesInDirComparisonSetMaxLastWriteTime = oDeleted.filesInDirMaxLastWriteTime;

                    writeLog(DateTime.Now, string.Format("DELETED - {0} - Latest File Last Write Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToLongTimeString()
                        , oDeleted.pathToWatch, oDeleted.filesInDirMaxLastWriteTime, oDeleted.filesInDirComparisonSetMaxLastWriteTime), oDeleted.logDirectory, @"FileWatch.log", oDeleted.loggingMode, 3);

                }


                // Check if any action has been taken on the file list
                if (oDeleted.hasChangedCount || oDeleted.hasChangedLastAccessTime || oDeleted.hasChangedLastWriteTime)
                {

                    writeLog(DateTime.Now, string.Format("DELETED EVENT - Activity detected since last timer poll - continue", DateTime.Now.ToLongTimeString(), oDeleted.pathToWatch
                        , oDeleted.filesInDirMaxLastAccessTime
                        , oDeleted.filesInDirComparisonSetMaxLastAccessTime)
                        , oDeleted.logDirectory
                        , @"FileWatch.log"
                        , oDeleted.loggingMode, 3);

                    // Set the comparison to the main set so that we don't keep getting notified of the same event
                    oDeleted.filesInDirComparisonSet = oDeleted.filesInDir;
                }

            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("DELETED EVENT EXCEPTION - Error: {0}", ex), oDeleted.logDirectory, @"FileWatch.log", oDeleted.loggingMode, 99);
            }
        }

        public static void fswObserver_Created(Observer oCreated)
        {

            try
            {
                writeLog(DateTime.Now, string.Format("{0} - {1} was CREATED ", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), oCreated.pathToWatch)
                    , oCreated.logDirectory, @"FileWatch.log", oCreated.loggingMode, 3);

                // Safe here to store details of the files as, even if created, the Changed method is fired (several times!)

                DirectoryInfo di = new DirectoryInfo(oCreated.pathToWatch);
                oCreated.filesInDir = di.GetFiles("*.*", SearchOption.AllDirectories);

                // If a fresh snapshot, store the comparison set
                if (oCreated.filesInDirComparisonSet == null || oCreated.filesInDirComparisonSet.Length == 0)
                {
                    oCreated.filesInDirComparisonSet = oCreated.filesInDir;
                }

                // Inital check - file count
                if (oCreated.filesInDir.Count() != oCreated.filesInDirComparisonSet.Count())
                {
                    writeLog(DateTime.Now, string.Format("File count has changed  - next checks"), oCreated.logDirectory, @"FileWatch.log", oCreated.loggingMode, 3);

                    oCreated.hasChangedCount = true;
                }
                else
                {
                    oCreated.hasChangedCount = false;
                }


                // Get the max Last Access datetime value from the current file list
                foreach (FileInfo fiMaxLastAccessTime in oCreated.filesInDir)
                {
                    if (fiMaxLastAccessTime.LastAccessTime > oCreated.filesInDirMaxLastAccessTime)
                    {
                        oCreated.filesInDirMaxLastAccessTime = fiMaxLastAccessTime.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Now compare against the max Last Access datetime value from the stored file list
                foreach (FileInfo fiMaxLastAccessTimeinDirComp in oCreated.filesInDirComparisonSet)
                {
                    if (fiMaxLastAccessTimeinDirComp.LastAccessTime > oCreated.filesInDirComparisonSetMaxLastAccessTime)
                    {
                        oCreated.filesInDirComparisonSetMaxLastAccessTime = fiMaxLastAccessTimeinDirComp.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Compare the winners of the two loops
                if (oCreated.filesInDirMaxLastAccessTime > oCreated.filesInDirComparisonSetMaxLastAccessTime)
                {
                    // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                    oCreated.hasChangedLastAccessTime = true;

                    writeLog(DateTime.Now, string.Format("CREATED - {0} - Latest File Last Access Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        , oCreated.pathToWatch, oCreated.filesInDirMaxLastAccessTime, oCreated.filesInDirComparisonSetMaxLastAccessTime), oCreated.logDirectory, @"FileWatch.log", oCreated.loggingMode, 3);
                }



                // Get the max Last Access datetime value from the current file list

                foreach (FileInfo fiMaxLastWriteTime in oCreated.filesInDir)
                {
                    if (fiMaxLastWriteTime.LastWriteTime > oCreated.filesInDirMaxLastWriteTime)
                    {
                        oCreated.filesInDirMaxLastWriteTime = fiMaxLastWriteTime.LastWriteTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Now compare against the max Last Access datetime value from the stored file list
                foreach (FileInfo fiMaxLastWriteTimeinDirComp in oCreated.filesInDirComparisonSet)
                {
                    if (fiMaxLastWriteTimeinDirComp.LastWriteTime > oCreated.filesInDirComparisonSetMaxLastWriteTime)
                    {
                        oCreated.filesInDirComparisonSetMaxLastWriteTime = fiMaxLastWriteTimeinDirComp.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Compare the winners of the two loops
                if (oCreated.filesInDirMaxLastWriteTime > oCreated.filesInDirComparisonSetMaxLastWriteTime)
                {
                    // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                    oCreated.hasChangedLastWriteTime = true;
                    oCreated.filesInDirComparisonSetMaxLastWriteTime = oCreated.filesInDirMaxLastWriteTime;
                    writeLog(DateTime.Now, string.Format("CREATED - {0} - Latest File Last Write Time for file {1} of {2} has changed from stored value of {3}",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), oCreated.pathToWatch, oCreated.filesInDirMaxLastWriteTime, oCreated.filesInDirComparisonSetMaxLastWriteTime)
                        , oCreated.logDirectory, @"FileWatch.log", oCreated.loggingMode, 3);

                }


                // Check if any action has been taken on the file list
                if (oCreated.hasChangedCount || oCreated.hasChangedLastAccessTime || oCreated.hasChangedLastWriteTime)
                {
                    writeLog(DateTime.Now, string.Format("CREATED EVENT - Activity detected since last timer poll - continue", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        , oCreated.pathToWatch, oCreated.filesInDirMaxLastAccessTime, oCreated.filesInDirComparisonSetMaxLastAccessTime), oCreated.logDirectory, @"FileWatch.log", oCreated.loggingMode, 3);

                    // Set the comparison to the main set so that we don't keep getting notified of the same event
                    oCreated.filesInDirComparisonSet = oCreated.filesInDir;
                }
            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("CREATED EVENT EXCEPTION - Error: {0}", ex), oCreated.logDirectory, @"FileWatch.log", oCreated.loggingMode, 99);
            }

        }

        public static void fswObserver_Changed(Observer oChanged)
        {

            try
            {
                writeLog(DateTime.Now, string.Format("{0} - {1} was CHANGED ", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    , oChanged.pathToWatch), oChanged.logDirectory, @"FileWatch.log", oChanged.loggingMode, 3);

                // Safe here to store details of the files as, even if created, the Changed method is fired (several times!)

                DirectoryInfo di = new DirectoryInfo(oChanged.pathToWatch);
                oChanged.filesInDir = di.GetFiles("*.*", SearchOption.AllDirectories);

                // Store the parent and grandparent so that when we transfer later, the files know where to go

                //foreach(FileInfo fileFound in oChanged.filesInDir)
                //{
                //    oChanged.directoriesForFiles
                //}


                // If a fresh snapshot, store the comparison set
                if (oChanged.filesInDirComparisonSet == null || oChanged.filesInDirComparisonSet.Length == 0)
                {
                    oChanged.filesInDirComparisonSet = oChanged.filesInDir;
                }

                // Inital check - file count
                if (oChanged.filesInDir.Count() != oChanged.filesInDirComparisonSet.Count())
                {
                    writeLog(DateTime.Now, string.Format("File count has changed  - next checks"), oChanged.logDirectory, @"FileWatch.log", oChanged.loggingMode, 3);

                    oChanged.hasChangedCount = true;
                }
                else
                {
                    oChanged.hasChangedCount = false;
                }

                // Get the max Last Access datetime value from the current file list
                foreach (FileInfo fiMaxLastAccessTime in oChanged.filesInDir)
                {
                    if (fiMaxLastAccessTime.LastAccessTime > oChanged.filesInDirMaxLastAccessTime)
                    {
                        oChanged.filesInDirMaxLastAccessTime = fiMaxLastAccessTime.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Now compare against the max Last Access datetime value from the stored file list
                foreach (FileInfo fiMaxLastAccessTimeinDirComp in oChanged.filesInDirComparisonSet)
                {
                    if (fiMaxLastAccessTimeinDirComp.LastAccessTime > oChanged.filesInDirComparisonSetMaxLastAccessTime)
                    {
                        oChanged.filesInDirComparisonSetMaxLastAccessTime = fiMaxLastAccessTimeinDirComp.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Compare the winners of the two loops
                if (oChanged.filesInDirMaxLastAccessTime > oChanged.filesInDirComparisonSetMaxLastAccessTime)
                {
                    // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                    oChanged.hasChangedLastAccessTime = true;

                    writeLog(DateTime.Now, string.Format("{0} - Latest File Last Access Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        , oChanged.pathToWatch, oChanged.filesInDirMaxLastAccessTime, oChanged.filesInDirComparisonSetMaxLastAccessTime), oChanged.logDirectory, @"FileWatch.log", oChanged.loggingMode, 3);
                }



                // Get the max Last Access datetime value from the current file list
                // DateTime filesInDirMaxLastWriteTime = DateTime.MinValue;
                foreach (FileInfo fiMaxLastWriteTime in oChanged.filesInDir)
                {
                    if (fiMaxLastWriteTime.LastWriteTime > oChanged.filesInDirMaxLastWriteTime)
                    {
                        oChanged.filesInDirMaxLastWriteTime = fiMaxLastWriteTime.LastWriteTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Now compare against the max Last Access datetime value from the stored file list
                // DateTime test = DateTime.MinValue;
                foreach (FileInfo fiMaxLastWriteTimeinDirComp in oChanged.filesInDirComparisonSet)
                {
                    if (fiMaxLastWriteTimeinDirComp.LastWriteTime > oChanged.filesInDirComparisonSetMaxLastWriteTime)
                    {
                        oChanged.filesInDirComparisonSetMaxLastWriteTime = fiMaxLastWriteTimeinDirComp.LastAccessTime;
                        // At least one has changed, so exit the loop
                        break;
                    }
                }

                // Compare the winners of the two loops
                if (oChanged.filesInDirMaxLastWriteTime > oChanged.filesInDirComparisonSetMaxLastWriteTime)
                {
                    // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                    oChanged.hasChangedLastWriteTime = true;
                    oChanged.filesInDirComparisonSetMaxLastWriteTime = oChanged.filesInDirMaxLastWriteTime;

                    writeLog(DateTime.Now, string.Format("{0} - Latest File Last Write Time for file {1} of {2} has changed from stored value of {3}"
                        , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), oChanged.pathToWatch, oChanged.filesInDirMaxLastWriteTime, oChanged.filesInDirComparisonSetMaxLastWriteTime)
                        , oChanged.logDirectory, @"FileWatch.log", oChanged.loggingMode, 3);
                }


                // Check if any action has been taken on the file list
                if (oChanged.hasChangedCount || oChanged.hasChangedLastAccessTime || oChanged.hasChangedLastWriteTime)
                {
                    writeLog(DateTime.Now, string.Format("CHANGED EVENT - Activity detected since last timer poll - continue", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        , oChanged.pathToWatch, oChanged.filesInDirMaxLastAccessTime, oChanged.filesInDirComparisonSetMaxLastAccessTime), oChanged.logDirectory, @"FileWatch.log", oChanged.loggingMode, 3);

                    // Set the comparison to the main set so that we don't keep getting notified of the same event
                    oChanged.filesInDirComparisonSet = oChanged.filesInDir;
                }

            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("CHANGED EVENT EXCEPTION - Error: {0}", ex), oChanged.logDirectory, @"FileWatch.log", oChanged.loggingMode, 99);
            }
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        public static int processArchive(string fileToProcessIncPath, string extractToHere, string appLocation, string key, string application, string actionToTake, string fileFilter)
        {
            int exitCodeFirst;
            string arguments = @"";
            string fileName = @"";

            switch (application.ToUpper())
            {
                case "7Z.EXE":
                    fileName = string.Format(@"{0}\{1}", appLocation, application);
                    arguments = string.Format(@"{0} {1} -o{2} -p{3} {4} -r -aos", actionToTake, fileToProcessIncPath, extractToHere, key.Trim(), fileFilter);
                    break;
                case "WINRAR.EXE":
                    arguments = string.Format("{0} -ibck -inul {1} -p{2} ", actionToTake, fileToProcessIncPath, key.Trim());
                    //runBatchProcessStartInfo.Arguments = string.Format("x -ibck -inul -ilogc:\\temp\\wrt.log -n{0} -p{1} -o+ {2} {3}", specificAudioType, unzipKey.Trim(), zipFile.Trim(), extractDirectory.Trim());
                    break;
            }

            if (File.Exists(fileToProcessIncPath))
            {

                Process runBatchProcess;
                ProcessStartInfo runBatchProcessStartInfo;

                runBatchProcessStartInfo = new ProcessStartInfo();
                runBatchProcessStartInfo.FileName = fileName;
                runBatchProcessStartInfo.Arguments = arguments;

                runBatchProcessStartInfo.CreateNoWindow = true;
                runBatchProcessStartInfo.UseShellExecute = false;
                runBatchProcessStartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                runBatchProcess = new Process();
                runBatchProcess.StartInfo = runBatchProcessStartInfo;

                runBatchProcess.Start();
                runBatchProcess.WaitForExit();

                exitCodeFirst = runBatchProcess.ExitCode;

                runBatchProcess.Close();
                runBatchProcess.Dispose();

            }
            else
            {
                exitCodeFirst = 2112;
            }

            return exitCodeFirst;
        }

        public static void writeLog(DateTime dateStamp, string message, string folder, string file, string loggingMode, int eventId)
        {

            string logToFile = ConfigurationManager.AppSettings.Get("LogToFile");
            string logToDB = ConfigurationManager.AppSettings.Get("LogToDB");

            if (logToFile == "Yes" && loggingMode != "None")
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(string.Format(@"{0}\\{1}", folder, file), true, System.Text.Encoding.GetEncoding(1252)))
                    {
                        sw.WriteLine(string.Format(@"{0} - {1}", dateStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), message));
                    }
                }
                catch
                {

                }
            }

            if (logToDB == "Yes" && loggingMode != "None")
            {
                try
                {
                    string sqlConnection = ConfigurationManager.AppSettings.Get("DBConnection"); //@"Persist Security Info=False;Integrated Security=true;Initial Catalog=Voice_DEV;server=VDBSTRS01P001\ISQL01"; // TODO: config driven

                    using (SqlConnection sqlConn = new SqlConnection(sqlConnection))
                    {

                        sqlConn.Open();

                        using (SqlCommand sqlComm = new SqlCommand("CDOL.AddEventLog", sqlConn))
                        {
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.AddWithValue("@eventId", eventId);
                            sqlComm.Parameters.AddWithValue("@eventTime", dateStamp);
                            sqlComm.Parameters.AddWithValue("@eventDescription", message);

                            sqlComm.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (StreamWriter sw = new StreamWriter(string.Format(@"{0}\\{1}", folder, file), true, System.Text.Encoding.GetEncoding(1252)))
                    {
                        sw.WriteLine(string.Format(@"{0} - {1}: {2}", dateStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), "ERROR - SQL Add Event failed.", ex.Message));
                    }
                }

            }
        }

        public static void elapsedTimeEvents(Observer ooCon, System.Timers.Timer timer)
        {
            writeLog(DateTime.Now, "Timed scan of observed directory occurred", ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 2);

            bool oChecks;

            ooCon.lastActionThresholdBreach = false;

            // Assume nothing to do
            // lastActionThresholdBreach = false;

            oChecks = observerChecks(ooCon);

            if (ooCon.lastActionThresholdBreach)
            {
                // Last action on the folder was beyond the agreed threshold
                // Transfer of files can take place.
                writeLog(DateTime.Now, string.Format("OBSERVER - last action on the folder was longer than {0} millisecond(s) ago. Begin transfer.", ooCon.lastActionThreshold), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);


                // Suspend the timer first so that timed activities do not interfere
                writeLog(DateTime.Now, "Suspending timer during transfer and zip processing", ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                timer.Stop();

                #region Perform file transfers

                //***********************
                // Perform file transfers
                //***********************

                bool startTransfer;
                startTransfer = transferFiles(ooCon);

                #endregion

                // Refresh the file lists
                DirectoryInfo di = new DirectoryInfo(ooCon.pathToWatch);
                ooCon.filesInDir = di.GetFiles("*.*", SearchOption.AllDirectories);
                ooCon.filesInDirComparisonSet = ooCon.filesInDir;

                if (startTransfer && ooCon.filesInDir.Length == 0)
                {
                    // Successfully transferred at least one file in the last transfer run and the
                    // new file list length is zero (no files). Reset the hi scores.

                    ooCon.filesInDirMaxLastAccessTime = DateTime.MinValue;
                    ooCon.filesInDirComparisonSetMaxLastAccessTime = DateTime.MinValue;
                    ooCon.filesInDirMaxLastWriteTime = DateTime.MinValue;
                    ooCon.filesInDirComparisonSetMaxLastWriteTime = DateTime.MinValue;
                    ooCon.filesInDirMaxLastCreateTime = DateTime.MinValue;
                    ooCon.filesInDirComparisonSetMaxLastCreateTime = DateTime.MinValue;

                    writeLog(DateTime.Now, string.Format("OBSERVER - Empty directory found, time flags reset."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                }

                // ================================================ UNZIP ========================================================

                // Check for any new zips to unpack
                // Check that the last transfer attempted completed successfully
                if (startTransfer)
                {

                    // Get a file delivery group id. This allows:
                    // Identification of files that have been received during a given period
                    // Identification of files contained within an archive as belonging to that archive (file list and archive connecion).

                    long fileDeliveryId = GetFileDeliveryId(ooCon.logDirectory, ooCon.loggingMode);

                    // TODO: Move these
                    // Boolean filesFoundToProcess = false;
                    Boolean headerFixed;
                    DirectoryInfo parentDirectory;
                    int exitCode = 0; // Default success

                    //string a;
                    //// string args;
                    //string b;
                    // string[] currentZipFiles;
                    string dataPendingDirectoryWorking;
                    string destinationPath;
                    string destinationPathSuccess;
                    string directoryOnly;
                    string exitCodeDescription;
                    string extractDirectory;
                    string fileNameOnly;
                    string location;
                    string parentDirForDelivery;
                    string parentDirectoryName;
                    // string processedDirectory; // SET THIS FROM THE APP CONFIG
                    // string programFiles = "C:\\Program Files"; //  SET THIS FROM THE APP CONFIG
                    string applicationLocation = ConfigurationManager.AppSettings.Get("ArchiveApplicationLocation"); // e.g. C:\Program Files\7-Zip
                    string applicationName = ConfigurationManager.AppSettings.Get("ArchiveApplication"); // e.g. 7Z.exe
                    string processedZipsSuccessDirectory = ConfigurationManager.AppSettings.Get("ProcessedZipsSuccessDirectory");
                    string processedZipsErrorDirectory = ConfigurationManager.AppSettings.Get("ProcessedZipsErrorDirectory");
                    string uniqueParentDirectoryName;
                    string unzipKey;
                    string[] zipFileList;

                    string sourcePath;
                    string manifestSearchPattern;
                    string trunkLabelSearchPattern;

                    // Move the files into position
                    //      Trunk labels
                    //      Manifests
                    // Metadata and audio will be moved into position automatically by the unzip process
                    //      SJC - 2018-04-19 - Metadata may be corrected by the client and sent across outside of a zip. Change to accomadate this.

                    // Trunk labels
                    // Look in root of delivery
                    // Build file list
                    // Loop and determine location and date of delivery
                    // Move to trunk label area using that info

                    writeLog(DateTime.Now, string.Format("OBSERVER - Moving files into position - Trunk labels."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                    sourcePath = ConfigurationManager.AppSettings.Get("TransferToDirectory");
                    destinationPathSuccess = ConfigurationManager.AppSettings.Get("ExtractDirectoryTrunkLabel");
                    string destinationPathError = ConfigurationManager.AppSettings.Get("ProcessedTrunkLabelErrorDirectory");
                    trunkLabelSearchPattern = ConfigurationManager.AppSettings.Get("TrunkLabelSearchPattern");
                    string fileType;
                    Boolean error = false;

                    fileType = "TrunkLabel"; // TODO: Make config as a change to the directory name will break this

                    // TODO: Refactor this
                    // bool processTrunkLabel = ProcessTrunkLabel(sourcePath, trunkLabelSearchPattern, ooCon);

                    try
                    {

                        string[] trunkLabelList = Directory.GetFiles(sourcePath, trunkLabelSearchPattern, SearchOption.AllDirectories);

                        // Any found?
                        if (trunkLabelList != null && trunkLabelList.Length != 0)
                        {

                            writeLog(DateTime.Now, string.Format("OBSERVER - trunk labels found - processing."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                            // Found some, loop and move
                            foreach (string trunkLabelFile in trunkLabelList)
                            {
                                // Get location and delivery day
                                parentDirForDelivery = Directory.GetParent(trunkLabelFile).ToString();
                                parentDirectory = new DirectoryInfo(parentDirForDelivery);
                                parentDirectoryName = parentDirectory.Name;
                                location = parentDirectory.Parent.Name;
                                fileNameOnly = Path.GetFileName(trunkLabelFile); //TODO variable name is misleading

                                // Assume a success until any errors are found
                                destinationPath = destinationPathSuccess;
                                error = false;

                                // Error check - if the file is OKB this is a useless file and should not proceed to the processing area.
                                // Set the detsination directory to the error folder
                                if (trunkLabelFile.Length == 0)
                                {
                                    destinationPath = destinationPathError;
                                    error = true;
                                }

                                // string transferDirectory = string.Format(@"{0}\{1}\{2}", destinationPath, location, parentDirectoryName);
                                string transferDirectory = string.Format(@"{0}\{1}\{2}\{3}", destinationPath, location, fileType, parentDirectoryName);

                                // Check if the destination directory exists and create if not
                                if (!Directory.Exists(transferDirectory))
                                {
                                    Directory.CreateDirectory(transferDirectory);
                                }

                                string transferDirectoryAndFile = string.Format(@"{0}\{1}\{2}\{3}\{4}", destinationPath, location, fileType, parentDirectoryName, fileNameOnly);

                                // Check if the file already exists - this will cause a failure if it does
                                if (File.Exists(transferDirectoryAndFile))
                                {
                                    File.Delete(transferDirectoryAndFile);
                                }
                                // Move the file
                                File.Move(trunkLabelFile, transferDirectoryAndFile);

                                // Alter the file (providing not in error)

                                // 1: Any spaces in the column names to be removed
                                // 2: Repeated column header field name to be renamed

                                // 1
                                if (error == false)
                                {
                                    headerFixed = FixHeader(transferDirectoryAndFile, ooCon.logDirectory, ooCon.loggingMode);
                                }

                            }

                        }

                        writeLog(DateTime.Now, string.Format("OBSERVER - successful move of trunk label files."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                    }

                    catch (Exception ex)
                    {
                        writeLog(DateTime.Now, string.Format("OBSERVER - ERROR - problem during processing of trunk label files: {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 2);
                    }

                    // Manifests
                    // Look in root of delivery
                    // Build file list
                    // Loop and determine location and date of delivery
                    // Move to manifest area using that info

                    sourcePath = ConfigurationManager.AppSettings.Get("TransferToDirectory");
                    destinationPathSuccess = ConfigurationManager.AppSettings.Get("ExtractDirectoryManifest"); // e.g. vfsrnex01p001\hsbc\Voice_Data\MetaData\Pending - add in location, type and day of receipt before using
                    destinationPathError = ConfigurationManager.AppSettings.Get("ProcessedManifestErrorDirectory"); // e.g. vfsrnex01p001\hsbc\Voice_Data\MetaData\Pending - add in location, type and day of receipt before using
                    manifestSearchPattern = ConfigurationManager.AppSettings.Get("ManifestSearchPattern");
                    fileType = @"manifest";

                    writeLog(DateTime.Now, string.Format("OBSERVER - Moving files into position - manifests."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                    try
                    {

                        string[] manifestList = Directory.GetFiles(sourcePath, manifestSearchPattern, SearchOption.AllDirectories);

                        // Any found?
                        if (manifestList != null && manifestList.Length != 0)
                        {

                            // Found some, loop and move
                            foreach (string manifestFile in manifestList)
                            {
                                // Get location and delivery day
                                parentDirForDelivery = Directory.GetParent(manifestFile).ToString();
                                parentDirectory = new DirectoryInfo(parentDirForDelivery);
                                parentDirectoryName = parentDirectory.Name;
                                location = parentDirectory.Parent.Name;
                                fileNameOnly = Path.GetFileName(manifestFile);


                                // Assume a success until any errors are found
                                destinationPath = destinationPathSuccess;
                                error = false;

                                // Error check - if the file is OKB this is a useless file and should not proceed to the processing area.
                                // Set the detsination directory to the error folder
                                if (manifestFile.Length == 0)
                                {
                                    destinationPath = destinationPathError;
                                    error = true;
                                }

                                string transferDirectory = string.Format(@"{0}\{1}\{2}\{3}", destinationPath, location, fileType, parentDirectoryName);

                                // Check if the destination directory exists and create if not
                                if (!Directory.Exists(transferDirectory))
                                {
                                    Directory.CreateDirectory(transferDirectory);
                                }
                                // Check if the file already exists - this will cause a failure if it does
                                if (File.Exists(transferDirectory + "\\" + fileNameOnly))
                                {
                                    File.Delete(transferDirectory + "\\" + fileNameOnly);
                                }

                                // Move the file
                                string transferDirectoryAndFile = string.Format(@"{0}\{1}\{2}\{3}\{4}", destinationPath, location, fileType, parentDirectoryName, fileNameOnly);

                                // Check if the file already exists - this will cause a failure if it does
                                if (File.Exists(transferDirectoryAndFile))
                                {
                                    File.Delete(transferDirectoryAndFile);
                                }
                                // Move the file
                                File.Move(manifestFile, transferDirectoryAndFile);

                                // Only try and modify if we haven't quarantined the file
                                if (error == false)
                                {
                                    // Change from colon field separator to comma

                                    List<string> newLines = new List<string>();

                                    try
                                    {
                                        var lines = File.ReadLines(transferDirectoryAndFile);
                                        var aLine = "";

                                        // Do we need to add a header?
                                        // Check if we need to add a column header row to this file
                                        string manifestAddHeader = ConfigurationManager.AppSettings.Get("ManifestAddHeader");
                                        if (manifestAddHeader == "Yes")
                                        {
                                            newLines.Add(@"DateDelivered,Filename");
                                        }

                                        foreach (var line in lines)
                                        {
                                            // Manifest files use a colon to seperate fields, replace that with something sensible.
                                            // Space is essential as there are time fields that use colons neccessarily
                                            aLine = line.Replace(@": ", @",");
                                            newLines.Add(aLine);

                                        }

                                        using (StreamWriter sw = new StreamWriter(transferDirectoryAndFile))
                                        {
                                            foreach (string lineWrite in newLines)
                                            {
                                                sw.WriteLine(lineWrite);
                                            }
                                        }
                                    }

                                    catch (Exception ex)
                                    {
                                        writeLog(DateTime.Now, string.Format("MANIFEST - ERROR - problem during manifest file field change: {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);
                                    }
                                } // end if (error == false)

                                // Check if we need to add a column header row to this file
                                // string manifestAddHeader = ConfigurationManager.AppSettings.Get("ManifestAddHeader");

                                //if (manifestAddHeader == "Yes")
                                //{
                                //    // Read the existing file
                                //    string existingData;
                                //    string newData;


                                //    writeLog(DateTime.Now, string.Format("MANIFEST - adding header to manifest file: {0}", transferDirectoryAndFile), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode);

                                //    try
                                //    {
                                //        using (StreamReader sr = new StreamReader(transferDirectoryAndFile))
                                //        {
                                //            existingData = sr.ReadToEnd();
                                //        }

                                //        // Add header and write out to the same file
                                //        using (StreamWriter sw = new StreamWriter(transferDirectoryAndFile, false))
                                //        {
                                //            newData = @"DateDelivered : Filename" + Environment.NewLine + existingData;
                                //            sw.Write(newData);
                                //        }

                                //        writeLog(DateTime.Now, string.Format("MANIFEST - successfully added header to manifest file: {0}", transferDirectoryAndFile), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode);

                                //    }
                                //    catch (Exception ex)
                                //    {
                                //        writeLog(DateTime.Now, string.Format("MANIFEST - ERROR - problem during manifest file uplift: {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode);
                                //    }
                                //}

                            } // End ForEach Manifest

                            writeLog(DateTime.Now, string.Format("OBSERVER - successful move of manifest files."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                        }

                    }
                    catch
                    {
                        writeLog(DateTime.Now, string.Format("OBSERVER - ERROR - problem during processing of manifest files."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 2);
                    }


                    /////////////////////////////////////////////////////////// ZIPS ////////////////////////////////////////////////////////////////////////////


                    // Process Zips

                    // We know where to look from the config
                    // sourcePath = ooCon.transferToDirectory;

                    writeLog(DateTime.Now, string.Format("OBSERVER - processing zips."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 3);

                    sourcePath = ConfigurationManager.AppSettings.Get("TransferToDirectory");

                    // Gather a list of zips to check and unpack

                    zipFileList = Directory.GetFiles(sourcePath, "*.zip", SearchOption.AllDirectories);

                    if (zipFileList != null && zipFileList.Length != 0)
                    {
                        foreach (string zipFileFound in zipFileList)
                        {
                            writeLog(DateTime.Now, string.Format("Zip file {0} found", zipFileFound), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);
                        }
                    }
                    else
                    {
                        writeLog(DateTime.Now, string.Format("OBSERVER - No zips found to process."), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);
                    }

                    // Store the full directory, without the filename
                    directoryOnly = sourcePath; //Path.GetDirectoryName(sourcePath);

                    //Get a list of the subdirectories, ignoring the Archive and the Old direcories (at present this leaves NY and LDN)
                    var listOfSubDirectories = Directory.GetDirectories(directoryOnly)
                         .Where(d => !d.ToUpper().Contains("\\ARCHIVE") && !d.ToString().ToUpper().Contains("\\OLD"));

                    // Look into each remaining subdirectory.
                    // Determine which one we're currently dealing with
                    // Make provision to copy any subfolders to the corresponding Pending location
                    foreach (string subDir in listOfSubDirectories)
                    {
                        //MessageBox.Show(string.Format("Sub Dir found: {0}", subDir));

                        //locationDirectory = new DirectoryInfo(subDir).Name;
                        dataPendingDirectoryWorking = sourcePath;

                        // Ensure sub has files
                        if (!IsDirectoryEmpty(subDir))
                        {
                            //MessageBox.Show("Files found");

                            sourcePath = subDir; // (string)Dts.Variables["User::metadataFileUNCPathRoot"].Value;
                            destinationPath = dataPendingDirectoryWorking + "\\"; // (string)Dts.Variables["User::dataPendingDirectoryUNC"].Value;

                            zipFileList = Directory.GetFiles(sourcePath, "*.zip", SearchOption.AllDirectories);

                            // Are any files to unzip found?
                            if (zipFileList.Length != 0)
                            {

                                // Set a flag indicating files have been found to process. This will be sent back to the package and enable subsequent processing.
                                // If, by the end, no files have been found, the flag will still be false and can be used to halt processing.

                                // filesFoundToProcess = true;

                                // From the root archive directory given, search for all instances of a matching zip file (there should only ever be one)
                                // If there is more than one and it accepts the given password, the last archive extracted will be the one used.

                                foreach (string zipFile in zipFileList)
                                {

                                    // Capture parent directory for later archiving

                                    parentDirForDelivery = Directory.GetParent(zipFile).ToString();
                                    parentDirectory = new DirectoryInfo(parentDirForDelivery);
                                    parentDirectoryName = parentDirectory.Name;
                                    location = parentDirectory.Parent.Name;

                                    // Add a suffix in case of multiple deliveries on the same day with the same name.
                                    uniqueParentDirectoryName = string.Format(parentDirectoryName + "{0}", DateTime.Now.ToString("yyyyMMdd'T'HHmmss"));

                                    //txtOutput.AppendText(string.Format("Found {0}", zipFile) + Environment.NewLine);
                                    extractDirectory = Path.GetDirectoryName(zipFile) + "\\" + Path.GetFileNameWithoutExtension(zipFile) + "\\";
                                    extractDirectory = ConfigurationManager.AppSettings.Get("ExtractDirectoryAudio");
                                    extractDirectory = string.Format(@"{0}\{1}\{2}\{3}\", extractDirectory, location, parentDirectoryName, Path.GetFileNameWithoutExtension(zipFile));


                                    // Depending on the location, get the appropriate password
                                    try
                                    {
                                        writeLog(DateTime.Now, string.Format("TRANSFER - Retrieving the password for location {0}", location), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 7);
                                        unzipKey = RetrievePassword(location, ooCon.logDirectory, ooCon.loggingMode);
                                        writeLog(DateTime.Now, string.Format("TRANSFER - Password retrieved for location {0}", location), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 7);
                                    }
                                    catch (Exception ex)
                                    {
                                        writeLog(DateTime.Now, string.Format("TRANSFER - ERROR - problem retrieving the password for location {0}: {1}", location, ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                                        // Restart the timer
                                        timer.Start();
                                        return;
                                    }

                                    // Now using a method for archive app flexibility
                                    // TODO: File filter for 7 zip can be passed as a string - change separator

                                    // New from here....
                                    // Test the file
                                    // Test the integrity of the archive
                                    writeLog(DateTime.Now, string.Format("UNZIP - Testing archive {0}", zipFile), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);
                                    exitCode = processArchive(zipFile, extractDirectory, applicationLocation, unzipKey, applicationName, "T", "");
                                    writeLog(DateTime.Now, string.Format("UNZIP - Finished testing archive {0} - return code {1}", zipFile, exitCode), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);

                                    //Exit Code	Meaning for 7Zip
                                    //0	No error
                                    //1	Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not compressed.
                                    //2	Fatal error
                                    //7	Command line error
                                    //8	Not enough memory for operation
                                    //255	User stopped the process

                                    switch (exitCode)
                                    {
                                        case 0:
                                            exitCodeDescription = "Success";

                                            // Leave zip in place for any next pass of audio extraction, plus following metadata

                                            break;
                                        case 1:
                                            exitCodeDescription = "Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not processed.";
                                            break;
                                        case 2:
                                            exitCodeDescription = "Fatal error";
                                            break;
                                        case 7:
                                            exitCodeDescription = "Command line error";
                                            break;
                                        case 8:
                                            exitCodeDescription = "Not enough memory for operation";
                                            break;
                                        case 255:
                                            exitCodeDescription = "User stopped the process";
                                            break;
                                        default:
                                            exitCodeDescription = "Unknown conclusion";
                                            break;
                                    }

                                    // ==================================================================================================================
                                    // //////////////////////////////////////// Invalid Archive - Quarantine ////////////////////////////////////////////
                                    // ==================================================================================================================

                                    if (exitCode != 0)
                                    {
                                        // Error during test of the archives integrity - move this archive to quarantine
                                        writeLog(DateTime.Now, string.Format("UNZIP - archive {0} failed integrity check with exit code {1}", zipFile, exitCode), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 6);
                                        writeLog(DateTime.Now, string.Format("UNZIP - moving failed archive {0} to error folder", zipFile), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);

                                        string errorDirectory = ConfigurationManager.AppSettings.Get("ProcessedZipsErrorDirectory");
                                        bool moveSuccess = moveArchive(location, uniqueParentDirectoryName, zipFile, errorDirectory);

                                        if (moveSuccess)
                                        {
                                            writeLog(DateTime.Now, string.Format(@"UNZIP - archive {0} successfully moved to error location {1}\{2}\{3}", zipFile, errorDirectory, location, uniqueParentDirectoryName), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);
                                        }
                                        else
                                        {
                                            writeLog(DateTime.Now, string.Format(@"UNZIP - archive {0} failed to move to error location {1}\{2}\{3}", zipFile, errorDirectory, location, uniqueParentDirectoryName), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 6);
                                        }

                                        try
                                        {
                                            writeProcessInfo(zipFile, exitCode, exitCodeDescription, fileDeliveryId);
                                            writeLog(DateTime.Now, string.Format("WRITE PROCESS - Success"), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                                        }
                                        catch (Exception ex)
                                        {
                                            writeLog(DateTime.Now, string.Format("WRITE PROCESS - ERROR - {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                                        }

                                        // We cannot perform another pass on this same archive, so break the loop
                                        continue;
                                    }

                                    //////////////////////////////////////////////////// AUDIO ///////////////////////////////////////////////////////////////////////////////

                                    string audioFileTypes = ConfigurationManager.AppSettings.Get("AudioFileTypes");
                                    string[] audioFileTypesList = audioFileTypes.Split(';');

                                    // Tidy the array of empty / blank / null elements
                                    audioFileTypesList = audioFileTypesList.Where(t => !string.IsNullOrWhiteSpace(t)).ToArray();

                                    // Loop through every required audio file type from the config
                                    foreach (string specificAudioType in audioFileTypesList)
                                    {

                                        // Unpack the files
                                        writeLog(DateTime.Now, string.Format("UNZIP - Processing archive {0}", zipFile), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);
                                        exitCode = processArchive(zipFile, extractDirectory, applicationLocation, unzipKey, applicationName, "X", specificAudioType);
                                        writeLog(DateTime.Now, string.Format("UNZIP - Finished processing archive {0} - return code {1}", zipFile, exitCode), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);

                                        // Although the test above has been successful there has been an error during the unpacking (lack of disk space for example, that would not have been an issue during an integrity test).
                                        // Log this error
                                        switch (exitCode)
                                        {
                                            case 0:
                                                exitCodeDescription = "Success";

                                                // Leave zip in place for any next pass of audio extraction, plus following metadata

                                                // If successful, generate filelist
                                                Boolean fileListCreated = false;
                                                try
                                                {
                                                    // Generate the file list
                                                    string fileListPendingDirectory = ConfigurationManager.AppSettings.Get("FileListPending");

                                                    fileListPendingDirectory = string.Format(@"{0}\{1}\", fileListPendingDirectory, location);

                                                    writeLog(
                                                                DateTime.Now,
                                                                string.Format("TRANSFER - creating file list of zip audio contents for {0} - location {1}", fileListPendingDirectory, location)
                                                                , ooCon.logDirectory
                                                                , @"FileWatch.log"
                                                                , ooCon.loggingMode
                                                                , 8
                                                            );

                                                    fileListCreated = GenerateFileList(extractDirectory.Trim(), fileListPendingDirectory, ooCon.logDirectory, ooCon.loggingMode, fileDeliveryId);

                                                    writeLog(
                                                                DateTime.Now,
                                                                string.Format("TRANSFER - Successful file creation for {0} - location {1}", fileListPendingDirectory, location)
                                                                , ooCon.logDirectory
                                                                , @"FileWatch.log"
                                                                , ooCon.loggingMode
                                                                , 8
                                                            );

                                                }
                                                catch (Exception ex)
                                                {
                                                    writeLog(DateTime.Now, string.Format("TRANSFER - ERROR - problem during file list creation: {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                                                }

                                                break;
                                            case 1:
                                                exitCodeDescription = "Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not processed.";
                                                break;
                                            case 2:
                                                exitCodeDescription = "Fatal error";
                                                break;
                                            case 7:
                                                exitCodeDescription = "Command line error";
                                                break;
                                            case 8:
                                                exitCodeDescription = "Not enough memory for operation";
                                                break;
                                            case 255:
                                                exitCodeDescription = "User stopped the process";
                                                break;
                                            default:
                                                exitCodeDescription = "Unknown conclusion";
                                                break;
                                        }

                                        try
                                        {
                                            writeProcessInfo(zipFile, exitCode, exitCodeDescription, fileDeliveryId);
                                            writeLog(DateTime.Now, string.Format("WRITE PROCESS - Success"), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                                        }
                                        catch (Exception ex)
                                        {
                                            writeLog(DateTime.Now, string.Format("WRITE PROCESS - ERROR - {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                                        }

                                        writeLog(DateTime.Now, string.Format("Result of unzip for file {0} was code {1} - {2}", zipFile, exitCode.ToString(), exitCodeDescription), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);

                                        // Original: runBatchProcess.Close();

                                        //////////////////////////////////////////////////// END AUDIO ///////////////////////////////////////////////////////////////////////////////

                                    } // End ForEach of all file types


                                    // ====================================================================================================================
                                    // ////////////////////////////////////////////// METADATA ////////////////////////////////////////////////////////////
                                    // ====================================================================================================================

                                    string metadataFileTypes = ConfigurationManager.AppSettings.Get("MetadataFileTypes");
                                    string[] metadataFileTypesList = metadataFileTypes.Split(';');
                                    fileType = @"MetaData";

                                    // Tidy the array of empty / blank / null elements
                                    metadataFileTypesList = metadataFileTypesList.Where(t => !string.IsNullOrWhiteSpace(t)).ToArray();

                                    // Loop through every required audio file type from the config
                                    foreach (string specificMetadataType in metadataFileTypesList)
                                    {

                                        // Add a suffix in case of multiple deliveries on the same day with the same name.
                                        uniqueParentDirectoryName = string.Format(parentDirectoryName + "{0}", DateTime.Now.ToString("yyyyMMdd'T'HHmmss"));

                                        extractDirectory = Path.GetDirectoryName(zipFile) + "\\" + Path.GetFileNameWithoutExtension(zipFile) + "\\";
                                        extractDirectory = ConfigurationManager.AppSettings.Get("ExtractDirectoryMetadata");
                                        extractDirectory = string.Format(@"{0}\{1}\{2}\{3}\{4}\", extractDirectory, location, fileType, parentDirectoryName, Path.GetFileNameWithoutExtension(zipFile));

                                        // Unpack the files
                                        exitCode = processArchive(zipFile, extractDirectory, applicationLocation, unzipKey.Trim(), applicationName, "X", specificMetadataType);

                                        switch (exitCode)
                                        {
                                            case 0:
                                                exitCodeDescription = "Success";

                                                // Re-brand the metadata files to ensure they have the word "MetaData" in the filename
                                                List<string> metadataFiles = new List<string>();

                                                writeLog(DateTime.Now, string.Format(@"TRANSFER - Re-branding metadata files in {0}", extractDirectory), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);

                                                metadataFiles = Directory.GetFiles(extractDirectory, specificMetadataType, SearchOption.AllDirectories).ToList();

                                                try
                                                {

                                                    foreach (string metaDataFile in metadataFiles)
                                                    {

                                                        // Only prefix the file if it isn't already prefixed correctly
                                                        if (metaDataFile.Substring(0, 9) != "Metadata_")
                                                        {
                                                            string pathName = Path.GetDirectoryName(metaDataFile);
                                                            string metaDataFileNew = string.Format(@"Metadata_{0}", Path.GetFileName(metaDataFile));

                                                            writeLog(DateTime.Now, string.Format(@"TRANSFER - Re-branding metadata file {0} to {1}", metaDataFile, metaDataFileNew), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);

                                                            string newMetaFile = string.Format(@"{0}\{1}", pathName, metaDataFileNew);

                                                            // Remove any previous instances of a metadata file with this name
                                                            if (File.Exists(newMetaFile))
                                                            {
                                                                File.Delete(newMetaFile);
                                                            }

                                                            // Rename it
                                                            File.Move(metaDataFile, newMetaFile);

                                                            writeLog(DateTime.Now, string.Format(@"TRANSFER - Successfully re-branded metadata file {0} to {1}", metaDataFile, metaDataFileNew), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);

                                                            // Alter the header of the metadata file to remove spaces from the column headers
                                                            headerFixed = FixHeader(newMetaFile, ooCon.logDirectory, ooCon.loggingMode);
                                                        }
                                                        else
                                                        {
                                                            // Alter the header of the metadata file to remove spaces from the column headers
                                                            headerFixed = FixHeader(metaDataFile, ooCon.logDirectory, ooCon.loggingMode);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    writeLog(DateTime.Now, string.Format(@"TRANSFER - Error re-branding metadata file: {0}", ex.Message), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                                                }

                                                // Finished now - move the zip file to the Processed directory

                                                bool moveSuccess = moveArchive(location, uniqueParentDirectoryName, zipFile, processedZipsSuccessDirectory);

                                                if (moveSuccess)
                                                {
                                                    writeLog(DateTime.Now, string.Format(@"UNZIP - METADATA - archive {0} successfully moved to success location {1}\{2}\{3}", zipFile, processedZipsSuccessDirectory, location, uniqueParentDirectoryName), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                                                }
                                                else
                                                {
                                                    writeLog(DateTime.Now, string.Format(@"UNZIP - METADATA - archive {0} failed to move to success location {1}\{2}\{3}", zipFile, processedZipsSuccessDirectory, location, uniqueParentDirectoryName), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                                                }

                                                break;
                                            case 1:
                                                exitCodeDescription = "Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not processed.";
                                                break;
                                            case 2:
                                                exitCodeDescription = "Fatal error";
                                                break;
                                            case 7:
                                                exitCodeDescription = "Command line error";
                                                break;
                                            case 8:
                                                exitCodeDescription = "Not enough memory for operation";
                                                break;
                                            case 255:
                                                exitCodeDescription = "User stopped the process";
                                                break;
                                            default:
                                                exitCodeDescription = "Unknown conclusion";
                                                break;
                                        }

                                        writeProcessInfo(zipFile, exitCode, exitCodeDescription, fileDeliveryId);

                                        writeLog(DateTime.Now, string.Format("Result of unzip for file {0} was code {1} - {2}", zipFile, exitCode.ToString(), exitCodeDescription), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 5);

                                        //////////////////////////////////////////////////// END METADATA ///////////////////////////////////////////////////////////////////////////////

                                    } // End ForEach zip file

                                } // End ForEach zip file

                            } // End if zips found

                        } // End If directory empty

                    } // For each location loop

                } // End if start transfer true check

                else
                {
                    writeLog(DateTime.Now, "Start transfer returned false", ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                }

                // Resume the timer
                writeLog(DateTime.Now, "Restarting timer", ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                timer.Start();

            }
            else
            {

                // Not breach but notified of changes

                if (oChecks)
                {
                    writeLog(DateTime.Now, string.Format("OBSERVER - changes found, performing actions then sleeping for {0} millisecond(s) or until action detected in folder or until non-activity threshold is reached.", ooCon.timerInterval), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                }
                else
                {
                    writeLog(DateTime.Now, string.Format("OBSERVER - no changes found - sleeping for {0} millisecond(s) or until action detected in folder or until non-activity threshold is reached.", ooCon.timerInterval), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);

                    // Housekeeping
                    try
                    {
                        writeLog(DateTime.Now, "Checking housekeeping (empty folders)", ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                        string tidyRoot = ConfigurationManager.AppSettings.Get("TransferToDirectory");
                        removeEmptyFolders(tidyRoot, ooCon);


                        tidyRoot = ConfigurationManager.AppSettings.Get("WatchDirectory");
                        removeEmptyFolders(tidyRoot, ooCon);
                        writeLog(DateTime.Now, "Checking housekeeping (empty folders) - complete", ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 1);
                    }
                    catch (Exception ex)
                    {
                        writeLog(DateTime.Now, string.Format("HOUSEKEEPING - error removing empty directories: {0}", ex), ooCon.logDirectory, @"FileWatch.log", ooCon.loggingMode, 99);
                    }

                }
            }
        }

        public static string RetrievePassword(string location, string logDirectory, string loggingMode)
        {
            // Get the correct, currently active password for the select location

            string password = "";
            // string sqlConnection = @"Persist Security Info=False;Integrated Security=true;Initial Catalog=Audio2_Metadata;server=vdbstrs01p001\isql01"; // TODO: config driven
            string sqlConnection = ConfigurationManager.AppSettings.Get("DBConnection"); //@"Persist Security Info=False;Integrated Security=true;Initial Catalog=Voice_DEV;server=VDBSTRS01P001\ISQL01"; // TODO: config driven

            try
            {

                using (SqlConnection sqlConn = new SqlConnection(sqlConnection))
                {

                    sqlConn.Open();

                    using (SqlCommand sqlComm = new SqlCommand("ETL.getKeyValueForUnzip", sqlConn))
                    {
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.AddWithValue("@location", location);

                        SqlParameter sqlParamOut = new SqlParameter();
                        sqlParamOut.Direction = ParameterDirection.Output;
                        sqlParamOut.ParameterName = "@keyValue";
                        sqlParamOut.SqlDbType = SqlDbType.NVarChar;
                        sqlParamOut.Size = 500;

                        // Add the parameter
                        sqlComm.Parameters.Add(sqlParamOut);

                        sqlComm.ExecuteNonQuery();

                        password = (string)sqlParamOut.Value;

                    }
                }
            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("PASSWORD RETRIEVAL - ERROR: {0}", ex.Message), logDirectory, @"FileWatch.log", loggingMode, 99);
                password = string.Empty;
            }
            return password;
        }

        public static long GetFileDeliveryId(string logDirectory, string loggingMode)
        {
            try
            {
                string DBConnection = ConfigurationManager.AppSettings.Get("DBConnection");
                SqlConnection conn = new SqlConnection(DBConnection);

                using (SqlCommand comm = new SqlCommand("dbo.getNextFileDeliveryGroupId", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.Parameters.Add("@fileDeliveryGroupId", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                    conn.Open();
                    comm.ExecuteNonQuery();

                    long fileDeliveryId = Convert.ToInt64(comm.Parameters["@fileDeliveryGroupId"].Value);

                    return fileDeliveryId;

                }

            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("FILE DELIVERY ID - ERROR: {0}", ex.Message), logDirectory, @"FileWatch.log", loggingMode, 99);
                return -1;
            }

            // throw new NotImplementedException();
        }

        public static Boolean FixHeader(string fileToFix, string logDirectory, string loggingMode)
        {
            List<string> metadataLines = new List<string>();
            string readLine = "";
            // string allData = "";
            // string newFile = "";
            string tempOutput = "";
            int lineRead = 1;
            tempOutput = string.Format(@"{0}.tmp", fileToFix);

            // Clear up any previous
            if (File.Exists(tempOutput))
            {
                File.Delete(tempOutput);
            }

            StreamWriter sw = new StreamWriter(tempOutput, true, System.Text.Encoding.GetEncoding(1252));

            try
            {

                using (StreamReader sr = new StreamReader(fileToFix, System.Text.Encoding.ASCII, false, 4096))
                {

                    while (!sr.EndOfStream)
                    {

                        // Get first line - this must be the header row
                        readLine = sr.ReadLine();

                        // Only make alterations to the first line
                        if (lineRead == 1)
                        {
                            readLine = readLine.Replace(@" ", @""); // Get rid of the spaces
                            readLine = readLine.Replace(@"OriginalID,iInteractionID,iContactID", @"OriginalID,iInteractionID2,iContactID"); // For trunk labels, fix the repeated field name issue for iInteractionID, making the second instance iInteractionID2. We target that instance by bookending with parts of the neighbouring fields. If these change, this will not work.
                            lineRead = 0;
                        }

                        if (sw != null)
                        {
                            sw.WriteLine(readLine);
                        }

                    }

                    sw.Close();

                }


                // Done - rename the tmp version to the original
                if (File.Exists(fileToFix))
                {
                    File.Delete(fileToFix);
                }
                File.Move(tempOutput, fileToFix);


                // Write the data out
                //using (StreamWriter sw = new StreamWriter(fileToFix, false))
                //{
                //    sw.WriteLine(newFile);
                //}

                writeLog(DateTime.Now, string.Format("HEADER FIX - SUCCESS - header for file {0} was fixed", fileToFix), logDirectory, @"FileWatch.log", loggingMode, 1);
                return true;
            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("HEADER FIX - ERROR - problem fixing header: {0}", ex.Message), logDirectory, @"FileWatch.log", loggingMode, 1);
                return false;
            }
        }

        public static void removeEmptyFolders(string rootToCheck, Observer oCon)
        {

            // Moved files can leave behind empty folders - remove these

            var listOfHeaderDirectories = Directory.GetDirectories(rootToCheck);

            foreach (string subDir in listOfHeaderDirectories)
            {
                // we don't want to remove the header dir (LDN, NYC etc), just empty folders under that
                var listOfSubDirectories = Directory.GetDirectories(subDir);
                foreach (string checkEmpty in listOfSubDirectories)
                {
                    try
                    {
                        if (IsDirectoryEmpty(checkEmpty))
                        {
                            // Found to be empty, remove
                            writeLog(DateTime.Now, string.Format(@"Empty directory {0} found - removing", checkEmpty), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 1);
                            Directory.Delete(checkEmpty);
                        }
                    }
                    catch (Exception ex)
                    {
                        writeLog(DateTime.Now, string.Format("TRANSFER - ERROR - problem during post move tidy up: {0}", ex.Message), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 99);
                    }

                }
            }
        }

        public static void writeDelivery(FileInfo fileToTransfer, string transferDirectory)
        {

            // Get the details we want to capture from the passed along file

            string path = Path.GetDirectoryName(fileToTransfer.FullName.ToString());
            string fileName = Path.GetFileName(fileToTransfer.ToString());
            string location;
            string dayOfDelivery;
            long fileSize;
            DateTime dateDeliveredToSFTP = File.GetCreationTime(fileToTransfer.FullName.ToString());
            DateTime dateTransferredForProcessing = DateTime.Now;

            DirectoryInfo locationDetails;

            fileSize = fileToTransfer.Length;

            // Get location - relies on standard delivery folder structure (e.g. SFTP\Location\DateOfdelvery
            locationDetails = new DirectoryInfo(path);
            dayOfDelivery = locationDetails.Name;
            location = locationDetails.Parent.Name;

            // string sqlConnection = @"Persist Security Info=False;Integrated Security=true;Initial Catalog=Voice_DEV;server=VDBSTRS01P001\ISQL01"; // TODO: config driven
            string sqlConnection = ConfigurationManager.AppSettings.Get("DBConnection"); //@"Persist Security Info=False;Integrated Security=true;Initial Catalog=Voice_DEV;server=VDBSTRS01P001\ISQL01"; // TODO: config driven

            using (SqlConnection sqlConn = new SqlConnection(sqlConnection))
            {

                sqlConn.Open();

                using (SqlCommand sqlComm = new SqlCommand("ETL.FileDeliveryCaptureInfo", sqlConn))
                {
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@deliveryPath", path);
                    sqlComm.Parameters.AddWithValue("@deliveryFileName", fileName);
                    sqlComm.Parameters.AddWithValue("@deliveryFileSize", fileSize);
                    sqlComm.Parameters.AddWithValue("@transferPath", transferDirectory);
                    sqlComm.Parameters.AddWithValue("@dateDeliveredToSFTP", dateDeliveredToSFTP);
                    sqlComm.Parameters.AddWithValue("@dateTransferredForProcessing", dateTransferredForProcessing);
                    sqlComm.Parameters.AddWithValue("@location", location);

                    sqlComm.ExecuteNonQuery();

                }
            }

        }

        public static bool observerChecks(Observer oCon)
        {

            writeLog(DateTime.Now, string.Format("{0} Performing timed checks", DateTime.Now.ToLongTimeString()), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 1);

            // Safe here to store details of the files as, even if created, the Changed method is fired (several times!)

            DirectoryInfo di = new DirectoryInfo(oCon.pathToWatch);
            oCon.filesInDir = di.GetFiles("*.*", SearchOption.AllDirectories);

            // If a fresh snapshot, store the comparison set
            if (oCon.filesInDirComparisonSet == null || oCon.filesInDirComparisonSet.Length == 0)
            {
                oCon.filesInDirComparisonSet = oCon.filesInDir;
            }

            // Inital check - file count
            if (oCon.filesInDir.Count() != oCon.filesInDirComparisonSet.Count())
            {
                writeLog(DateTime.Now, string.Format("File count has changed  - next checks"), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 1);

                oCon.hasChangedCount = true;
            }
            else
            {
                oCon.hasChangedCount = false;
            }

            // Get the max Last Access datetime value from the current file list

            foreach (FileInfo fiMaxLastAccessTime in oCon.filesInDir)
            {
                if (fiMaxLastAccessTime.LastAccessTime > oCon.filesInDirMaxLastAccessTime)
                {
                    oCon.filesInDirMaxLastAccessTime = fiMaxLastAccessTime.LastAccessTime;

                    // At least one has changed, so exit the loop
                    break;
                }
            }

            // Now compare against the max Last Access datetime value from the stored file list

            foreach (FileInfo fiMaxLastAccessTimeinDirComp in oCon.filesInDirComparisonSet)
            {
                if (fiMaxLastAccessTimeinDirComp.LastAccessTime > oCon.filesInDirComparisonSetMaxLastAccessTime)
                {
                    oCon.filesInDirComparisonSetMaxLastAccessTime = fiMaxLastAccessTimeinDirComp.LastAccessTime;
                    // At least one has changed, so exit the loop
                    break;
                }
            }

            // Compare the winners of the two loops
            if (oCon.filesInDirMaxLastAccessTime > oCon.filesInDirComparisonSetMaxLastAccessTime)
            {
                // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                oCon.hasChangedLastAccessTime = true;

                // // uc these writeLog(DateTime.Now, string.Format("{0} - Latest File Last Access Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToLongTimeString(), e.FullPath, filesInDirMaxLastAccessTime, filesInDirComparisonSetMaxLastAccessTime), logDirectory, @"FileWatch.log");
                writeLog(DateTime.Now, string.Format("{0} - Latest File Last Access Time has changed from stored value of {1}", DateTime.Now.ToLongTimeString(), oCon.filesInDirComparisonSetMaxLastAccessTime), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 1);
            }
            else
            {
                // Turn the flag of if nothing has happened
                oCon.hasChangedLastAccessTime = false;
            }





            // Get the max Last WRITE datetime value from the current file list

            foreach (FileInfo fiMaxLastWriteTime in oCon.filesInDir)
            {
                if (fiMaxLastWriteTime.LastWriteTime > oCon.filesInDirMaxLastWriteTime)
                {
                    oCon.filesInDirMaxLastWriteTime = fiMaxLastWriteTime.LastWriteTime;
                    // At least one has changed, so exit the loop
                    break;
                }
            }

            // Now compare against the max Last Access datetime value from the stored file list
            // DateTime test = DateTime.MinValue;
            foreach (FileInfo fiMaxLastWriteTimeinDirComp in oCon.filesInDirComparisonSet)
            {
                if (fiMaxLastWriteTimeinDirComp.LastWriteTime > oCon.filesInDirComparisonSetMaxLastWriteTime)
                {
                    oCon.filesInDirComparisonSetMaxLastWriteTime = fiMaxLastWriteTimeinDirComp.LastWriteTime;
                    // At least one has changed, so exit the loop
                    break;
                }
            }

            // Compare the winners of the two loops
            if (oCon.filesInDirMaxLastWriteTime > oCon.filesInDirComparisonSetMaxLastWriteTime)
            {
                // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                oCon.hasChangedLastWriteTime = true;
                oCon.filesInDirComparisonSetMaxLastWriteTime = oCon.filesInDirMaxLastWriteTime;
                // // uc these writeLog(DateTime.Now, string.Format("{0} - Latest File Last Write Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToLongTimeString(), e.FullPath, filesInDirMaxLastWriteTime, filesInDirComparisonSetMaxLastWriteTime), logDirectory, @"FileWatch.log");
                writeLog(DateTime.Now, string.Format("{0} - Latest File Last Write Time has changed from stored value of {1}", DateTime.Now.ToLongTimeString(), oCon.filesInDirComparisonSetMaxLastWriteTime), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 1);
            }
            else
            {
                // Turn the flag of if nothin has happened
                oCon.hasChangedLastWriteTime = false;
            }


            // Get the max Last CREATE datetime value from the current file list

            foreach (FileInfo fiMaxLastCreateTime in oCon.filesInDir)
            {
                if (fiMaxLastCreateTime.CreationTime > oCon.filesInDirMaxLastCreateTime)
                {
                    oCon.filesInDirMaxLastCreateTime = fiMaxLastCreateTime.CreationTime;
                    // At least one has changed, so exit the loop
                    break;
                }
            }

            // Now compare against the max Last CREATE datetime value from the stored file list

            foreach (FileInfo fiMaxLastCreateTimeinDirComp in oCon.filesInDirComparisonSet)
            {
                if (fiMaxLastCreateTimeinDirComp.CreationTime > oCon.filesInDirComparisonSetMaxLastCreateTime)
                {
                    oCon.filesInDirComparisonSetMaxLastCreateTime = fiMaxLastCreateTimeinDirComp.CreationTime;
                    // At least one has changed, so exit the loop
                    break;
                }
            }

            // Compare the winners of the two loops
            if (oCon.filesInDirMaxLastCreateTime > oCon.filesInDirComparisonSetMaxLastCreateTime)
            {
                // The max last access time from the current snapshot is greater than the max last access from the stored snapshot
                oCon.hasChangedLastCreateTime = true;

                // writeLog(DateTime.Now, string.Format("{0} - Latest File Last Write Time for file {1} of {2} has changed from stored value of {3}", DateTime.Now.ToLongTimeString(), e.FullPath, filesInDirMaxLastWriteTime, filesInDirComparisonSetMaxLastWriteTime), logDirectory, @"FileWatch.log");
                writeLog(DateTime.Now, string.Format("{0} - Latest File Last Create Time has changed from stored value of {1}"
                    , DateTime.Now.ToLongTimeString()
                    , oCon.filesInDirComparisonSetMaxLastCreateTime)
                    , oCon.logDirectory
                    , @"FileWatch.log"
                    , oCon.loggingMode
                    , 1);
            }
            else
            {
                // Turn the flag of if nothing has happened
                oCon.hasChangedLastCreateTime = false;
            }


            // Reset the threshold breach
            oCon.lastActionThresholdBreach = false;

            bool doneAnything = false;

            doneAnything = (oCon.hasChangedCount || oCon.hasChangedLastAccessTime || oCon.hasChangedLastWriteTime || oCon.hasChangedLastCreateTime);

            // Check if any action has been taken on the file list

            if (doneAnything)
            {
                // writeLog(DateTime.Now, string.Format("Activity detected since last timer poll - continue", DateTime.Now.ToLongTimeString(), e.FullPath, filesInDirMaxLastAccessTime, filesInDirComparisonSetMaxLastAccessTime), logDirectory, @"FileWatch.log");
                writeLog(DateTime.Now, string.Format("OBSERVER - Activity detected since last timer poll - continue"), oCon.logDirectory, @"FileWatch.log", oCon.loggingMode, 1);

                // Change in the situation is noted. Set the comparison set to be the new reality.

                oCon.filesInDirComparisonSet = oCon.filesInDir;
                oCon.filesInDirComparisonSetMaxLastCreateTime = oCon.filesInDirMaxLastCreateTime;
                oCon.filesInDirComparisonSetMaxLastAccessTime = oCon.filesInDirMaxLastAccessTime;
                oCon.filesInDirComparisonSetMaxLastWriteTime = oCon.filesInDirMaxLastWriteTime;

                // Threshold breach will remain false
            }
            else
            {
                // Threshold breach needs to be checked. Basically no actions have been identified on files in the folder during this observation
                // so check if the datetime of the last action for the maxium datetime file was less than the threshold

                TimeSpan maxLastWriteTimeSpan = DateTime.Now - oCon.filesInDirMaxLastWriteTime;
                TimeSpan maxLastAccessTimeSpan = DateTime.Now - oCon.filesInDirMaxLastAccessTime;
                TimeSpan maxLastCreateTimeSpan = DateTime.Now - oCon.filesInDirMaxLastCreateTime;

                // Choose the winner from these hi scores and check if the action was more recent than the threshold.

                double maxLastWriteTimeTotalMilliseconds = maxLastWriteTimeSpan.TotalMilliseconds;
                double maxLastAccessTimeTotalMilliseconds = maxLastAccessTimeSpan.TotalMilliseconds;
                double maxLastCreateTimeTotalMilliseconds = maxLastCreateTimeSpan.TotalMilliseconds;

                double nuMin = Min(maxLastWriteTimeTotalMilliseconds, maxLastAccessTimeTotalMilliseconds, maxLastCreateTimeTotalMilliseconds);

                // Find the minimum time stored and see if it breaches threshold.
                if (nuMin >= Convert.ToDouble(oCon.lastActionThreshold) && oCon.filesInDir.Length != 0)
                {
                    writeLog(DateTime.Now, string.Format("OBSERVER - last action on the directory was {0} millisecond(s) ago. Action can now be taken.", nuMin.ToString()), oCon.logDirectory, @"FileWatch.log"
                    , oCon.loggingMode, 3);

                    oCon.lastActionThresholdBreach = true;

                    // Threshold breached - constitutes action
                    doneAnything = true;
                }

            }

            return doneAnything;
        }

        public static bool transferFiles(Observer tranOCon)
        {

            bool additionalDirectoriesRequired = true; // TODO: Change this appconfig
            bool successfulTransfer = false;
            DirectoryInfo parentDetails;
            int fileCount = 0;
            string parent = "";
            string grandParent = "";
            string additionalDirectory;

            try
            {

                // In the current given set, transfer each file to the destination directory
                foreach (FileInfo fileToTransfer in tranOCon.filesInDir)
                {

                    // Work out the extra directories required
                    if (additionalDirectoriesRequired)
                    {
                        additionalDirectory = Directory.GetParent(fileToTransfer.FullName.ToString()).ToString();
                        parentDetails = new DirectoryInfo(additionalDirectory);
                        parent = parentDetails.Name;
                        grandParent = parentDetails.Parent.Name;
                    }

                    // movOCon.moveToDirectory = string.Format(@"{0}\{1}\{2}", movOCon.moveToDirectory, parent, grandParent);
                    tranOCon.transferToDirectory = string.Format(@"{0}\{1}\{2}", ConfigurationManager.AppSettings.Get("TransferToDirectory"), grandParent, parent);

                    // Log the delivery details to SQL
                    writeDelivery(fileToTransfer, tranOCon.transferToDirectory);


                    // =============== Move =================

                    if (tranOCon.transferMode == "Move")
                    {
                        writeLog(DateTime.Now, string.Format("MOVE - Attempting move of file {0} to folder {1}", fileToTransfer.FullName, tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log"
                                            , tranOCon.loggingMode, 10);

                        if (!Directory.Exists(tranOCon.transferToDirectory))
                        {
                            Directory.CreateDirectory(tranOCon.transferToDirectory);
                            writeLog(DateTime.Now, string.Format("MOVE - Transfer destination directory {0} was created.", tranOCon.transferToDirectory)
                                , tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 10);
                        }

                        if (File.Exists(string.Format(@"{0}\{1}", tranOCon.transferToDirectory, fileToTransfer.Name)))
                        {
                            File.Delete(string.Format(@"{0}\{1}", tranOCon.transferToDirectory, fileToTransfer.Name));
                            writeLog(DateTime.Now, string.Format("MOVE - File {0} found in folder {1} - file at destination was deleted."
                                , fileToTransfer.FullName, tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 10);
                        }

                        // Check if the file still exists - situation may have changed during the pause
                        if (File.Exists(fileToTransfer.FullName))
                        {
                            File.Move(fileToTransfer.FullName, string.Format(@"{0}\{1}", tranOCon.transferToDirectory, fileToTransfer.Name));
                            writeLog(DateTime.Now, string.Format("MOVE - Successful move of file {0} to folder {1}", fileToTransfer.FullName, tranOCon.transferToDirectory)
                                , tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 10);
                        }
                        else
                        {
                            writeLog(DateTime.Now, string.Format("MOVE - Unsuccessful move of file {0} to folder {1} - file was no longer found in source location."
                                , fileToTransfer.FullName, tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 10);
                        }

                    }


                    // =============== Copy =================

                    if (tranOCon.transferMode == "Copy")
                    {
                        writeLog(DateTime.Now, string.Format("COPY - Attempting copy of file {0} to folder {1}", fileToTransfer.FullName, tranOCon.transferToDirectory)
                            , tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 11);

                        if (!Directory.Exists(tranOCon.transferToDirectory))
                        {
                            Directory.CreateDirectory(tranOCon.transferToDirectory);
                            writeLog(DateTime.Now, string.Format("COPY - Source directory {0} was created.", tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 11);
                        }

                        if (File.Exists(string.Format(@"{0}\{1}", tranOCon.transferToDirectory, fileToTransfer.Name)))
                        {
                            File.Delete(string.Format(@"{0}\{1}", tranOCon.transferToDirectory, fileToTransfer.Name));
                            writeLog(DateTime.Now, string.Format("COPY - File {0} found in folder {1} - file at destination was deleted.", fileToTransfer.FullName
                                , tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 11);
                        }

                        // Check if the file still exists - situation may have changed during the pause
                        if (File.Exists(fileToTransfer.FullName))
                        {
                            File.Copy(fileToTransfer.FullName, string.Format(@"{0}\{1}", tranOCon.transferToDirectory, fileToTransfer.Name));
                            writeLog(DateTime.Now, string.Format("COPY - Successful copy of file {0} to folder {1}", fileToTransfer.FullName, tranOCon.transferToDirectory)
                                , tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 11);
                        }
                        else
                        {
                            writeLog(DateTime.Now, string.Format("COPY - Unsuccessful copy of file {0} to folder {1} - file was no longer found in source location.", fileToTransfer.FullName
                                , tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 11);
                        }

                    }

                    if (tranOCon.transferMode == "ReportOnly")
                    {

                        // For testing, include any pause

                        writeLog(DateTime.Now, string.Format("REPORT ONLY - An attempt would be made to transfer file {0} to folder {1}", fileToTransfer.FullName
                            , tranOCon.transferToDirectory), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 12);

                        if (tranOCon.pauseTransfer == true)
                        {
                            fileCount += 1;

                            if (fileCount == tranOCon.pauseTransferFilesPerBatch || tranOCon.pauseTransferFilesPerBatch == 0)
                            {
                                writeLog(DateTime.Now, string.Format("Pausing for {0} second(s) before resuming...", tranOCon.pauseTransferDelayBetweenBatches.ToString())
                                    , tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 12);
                                System.Threading.Thread.Sleep(tranOCon.pauseTransferDelayBetweenBatches);
                                fileCount = 0;
                            }
                        }

                    }
                }

                // Suspend this for now
                //if (tranOCon.transferMode == "Move" || tranOCon.transferMode == "Copy")
                //{
                //    // Create the CTL file
                //    File.Create(string.Format(@"{0}\{1}", tranOCon.transferToDirectory, "PACKET.CTL"));
                //}

                // Refresh the file lists
                DirectoryInfo di = new DirectoryInfo(tranOCon.pathToWatch);
                tranOCon.filesInDir = di.GetFiles("*.*", SearchOption.AllDirectories);
                tranOCon.filesInDirComparisonSet = tranOCon.filesInDir;

                successfulTransfer = true;
            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("TRANSFER - Failed transfer of file. Error {0} ", ex.Message), tranOCon.logDirectory, @"FileWatch.log", tranOCon.loggingMode, 99);

                successfulTransfer = false;
            }

            return successfulTransfer;
        }

        public static double Min(params double[] numbers)
        {
            return numbers.Min();
        }

        private static bool moveArchive(string location, string uniqueParentDirectoryName, string zipFile, string errorDirectory)
        {
            try

            {
                string a;
                string b;
                string[] currentZipFiles;

                string moveZipTo = string.Format(@"{0}\Zips\{1}\{2}", errorDirectory, location, uniqueParentDirectoryName);

                // Check if this directory already exists. If it does not, create it.
                if (!Directory.Exists(moveZipTo))
                {
                    new System.IO.FileInfo(moveZipTo + "\\").Directory.Create();
                }

                // Move all files related to the zip being processed in case this is multi-part.

                a = Path.GetDirectoryName(zipFile);
                b = Path.GetFileName(zipFile).Replace(".zip", ".z*");

                currentZipFiles = Directory.GetFiles(a, b);

                foreach (string moveZip in currentZipFiles)
                {
                    if (File.Exists(moveZipTo + "\\" + Path.GetFileName(moveZip)))
                    {
                        File.Delete(moveZipTo + "\\" + Path.GetFileName(moveZip));
                    }
                    File.Move(moveZip, moveZipTo + "\\" + Path.GetFileName(moveZip));
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void writeProcessInfo(string fileBeingProcessed, int exitCode, string exitDescription, long fileDeliveryId)
        {

            // Get the details we want to capture from the passed along file

            string processPath = Path.GetDirectoryName(fileBeingProcessed);
            string processFilename = Path.GetFileName(fileBeingProcessed);
            DateTime dateDeliveredToSFTP = File.GetCreationTime(fileBeingProcessed);
            DateTime processDate = DateTime.Now;
            Boolean processOutcome = false;
            Boolean communicationSent = false;

            if (exitCode == 0)
            {
                processOutcome = true;
            }
            else
            {
                processOutcome = false;
            }

            // TODO: Place in app.config
            string sqlConnection = ConfigurationManager.AppSettings.Get("DBConnection");
            // string sqlConnection = @"Persist Security Info=False;Integrated Security=true;Initial Catalog=Voice_DEV;server=VDBSTRS01P001\ISQL01"; // TODO: config driven

            using (SqlConnection sqlConn = new SqlConnection(sqlConnection))
            {

                sqlConn.Open();

                using (SqlCommand sqlComm = new SqlCommand("ETL.FileProcessInfo", sqlConn))
                {
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@processPath", processPath);
                    sqlComm.Parameters.AddWithValue("@processFilename", processFilename);
                    sqlComm.Parameters.AddWithValue("@processedDate", processDate);
                    sqlComm.Parameters.AddWithValue("@processExitCode", exitCode);
                    sqlComm.Parameters.AddWithValue("@processExitMessage", exitDescription);
                    sqlComm.Parameters.AddWithValue("@processOutcome", processOutcome);
                    sqlComm.Parameters.AddWithValue("@communicationSent", communicationSent);
                    sqlComm.Parameters.AddWithValue("@fileDeliveryId", fileDeliveryId);

                    sqlComm.ExecuteNonQuery();

                }
            }

        }

        private static Boolean GenerateFileList(string lookHere, string createHere, string logDirectory, string logMode, long fileDeliveryId)
        {
            // If successful, generate a file list of the audio just extracted and write it to the filelist pending direcory

            List<string> mediaFiles = new List<string>();

            string fileListPendingDirectory = string.Format(@"{0}{1}", createHere, @"FileList");

            //Directory.Move(audioSource + audioDirectoryName, audioDestination + audioDirectoryUnique);

            // Generate a file list text file of the copied directory

            mediaFiles.Clear();
            // mediaFiles = Directory.GetFiles(audioDestination + "\\" + locationDirectory + "\\" + grandParentDirectoryName + "\\" + parentDirectoryName + "\\", "*.NMF", SearchOption.AllDirectories).ToList();

            // TODO - make NMF dynamic as per config list
            mediaFiles = Directory.GetFiles(lookHere, "*.NMF", SearchOption.AllDirectories).ToList();
            string directoryName;
            string fileName;

            //txtStatus.Text = "Processing...";

            // Generate a text file listing the media files' directories and the filename.
            // This will be imported into SQL at a later stage and used to pinpoint where a file resides
            // so that it can be copied for Nexidia import.

            string outputFile;
            // outputFile = fileListPendingDirectory + locationDirectory + "\\" + string.Format("FileList{0}.txt", DateTime.Now.ToString("yyyyMMdd'T'HHmmss"));
            outputFile = fileListPendingDirectory + "\\" + string.Format("FileList{0}.txt", DateTime.Now.ToString("yyyyMMdd'T'HHmmss"));

            // if (!Directory.Exists(fileListPendingDirectory + "\\" + locationDirectory))
            if (!Directory.Exists(fileListPendingDirectory))
            {
                // Directory.CreateDirectory(fileListPendingDirectory + "\\" + locationDirectory);
                Directory.CreateDirectory(fileListPendingDirectory);
            }

            try
            {

                System.IO.StreamWriter fileListWrite = new System.IO.StreamWriter(outputFile, false, System.Text.Encoding.GetEncoding(1252));

                // Write header
                fileListWrite.WriteLine(string.Format("{0}|{1}|{2}|{3}", @"Directory", @"Filename", @"FileSize", @"FileDeliveryId"));

                FileInfo getFileInfo;
                string getFileSize;

                foreach (string mediaFile in mediaFiles)
                {
                    directoryName = Path.GetDirectoryName(mediaFile);
                    fileName = Path.GetFileName(mediaFile);

                    getFileInfo = new FileInfo(mediaFile);

                    getFileSize = getFileInfo.Length.ToString();

                    fileListWrite.WriteLine(string.Format("\"{0}\"|\"{1}\"|{2}|{3}", directoryName, fileName, getFileSize, fileDeliveryId.ToString()));

                }

                fileListWrite.Close();

                return true;
            }
            catch (Exception ex)
            {
                writeLog(DateTime.Now, string.Format("OBSERVER - ERROR - problem during generation of file list: {0}", ex.Message), logDirectory, @"FileWatch.log", logMode, 99);
                return false;
            }


        }

    }
}
