﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDOLs
{
    public class Observer
    {
        public Boolean lastActionThresholdBreach;
        public string pathToWatch;
        public FileInfo[] filesInDir;
        public FileInfo[] filesInDirComparisonSet;
        public Boolean hasChangedCount;
        public DateTime filesInDirMaxLastAccessTime;
        public DateTime filesInDirComparisonSetMaxLastAccessTime;
        public DateTime filesInDirMaxLastWriteTime;
        public DateTime filesInDirComparisonSetMaxLastWriteTime;
        public DateTime filesInDirMaxLastCreateTime;
        public DateTime filesInDirComparisonSetMaxLastCreateTime;
        public Boolean hasChangedLastAccessTime;
        public Boolean hasChangedLastCreateTime;
        public Boolean hasChangedLastWriteTime;
        public string lastActionThreshold;
        public string transferToDirectory;
        public string timerInterval;
        public string loggingMode;
        public string transferMode;
        public string logDirectory;
        public Boolean pauseTransfer;
        public int pauseTransferFilesPerBatch;
        public int pauseTransferDelayBetweenBatches;
    }
}
