﻿namespace CDOLs
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClientDataDeliveryObserveAndLoadInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ClientDataDeliveryObserveAndLoad = new System.ServiceProcess.ServiceInstaller();
            // 
            // ClientDataDeliveryObserveAndLoadInstaller
            // 
            this.ClientDataDeliveryObserveAndLoadInstaller.Password = null;
            this.ClientDataDeliveryObserveAndLoadInstaller.Username = null;
            // 
            // ClientDataDeliveryObserveAndLoad
            // 
            this.ClientDataDeliveryObserveAndLoad.Description = "Client Data Delivery Observe and Load";
            this.ClientDataDeliveryObserveAndLoad.DisplayName = "ClientDataDeliveryObserveLoad";
            this.ClientDataDeliveryObserveAndLoad.ServiceName = "CDOLs";
            this.ClientDataDeliveryObserveAndLoad.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ClientDataDeliveryObserveAndLoadInstaller,
            this.ClientDataDeliveryObserveAndLoad});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ClientDataDeliveryObserveAndLoadInstaller;
        private System.ServiceProcess.ServiceInstaller ClientDataDeliveryObserveAndLoad;
    }
}