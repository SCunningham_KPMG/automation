﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CDOLs
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        // ==========================================================================================================================
        // Autoher: SJCunningham
        // Date: 2018 - 02-03
        // --------------------------------------------------------------------------------------------------------------------------
        // v1.0: Initial cut
        // v1.1: 2018-03-20 - Moved housekeeping to a quieter point in the code to avoid attempts to remove directories still in use.
        // v1.2: 2018-03-20 - Parallel run QA - tighter error handling in delegate events
        // v1.3: 2018-03-21 - Remove housekeeping of watch directory in case this is contributing to data redelivery into SFTP
        // v1.4: 2018-03-20 - Change StreamWriter encoding to ASCII
        // v1.5: 2018-03-28 - Re-instated housekeeping of watch directory - redeliveries issue resolved, and unrelated to this
        // v1.6: 2018-04-10 - Conversion to service ready for phase 2
        // ==========================================================================================================================

        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new CDOLs()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
