﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CDOLs
{
    public partial class CDOLs : ServiceBase
    { 

        // Set up the file system watcher

        FileSystemWatcher fswObserver = new FileSystemWatcher();

        public CDOLs()
        { 
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            // Set logging level, transfer mode (move or copy) and pause transfer mode

            string loggingMode = "None";
            string transferMode = "ReportOnly";
            string pauseTransferYN = "No";
            string fileLog = string.Format(@"FileWatch_{0}.log", DateTime.Now.ToString("yyyyMMdd"));

            loggingMode = ConfigurationManager.AppSettings.Get("SwitchModeLogging");
            transferMode = ConfigurationManager.AppSettings.Get("SwitchModeTransfer");

            // Global var defs
            string pathToWatch = ConfigurationManager.AppSettings.Get("WatchDirectory");
            string timerInterval = ConfigurationManager.AppSettings.Get("TimerInterval");
            string lastActionThreshold = ConfigurationManager.AppSettings.Get("LastActionThreshold");
            string transferToDirectory = ConfigurationManager.AppSettings.Get("TransferToDirectory");
            string logDirectory = ConfigurationManager.AppSettings.Get("LogDirectory");
            int pauseTransferFilesPerBatch = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PauseTransferFilesPerBatch"));
            int pauseTransferDelayBetweenBatches = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PauseTransferDelayBetweenBatchesInSeconds"));
            string endProcessFile = ConfigurationManager.AppSettings.Get("EndProcess");
            string observerFilesToLookFor = ConfigurationManager.AppSettings.Get("ObserverFilesToLookFor");

            // Log event id types
            // Logged to SQL only
            // 1 - Information
            // 2 - Timed scan
            // 3 - Observer actions
            // 4 - Zip (general)
            // 5 - Zip success
            // 96 - Zip error
            // 99 - general error

            Library.writeLog(DateTime.Now, "File watch started.", logDirectory, @"FileWatch.log", loggingMode, 1);
            Library.writeLog(DateTime.Now, string.Format(@"Observing directory {0} and any sub-folders", pathToWatch), logDirectory, @"FileWatch.log", loggingMode, 1);
            Library.writeLog(DateTime.Now, string.Format("Logging mode: {0}", loggingMode), logDirectory, @"FileWatch.log", loggingMode, 1);
            Library.writeLog(DateTime.Now, string.Format("Transfer mode: {0}", transferMode), logDirectory, @"FileWatch.log", loggingMode, 1);
            Library.writeLog(DateTime.Now, string.Format("Pause transfer: {0}", pauseTransferYN), logDirectory, @"FileWatch.log", loggingMode, 1);

            int timerIntervalnum;

            timerIntervalnum = Convert.ToInt32(timerInterval);

            try
            {

                Observer oConversation = new Observer();

                oConversation.lastActionThreshold = lastActionThreshold;
                oConversation.transferToDirectory = transferToDirectory;
                oConversation.pathToWatch = pathToWatch;
                oConversation.timerInterval = timerInterval;
                oConversation.loggingMode = loggingMode;
                oConversation.transferMode = transferMode;
                oConversation.logDirectory = logDirectory;
                oConversation.pauseTransferFilesPerBatch = pauseTransferFilesPerBatch;
                oConversation.pauseTransferDelayBetweenBatches = pauseTransferDelayBetweenBatches;

                fswObserver.Path = oConversation.pathToWatch;
                fswObserver.Filter = observerFilesToLookFor;
                fswObserver.EnableRaisingEvents = true;
                fswObserver.IncludeSubdirectories = true;

                fswObserver.Changed += delegate { Library.fswObserver_Changed(oConversation); };
                fswObserver.Created += delegate { Library.fswObserver_Created(oConversation); };
                fswObserver.Deleted += delegate { Library.fswObserver_Deleted(oConversation); };
                fswObserver.Renamed += delegate { Library.fswObserver_Renamed(oConversation); };

                //// Set up the timer
                System.Timers.Timer observerTimer;
                observerTimer = new System.Timers.Timer(timerIntervalnum);

                observerTimer.Elapsed += delegate { Library.elapsedTimeEvents(oConversation, observerTimer); }; //new ElapsedEventHandler(elapsedTimeEvents); Use a delegate to call the time events method - we do not need the sender and event arguments. If we do, change this to a lambda.
                observerTimer.Enabled = true;

                bool dbAccessible = Library.checkDB();

                if (dbAccessible)
                {
                    Library.writeLog(DateTime.Now, string.Format("Database checked and accessible"), logDirectory, @"FileWatch.log", loggingMode, 1);
                }
                else
                {
                    Library.writeLog(DateTime.Now, string.Format("Database checked: cannot access"), logDirectory, @"FileWatch.log", loggingMode, 99);
                }
            }
            catch (Exception ex)
            {
                Library.writeLog(DateTime.Now, string.Format("Service start ERROR: {0}", ex.Message), logDirectory, @"FileWatch.log", loggingMode, 99);
            }
        }

        protected override void OnStop()
        {
            string loggingMode = ConfigurationManager.AppSettings.Get("SwitchModeLogging");
            string logDirectory = ConfigurationManager.AppSettings.Get("LogDirectory");

            Library.writeLog(DateTime.Now, string.Format(@"File watch ended"), logDirectory, @"FileWatch.log", loggingMode, 1);
        }
    }
}
