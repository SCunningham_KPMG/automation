﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoiceFileArchive
{
    class Program
    {
        static void Main(string[] args)
        {

            string conn = ConfigurationManager.AppSettings.Get("VDB");
            string runLocal = ConfigurationManager.AppSettings.Get("RunLocal");
            string copyTo = ConfigurationManager.AppSettings.Get("StorageDir");
            string sqlCommand = "";

            DataTable dt = new DataTable();

            DataTable fbTab = new DataTable();
            DataColumn fbCol = new DataColumn("Location", typeof(String));
            fbTab.Columns.Add(fbCol);

            fbCol = new DataColumn("FilePathFrom", typeof(String));
            fbTab.Columns.Add(fbCol);

            fbCol = new DataColumn("FileNameFrom", typeof(String));
            fbTab.Columns.Add(fbCol);

            fbCol = new DataColumn("FilePathTo", typeof(String));
            fbTab.Columns.Add(fbCol);

            fbCol = new DataColumn("FileNameTo", typeof(String));
            fbTab.Columns.Add(fbCol);

            Console.WriteLine(@"Start: {0}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));

            using (SqlConnection sqlConn = new SqlConnection(conn))
            {

                sqlConn.Open();

                // TODO: Check this better and do replacements better when time
                if (runLocal.ToUpper() == "NO")
                {
                    sqlCommand = @"SELECT 
	                                Directory_UNC AS [FilePath]
	                                ,ISNULL(b.FileName, '') AS FileName
	                                ,Location
                                FROM 
	                                [wip].[SpeakerCallsToBeDeleted] a
                                INNER JOIN
	                                Voice_Data.dbo.vwFileList b
                                ON 
	                                a.FileName = b.FileName
                                        ";
                }
                else
                {
                    sqlCommand = @"SELECT 
	                                Directory AS [FilePath]
	                                ,ISNULL(b.FileName, '') AS FileName
	                                ,Location
                                FROM 
	                                [wip].[SpeakerCallsToBeDeleted] a
                                INNER JOIN
	                                Voice_Data.dbo.vwFileList b
                                ON 
	                                a.FileName = b.FileName";
                }
                using (SqlDataAdapter sda = new SqlDataAdapter(sqlCommand, conn))
                {
                    sda.SelectCommand.CommandType = CommandType.Text;
                    sda.SelectCommand.CommandTimeout = 180;
                    sda.Fill(dt);
                }
            }

            // Get a list of all folders first, then iterate over them one by looking for the file - much quicker than brute force GetFiles()

            //Console.WriteLine("{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), "Getting dirs and subs");
            //string[] getDirectories = Directory.GetDirectories(rootPath, "*", SearchOption.AllDirectories);
            //Console.WriteLine("{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), "Finished getting dirs and subs");

            foreach (DataRow dr in dt.Rows)
            {
                string filePath = dr["FilePath"].ToString();
                string fileName = dr["FileName"].ToString();
                string location = dr["Location"].ToString();
                string fileExisting = string.Format(@"{0}\{1}", filePath, fileName);

                // Move this file

                string dirNew = string.Format(@"{0}\{1}", copyTo, location);
                string fileNew = string.Format(@"{0}\{1}\{2}", copyTo, location, fileName);

                if (!Directory.Exists(dirNew))
                {
                    Directory.CreateDirectory(string.Format(@"{0}\{1}", copyTo, location));
                }

                // Remove from the destination ONLY if it also exists at the source
                // This may not be true if a move operation was interrupted i.e.
                // the file now only exists at the destination and was moved from the source,
                // so we would lose all record of it.
                // If it doesn't exist at the source we don't need to move it anyway.
                if (File.Exists(fileExisting))
                {
                    if (File.Exists(fileNew)) // Somehow exists in both places, must remove the destination to avoid move errors
                    {
                        File.Delete(fileNew);
                    }

                    try
                    {
                        Console.WriteLine(@"Moving file: {0} - {1} to {2}", location, fileExisting, fileNew);

                        File.Move(fileExisting, fileNew);

                        DataRow fbDR = fbTab.NewRow();

                        fbDR["Location"] = location;
                        fbDR["FilePathFrom"] = filePath;
                        fbDR["FileNameFrom"] = fileName;
                        fbDR["FilePathTo"] = dirNew;
                        fbDR["FileNameTo"] = fileName;

                        fbTab.Rows.Add(fbDR);

                    }
                    catch
                    {

                    }
                }
            }

            using (SqlBulkCopy sqlBC = new SqlBulkCopy(conn))
            {
                sqlBC.DestinationTableName = "[wip].[SpeakerCallsToBeDeleted_Log]";
                sqlBC.WriteToServer(fbTab);
            }

            Console.WriteLine(@"End: {0}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();

        }
    }
}
