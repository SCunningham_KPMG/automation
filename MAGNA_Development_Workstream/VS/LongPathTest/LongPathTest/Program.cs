﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongPathTest
{
    class Program
    {

        public static string AddQuotesIfRequired(string path)
        {
            return !string.IsNullOrEmpty(path) ?
                path.Contains(" ") ? "\"" + path + "\"" : path
                : string.Empty;
        }

        public static string RemoveInvalidChars(string toCheck)
        {
            foreach (var ic in System.IO.Path.GetInvalidPathChars())
            {
                toCheck.Replace(ic.ToString(), "");
            }
            foreach (var ic in System.IO.Path.GetInvalidFileNameChars())
            {
                toCheck.Replace(ic.ToString(), "");
            }

            return toCheck;
        }

        static void Main(string[] args)
        {
            //  - Doesn't work: string[] slf = Directory.GetFiles(@"D:\1.Development\CunninghamS\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\2017-11-13.20171123\2017-11-13", "*.txt");
            // - Works string[] slf = Directory.GetFiles(@"\\?\D:\1.Development\CunninghamS\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\2017-11-13.20171123\2017-11-13", "*.*");
            // string[] slf = Directory.GetFiles(@"\\?\D:\1.Development\CunninghamS\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\2017-11-13.20171123\2017-11-13", "*.*");
            // - Doesn't work string[] slf = Directory.GetFiles(@"\\?\\\KPMGPROD.COM\prd\trs01p\challenger\Development\OzDataTestBackup\S0472\Reuters_NL_240717 - 300717\2017 - 09 - 08 114929\Paul Butler2017-09 - 08 114929\Paul ButlerContent2017-09 - 08 114929\Reuters\TRM_NL_RAB_17072425.xml\ReallyLongTest", "*.*");

            string[] slf = Directory.GetFiles(@"Y:\trs01p\challenger\Development\OzDataTestBackup\S0472\Reuters_NL_240717-300717\2017-09-08 114929\Paul Butler2017-09-08 114929\Paul ButlerContent2017-09-08 114929\Reuters\TRM_NL_RAB_17072425.xml\ReallyLongTest\ReallyLongTest", "*.*");
            foreach (string fileName in slf)
            {
                Console.WriteLine(fileName);

                // Broken - string ic = "Y:\\trs01p\\challenger\\Development\\OzDataTestBackup\\Steve Test.txt\";
                // Works -- string ic2 = "Y:\\trs01p\\challenger\\Development\\OzDataTestBackup\\Steve Test.txt";
                string ic3 = @"Y:\trs01p\challenger\Development\OzDataTestBackup\S0472\Reuters_NL_240717 - 300717\2017 - 09 - 08 114929\Paul Butler2017-09 - 08 114929\Paul ButlerContent2017-09 - 08 114929\Reuters\TRM_NL_RAB_17072425.xml\ReallyLongTest\ReallyLongTest\SteveTest.txt";

                string ic4 = "Y:\\trs01p\\challenger\\Development\\OzDataTestBackup\\S0472\\Reuters_NL_240717%20-%20300717\\2017%20-%2009%20-%2008%20114929\\Paul%20Butler2017-09%20-%2008%20114929\\Paul%20ButlerContent2017-09%20-%2008%20114929\\Reuters\\TRM_NL_RAB_17072425.xml\\ReallyLongTest\\ReallyLongTest\\SteveTest.txt";

                string pathForNewFile = Path.GetDirectoryName(fileName);
                
                string newFile = AddQuotesIfRequired( string.Format(@"{0}\{1}", pathForNewFile, @"SteveTest.txt") );
                newFile = newFile.Trim('\r', '\n');
                ic3 = RemoveInvalidChars(ic3);
                ic3 = AddQuotesIfRequired(ic3);

                using (StreamWriter sw = new StreamWriter(ic4))
                {
                    sw.WriteLine("Here's a line of text");
                }
            }
            Console.ReadKey();
        }
    }
}
