﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveAlreadyProcessed
{
    class Program
    {
        static void Main(string[] args)
        {
            string conn = ConfigurationManager.AppSettings.Get("CDB");

            DataTable dt = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(conn))
            {

                sqlConn.Open();

                using (SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM [Chat].[S0499Processed_Separate]", conn))
                {
                    sda.SelectCommand.CommandType = CommandType.Text;
                    sda.SelectCommand.CommandTimeout = 180;
                    sda.Fill(dt);
                }
            }

            string rootPath = @"T:\p\S0499";

            // Get a list of all folders first, then iterate over them one by looking for the file - much quicker than brute force GetFiles()

            Console.WriteLine("{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), "Getting dirs and subs");
            List<string> getDirectories = Directory.GetDirectories(rootPath, "*").ToList();

            foreach (string dir in getDirectories)
            {

            }

            getDirectories.Add(Directory.GetDirectories(rootPath, "*").ToList());

            Console.WriteLine("{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), "Finished getting dirs and subs");

            foreach (DataRow dr in dt.Rows)
            {
                string fileName = dr["FileName"].ToString();
                Console.WriteLine(fileName);

                // Find this file
                string[] fileLocation = Directory.GetFiles(rootPath, fileName, SearchOption.AllDirectories);
            }

        }
    }
}
