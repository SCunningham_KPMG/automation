﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SampleLoadFileGenerator
{
    class Program
    {

        static void Main(string[] args)
        {

            // Decide what we're running - TF, No load etc

            string spToRun = "";
            string releaseType = "";
            string logFileLocation = ConfigurationManager.AppSettings.Get("LogFileLocation");
            string conn = ConfigurationManager.AppSettings.Get("VoiceDDB");
            string sampleDir = ConfigurationManager.AppSettings.Get("SampleDir");
            string copyFiles = ConfigurationManager.AppSettings.Get("CopyFiles"); // Can be toggled to generate a meta file without the file copy.
            string runLocal = ConfigurationManager.AppSettings.Get("RunLocal");
            string writeBackToDB = ConfigurationManager.AppSettings.Get("WriteBackToDB");
            string releaseId = "";

            // Check if switches have been supplied. If none, report error and cease
            if (args.Length == 0)
            {
                Library.WriteLog("Error - please supply a command switch to indicate the release type required e.g. /TF for Trader Follow. Use /? to see all valid switches.", logFileLocation);
                System.Environment.Exit(-2);
            }

            if (args.Contains(@"/?"))
            {
                Library.WriteLog("Valid switches are /TF for Trader Follow and /NO for No Load", logFileLocation);
                System.Environment.Exit(-2);
            }

            if (args.Contains(@"/TF"))
            {
                spToRun = @"dbo.SampleLoadFileSource";
                releaseType = "TF";
                Library.WriteLog("Trader Follow release will be generated", logFileLocation);
            }
            if (args.Contains(@"/NO"))
            {
                spToRun = @"dbo.SampleLoadFileSource_NoLoad";
                releaseType = "NO";
                Library.WriteLog("A \"No\" Load release will be generated", logFileLocation);
            }

            DataTable dt = new DataTable();

            // Get the list of candidate calls

            try

            {

                Library.WriteLog("Getting list of calls", logFileLocation);

                try
                {

                    dt = Library.GetReleaseData(conn, spToRun);

                }
                catch (Exception ex)
                {
                    Library.WriteLog(string.Format("ERROR - Getting list of calls: {0}", ex.Message), logFileLocation);
                    //================
                    // Do not continue
                    //================
                    System.Environment.Exit(-1);
                }

                int batchSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("BatchSize"));
                int batchPauseInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings.Get("BatchPauseInSeconds"));
                int batchPauseInMilliseconds = Convert.ToInt32((TimeSpan.FromSeconds(batchPauseInSeconds).TotalMilliseconds));
                int metadataToFileCopyPauseInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings.Get("MetadataToFileCopyPauseInSeconds"));
                int metadataToFileCopyPauseInMilliseconds = Convert.ToInt32((TimeSpan.FromSeconds(metadataToFileCopyPauseInSeconds).TotalMilliseconds));
                int copiedThisBatch = 0;

                //================================
                // Get a release id for this batch
                //================================
                Library.WriteLog("Getting release ID", logFileLocation);

                try
                {
                    string exMessage = "";
                    bool releaseIdGen = Library.GetReleaseID(conn, out exMessage, out releaseId);
                    if (releaseIdGen)
                    {
                        Library.WriteLog(string.Format("Release ID generated: {0}", releaseId), logFileLocation);
                    }
                    else
                    {
                        Library.WriteLog(string.Format("ERROR - Getting Release ID: {0}", exMessage), logFileLocation);
                        throw new System.Exception("Error encountered while generating release id (see log) - exiting...");
                    }
                }
                catch (Exception ex)
                {
                    Library.WriteLog(string.Format("ERROR - Getting release Id: {0}", ex.Message), logFileLocation);

                    //================
                    // Do not continue
                    //================
                    System.Environment.Exit(-1);
                }


                // =======================
                // Write the metadata file
                // =======================
                Library.WriteLog("Writing metadata file...", logFileLocation);

                string returnedMessage = "";

                try
                {
                    bool metadataWritten = Library.WriteMetadataFile(sampleDir, releaseId, dt, out returnedMessage);
                    if (metadataWritten)
                    {
                        Library.WriteLog("Finished writing metadata file.", logFileLocation);
                    }
                    else
                    {
                        Library.WriteLog(string.Format(@"Error writing metadata file: {0}", returnedMessage), logFileLocation);
                        throw new System.Exception("Error encountered while writing metadata file (see log) - exiting...");
                    }
                }
                catch (Exception ex)
                {
                    Library.WriteLog(string.Format(@"Error writing metadata file: {0}", ex.Message), logFileLocation);
                    System.Environment.Exit(-1);
                }

                // =====================================================================
                // Now pause a set time to allow the metadata file to be ingested before
                // beginning the audio file copy
                // =====================================================================
                Library.WriteLog(string.Format(@"Sleeping {0}(s) before file copies start", metadataToFileCopyPauseInSeconds), logFileLocation);
                Thread.Sleep(metadataToFileCopyPauseInMilliseconds);

                // Check that the Inbox folder has actually been cleared of previous files as is ready to receive new files

                while (!Library.DirectoryIsEmpty(sampleDir))
                {
                    Library.WriteLog("Nexidia inbox is not yet clear. Pausing for 1 minute...", logFileLocation);
                    Thread.Sleep(60000);
                }

                Library.WriteLog(@"Continuing...", logFileLocation);

                //===========================================================================================
                // Now perform the copies - any issues will mean the row stays set to 1 ready for anoter pass
                //===========================================================================================
                foreach (DataRow dr in dt.Rows)
                {
                    string path;

                    if (runLocal.ToUpper() == "YES")
                    {
                        path = dr["Directory"].ToString();
                    }
                    else
                    {
                        path = dr["Directory_UNC"].ToString();
                    }

                    string fileName = dr["FileName"].ToString();

                    try
                    {

                        // Copy the file to the import directory if copying is switched on - could be we only want to generate the meta file
                        if (copyFiles.ToUpper() == "YES")
                        {
                            Library.WriteLog(string.Format(@"Copying: {0}\{1} to {2}\{3}", path, fileName, sampleDir, fileName), logFileLocation);

                            File.Copy(string.Format(@"{0}\{1}", path, fileName), string.Format(@"{0}\{1}", sampleDir, fileName), true);

                            copiedThisBatch++;

                            if (copiedThisBatch == batchSize)
                            {
                                Library.WriteLog(string.Format(@"Batch size {0} reached. Pausing for {1} second(s)", batchSize, batchPauseInSeconds), logFileLocation);
                                copiedThisBatch = 0;
                                Thread.Sleep(batchPauseInMilliseconds);
                                Library.WriteLog("Resuming...", logFileLocation);
                            }
                        }

                        // Set the status of this row to 2 (copied)
                        dr["Status"] = 2;
                        // Stamp the row with the release Id
                        dr["ReleaseFileID"] = releaseId;

                        Library.WriteLog("Status and release id written", logFileLocation);
                    }
                    catch (Exception ex)
                    {
                        Library.WriteLog(string.Format(@"Error copying: {0}\{1} to {2}\{3}: {4}", path, fileName, sampleDir, fileName, ex.Message), logFileLocation);
                    }

                } // End ForEach

 

                // Write the changes (status 2 changes) back to the DB, if that option has not been disabled in the config file.
                // It may have been disabled just to create the text file only

                if (writeBackToDB.ToUpper() == "YES")
                {

                    string fbConn = ConfigurationManager.AppSettings.Get("VoiceDDB");
                    string exMessage = "";

                    Library.WriteLog("Writing status and release id information back to SQL...", logFileLocation);

                    bool dbWritebackSuccess = Library.DBWriteBack(conn, dt, out exMessage, releaseType);

                    if (dbWritebackSuccess)
                    {
                        Library.WriteLog("Finished writing status and release id information back to SQL.", logFileLocation);
                    }
                    else
                    {
                        Library.WriteLog(string.Format("Error writing to SQL: {0}", exMessage), logFileLocation);
                        throw new System.Exception("Error during SQL status writeback (seel log). Exitting...");
                    }

                }
            }

            catch (Exception ex)
            {
                Library.WriteLog(string.Format("Error, ceasing operation: {0}", ex.Message), logFileLocation);
                System.Environment.Exit(-1);
            }
        }
    }
}
