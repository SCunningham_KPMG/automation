﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLoadFileGenerator
{
    public class Library
    {
        internal static bool DirectoryIsEmpty(string path)
        {
            // Returns false if files are present or true if the folder is empty
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        internal static bool DBWriteBack(string sqlConnection, DataTable releaseData, out string exMessage, string releaseType)
        {

            try
            {
                using (SqlConnection sqlFBConn = new SqlConnection(sqlConnection))
                {
                    sqlFBConn.Open();

                    // Clear the staging table
                    string sqlCom = @"TRUNCATE TABLE [ETL].[OutputFile_WriteBack]";

                    SqlCommand sqlClear = new SqlCommand(sqlCom, sqlFBConn);

                    sqlClear.ExecuteNonQuery();

                    // Copy new
                    using (SqlBulkCopy sqlBC = new SqlBulkCopy(sqlFBConn))
                    {
                        sqlBC.DestinationTableName = "[ETL].[OutputFile_WriteBack]";
                        sqlBC.WriteToServer(releaseData);
                    }

                    // Update the statuses and release id
                    sqlCom = @"dbo.SampleLoadFile_WriteBack";

                    SqlCommand sqlUpdate = new SqlCommand(sqlCom, sqlFBConn);

                    sqlUpdate.CommandType = CommandType.StoredProcedure;
                    sqlUpdate.Parameters.AddWithValue("@releaseType", releaseType);

                    sqlUpdate.ExecuteNonQuery();

                }

                exMessage = "";
                return true;
            }
            catch (Exception ex)
            {
                exMessage = ex.Message;
                return false;
            }



        }

        public static bool GetReleaseID(string sqlConnection, out string message, out string releaseID)
        {
            try
            {

                string releaseId;

                using (SqlConnection relConn = new SqlConnection(sqlConnection))
                {
                    relConn.Open();

                    using (SqlCommand relComm = new SqlCommand(@"dbo.getNextReleaseId", relConn))
                    {
                        relComm.CommandType = CommandType.StoredProcedure;
                        relComm.Parameters.Add("@releaseId", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

                        relComm.ExecuteNonQuery();

                        releaseId = relComm.Parameters["@releaseId"].Value.ToString();

                        message = "";
                        releaseID = releaseId;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                releaseID = "";
                return false;
            }

        }

        public static DataTable GetReleaseData(string sqlConnString, string spToRun)
        {

            DataTable dtRelease = new DataTable();

            using (SqlConnection sqlConn = new SqlConnection(sqlConnString))
            {
                sqlConn.Open();

                using (SqlDataAdapter sqlDA = new SqlDataAdapter(spToRun, sqlConn))
                {
                    sqlDA.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDA.SelectCommand.CommandTimeout = 1800;
                    sqlDA.Fill(dtRelease);
                }
            }

            return dtRelease;
        }

        public static void WriteLog(string infoToWrite, string logFileLocation)
        {
            string logFile = string.Format(@"{0}\{1}{2}.log", logFileLocation, "SampleFileGenerator_", DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture));
            try
            {
                using (StreamWriter swExceptions = new StreamWriter(logFile, true))
                {
                    swExceptions.WriteLine(string.Format(@"{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), infoToWrite));
                }
                Console.WriteLine(string.Format(@"{0} - {1}", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"), infoToWrite));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error writing to log file: {0}", ex);
            }
        }

        internal static bool WriteMetadataFile(string sampleDirectory, string releaseID, DataTable releaseData, out string message)
        {
            string dateForReleaseFile = DateTime.Now.ToString("yyyyMMdd");
            string sampleFile = string.Format(@"{0}\Nexidia_Sample_{1}-{2}.txt", sampleDirectory, dateForReleaseFile, releaseID);

            try
            {
                using (StreamWriter sw = new StreamWriter(sampleFile, false, System.Text.Encoding.GetEncoding(1252)))
                {

                    sw.WriteLine("\"AudioFileName\"|\"CallId\"|\"StartDateTime\"|\"EndDateTime\"|\"Duration\"|\"DeviceType\"|\"TrunkLabel\"|\"Trader ID\"|\"TraderName\"|\"Desk\"|\"DirectionType\"|\"DialedNumber\"|\"CLI\"|\"PhoneNumber\"|\"SignalIdentifiers\"|\"Comment\"");

                    foreach (DataRow dr in releaseData.Rows)
                    {
                        if (dr["Status"].ToString() == "1")
                        {
                            sw.WriteLine(string.Format("\"{0}\"|\"{1}\"|\"{2}\"|\"{3}\"|\"{4}\"|\"{5}\"|\"{6}\"|\"{7}\"|\"{8}\"|\"{9}\"|\"{10}\"|\"{11}\"|\"{12}\"|\"{13}\"|\"{14}\"|\"{15}\""
                                , dr["MediaFileName"].ToString() // 0
                                , dr["CallID"].ToString() // 1
                                , string.Format(@"{0:dd\MM\yyyy HH:mm:ss}", dr["LocalStartTime"].ToString()) // 2
                                , string.Format(@"{0:dd\MM\yyyy HH:mm:ss}", dr["LocalEndTime"].ToString()) // 3
                                , dr["DurationHHMMSS"].ToString() // 4
                                , dr["DeviceType"].ToString() // 5
                                , dr["TrunkLabel"].ToString() // 6
                                , dr["TraderID"].ToString() // 7
                                , dr["TraderName"].ToString() // 8
                                , dr["CombinedBUandDepartment"].ToString() // 9
                                , dr["CallDirection"].ToString() // 10
                                , dr["DialedNumber"].ToString() // 11
                                , dr["CallLineIdentifier"].ToString() // 12
                                , dr["PhoneNumber"].ToString() // 13
                                , dr["SignalIdentifiers"].ToString() // 14
                                , dr["Comment"].ToString() // 15
                                ));
                        }
                    }

                } // End Streamwriter

                message = "";
                return true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

        }
    }
}
