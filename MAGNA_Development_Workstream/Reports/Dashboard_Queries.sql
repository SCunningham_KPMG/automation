--1.  Example for date 03/04/2018
SELECT * FROM [Voice_Analytics].[report].[ReviewDashboardDailyStats] WHERE ingestDate <= '20180403' 

--2. Number of FPR Call in Queue
SELECT SUM(NumberOfCalls) FROM [report].[ReviewDashboardDailyStats] WHERE HasBeenFPReviewed = 0 AND ingestDate <= '20180403'

--3. Number of calls Ingested
SELECT SUM(NumberOfCalls) FROM [report].[ReviewDashboardDailyStats] WHERE ingestDate <= '20180403' 

--4. Number of SPR calls in queue
SELECT SUM(NumberOfCalls) FROM [report].[ReviewDashboardDailyStats] 
WHERE [HasBeenSPReviewed] = 0 AND [ReleasedForSPR]=1 AND ingestDate <= '20180403'

--5. Number of FPR Call To Review I+5
SELECT SUM(NumberOfCalls) FROM [report].[ReviewDashboardDailyStats] WHERE HasBeenFPReviewed = 0 AND ingestDate = 
(	SELECT ic2.ingestDate 
	FROM report.IngestCalendar ic1
	INNER JOIN report.IngestCalendar ic2
	ON ic1.seq - 5 = ic2.seq
	WHERE ic1.ingestDate = '20180403')

-- 6. Calls Reviewed Stats
select * FROM [report].[ReviewedByReviewDateDailyStats] WHERE ReviewDate <= '20180403' 

-- 7. Average Daily Review Speed (I to I+5)
SELECT SUM(NumberOfCalls)/6.00 
FROM [report].[ReviewedByReviewDateDailyStats]
WHERE ReviewDate >=
(	SELECT MIN(ic2.ingestDate) 
	FROM report.IngestCalendar ic1
	INNER JOIN report.IngestCalendar ic2
	ON ic1.seq - 5 <= ic2.seq
	WHERE ic1.ingestDate = '20180403')
AND ReviewDate <=  '20180403'

-- 8. Calls Reviewed (I+5) vs Relevant Calls
SELECT ReviewDate, SUM(FPR_Relevant) CallsRelevant, SUM(NumberOfCalls) CallsReviewed 
FROM [report].[ReviewedByReviewDateDailyStats]
WHERE ReviewDate =
(	SELECT MIN(ic2.ingestDate) 
	FROM report.IngestCalendar ic1
	INNER JOIN report.IngestCalendar ic2
	ON ic1.seq - 5 = ic2.seq
	WHERE ic1.ingestDate = '20180403')
GROUP BY ReviewDate

-- 9. Calls Reviewed (I+5) vs Relevant Calls (Example view by asset class)
SELECT ReviewDate, AssetClass, SUM(FPR_Relevant) CallsRelevant, SUM(NumberOfCalls) CallsReviewed 
FROM [report].[ReviewedByReviewDateDailyStats]
WHERE ReviewDate =
(	SELECT MIN(ic2.ingestDate) 
	FROM report.IngestCalendar ic1
	INNER JOIN report.IngestCalendar ic2
	ON ic1.seq - 5 = ic2.seq
	WHERE ic1.ingestDate = '20180403')
GROUP BY ReviewDate, AssetClass

-- 10. Calls reviewd vs Calls sampled

SELECT r.reviewDate, SUM(r.NumberOfCalls) CallsReviewed, COALESCE(SUM(i.NumberOfCalls),0) CallsIngested
FROM report.vwCallsReviewedPerDay r
LEFT JOIN report.vwCallsIngestedPerDay i
	ON r.ReviewDate = i.IngestDate
	AND r.location = i.location
	AND r.assetclass = i.AssetClass
	AND r.segment = i.Segment
	AND r.ReviewerLocation = i.ReviewerLocation
WHERE ReviewDate <= '20180403'
GROUP BY r.reviewDate
ORDER BY r.reviewDate

-- 11. Main graph at the top of the dashboard

-- TODAY
SELECT 'I (today)' IDate, ds.IngestDate, category, SUM(NumberOfCalls) NumberOfCalls, sum(duration) Duration 
FROM [report].[ReviewDashboardDailyStats] ds 
INNER JOIN [report].[IngestCalendar] c
	ON c.IngestDate = ds.IngestDate
WHERE ds.ingestDate = '20180404' 
GROUP BY ds.IngestDate, category
UNION
-- I+1 to I+20
SELECT 'I+' + CAST (icalToday.seq-ical.seq as varchar(4)), ds.IngestDate, category, SUM(NumberOfCalls) NumberOfCalls, sum(duration) Duration 
FROM [report].[ReviewDashboardDailyStats] ds 
INNER JOIN  report.IngestCalendar ical
	ON ds.IngestDate = ical.IngestDate
INNER JOIN  report.IngestCalendar icalToday
	ON icalToday.IngestDate = '20180404'
WHERE ds.IngestDate IN
(	SELECT TOP 20 ingestDate 
	FROM report.IngestCalendar ic1
	WHERE ic1.IngestDate < '20180404'
	ORDER BY ingestDate DESC)
GROUP BY 'I+' + CAST (icalToday.seq-ical.seq as varchar(4)), ds.IngestDate, category
UNION
-- I+21 to I+30 as 1 row
SELECT 'I+21 to I+30', MIN(ds.IngestDate), category, SUM(NumberOfCalls) NumberOfCalls, sum(duration) Duration 
FROM [report].[ReviewDashboardDailyStats] ds 
INNER JOIN  report.IngestCalendar ical
	ON ds.IngestDate = ical.IngestDate
WHERE ds.IngestDate IN
(	SELECT ingestDate 
	FROM report.IngestCalendar ic1
	WHERE ic1.IngestDate < '20180404'
	ORDER BY ingestDate DESC
	OFFSET 20 rows FETCH NEXT 10 ROWS ONLY
	)
GROUP BY category
ORDER BY 2 desc, 3
