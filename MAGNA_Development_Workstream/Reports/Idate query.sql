-- TODAY
SELECT 'I (today)' IDate, ds.IngestDate, category, SUM(NumberOfCalls) NumberOfCalls, sum(duration) Duration, sum(FPS_Hits) FPS_Hits, SUM(CR_Hits) CR_Hits, ds.HasBeenFPReviewed, ds.SelectedForReview, ds.FPR_Relevant, ds.Location, ds.ReviewerLocation, ds.AssetClass, ds.Segment
FROM [report].[ReviewDashboardDailyStats] ds 
INNER JOIN [report].[IngestCalendar] c
	ON c.IngestDate = ds.IngestDate
WHERE ds.ingestDate = '20180404' 
GROUP BY ds.IngestDate, category, Location, ReviewerLocation, AssetClass, Segment, ds.HasBeenFPReviewed, ds.SelectedForReview, ds.FPR_Relevant
UNION
-- I+1 to I+20
SELECT 'I+' + CAST (icalToday.seq-ical.seq as varchar(4)), ds.IngestDate, category, SUM(NumberOfCalls) NumberOfCalls, sum(duration) Duration,  sum(FPS_Hits) FPS_Hits, SUM(CR_Hits) CR_Hits,ds.HasBeenFPReviewed, ds.SelectedForReview, ds.FPR_Relevant, ds.Location, ds.ReviewerLocation, ds.AssetClass, ds.Segment
FROM [report].[ReviewDashboardDailyStats] ds 
INNER JOIN  report.IngestCalendar ical
	ON ds.IngestDate = ical.IngestDate
INNER JOIN  report.IngestCalendar icalToday
	ON icalToday.IngestDate = '20180404'
WHERE ds.IngestDate IN
(	SELECT TOP 20 ingestDate 
	FROM report.IngestCalendar ic1
	WHERE ic1.IngestDate < '20180404'
	ORDER BY ingestDate DESC)
GROUP BY 'I+' + CAST (icalToday.seq-ical.seq as varchar(4)), ds.IngestDate, category, Location, ReviewerLocation, AssetClass, Segment, ds.HasBeenFPReviewed, ds.SelectedForReview, ds.FPR_Relevant
UNION
-- I+21 to I+30 as 1 row
SELECT 'I+21 to I+30', MIN(ds.IngestDate), category, SUM(NumberOfCalls) NumberOfCalls, sum(duration) Duration,  sum(FPS_Hits) FPS_Hits, SUM(CR_Hits) CR_Hits, ds.HasBeenFPReviewed, ds.SelectedForReview, ds.FPR_Relevant, ds.Location, ds.ReviewerLocation, ds.AssetClass, ds.Segment
FROM [report].[ReviewDashboardDailyStats] ds 
INNER JOIN  report.IngestCalendar ical
	ON ds.IngestDate = ical.IngestDate
WHERE ds.IngestDate IN
(	SELECT ingestDate 
	FROM report.IngestCalendar ic1
	WHERE ic1.IngestDate < '20180404'
	ORDER BY ingestDate DESC
	OFFSET 20 rows FETCH NEXT 10 ROWS ONLY
	)
GROUP BY category, Location, ReviewerLocation, AssetClass, Segment, HasBeenFPReviewed, SelectedForReview, FPR_Relevant
--ORDER BY 2 desc, 3